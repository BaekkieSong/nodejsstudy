// source: sync.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var app_list_specifics_pb = require('./app_list_specifics_pb.js');
goog.object.extend(proto, app_list_specifics_pb);
var app_notification_specifics_pb = require('./app_notification_specifics_pb.js');
goog.object.extend(proto, app_notification_specifics_pb);
var app_setting_specifics_pb = require('./app_setting_specifics_pb.js');
goog.object.extend(proto, app_setting_specifics_pb);
var app_specifics_pb = require('./app_specifics_pb.js');
goog.object.extend(proto, app_specifics_pb);
var arc_package_specifics_pb = require('./arc_package_specifics_pb.js');
goog.object.extend(proto, arc_package_specifics_pb);
var article_specifics_pb = require('./article_specifics_pb.js');
goog.object.extend(proto, article_specifics_pb);
var autofill_specifics_pb = require('./autofill_specifics_pb.js');
goog.object.extend(proto, autofill_specifics_pb);
var bookmark_specifics_pb = require('./bookmark_specifics_pb.js');
goog.object.extend(proto, bookmark_specifics_pb);
var client_commands_pb = require('./client_commands_pb.js');
goog.object.extend(proto, client_commands_pb);
var client_debug_info_pb = require('./client_debug_info_pb.js');
goog.object.extend(proto, client_debug_info_pb);
var device_info_specifics_pb = require('./device_info_specifics_pb.js');
goog.object.extend(proto, device_info_specifics_pb);
var dictionary_specifics_pb = require('./dictionary_specifics_pb.js');
goog.object.extend(proto, dictionary_specifics_pb);
var encryption_pb = require('./encryption_pb.js');
goog.object.extend(proto, encryption_pb);
var experiments_specifics_pb = require('./experiments_specifics_pb.js');
goog.object.extend(proto, experiments_specifics_pb);
var extension_setting_specifics_pb = require('./extension_setting_specifics_pb.js');
goog.object.extend(proto, extension_setting_specifics_pb);
var extension_specifics_pb = require('./extension_specifics_pb.js');
goog.object.extend(proto, extension_specifics_pb);
var favicon_image_specifics_pb = require('./favicon_image_specifics_pb.js');
goog.object.extend(proto, favicon_image_specifics_pb);
var favicon_tracking_specifics_pb = require('./favicon_tracking_specifics_pb.js');
goog.object.extend(proto, favicon_tracking_specifics_pb);
var get_updates_caller_info_pb = require('./get_updates_caller_info_pb.js');
goog.object.extend(proto, get_updates_caller_info_pb);
var history_delete_directive_specifics_pb = require('./history_delete_directive_specifics_pb.js');
goog.object.extend(proto, history_delete_directive_specifics_pb);
var managed_user_setting_specifics_pb = require('./managed_user_setting_specifics_pb.js');
goog.object.extend(proto, managed_user_setting_specifics_pb);
var managed_user_shared_setting_specifics_pb = require('./managed_user_shared_setting_specifics_pb.js');
goog.object.extend(proto, managed_user_shared_setting_specifics_pb);
var managed_user_specifics_pb = require('./managed_user_specifics_pb.js');
goog.object.extend(proto, managed_user_specifics_pb);
var managed_user_whitelist_specifics_pb = require('./managed_user_whitelist_specifics_pb.js');
goog.object.extend(proto, managed_user_whitelist_specifics_pb);
var mountain_share_specifics_pb = require('./mountain_share_specifics_pb.js');
goog.object.extend(proto, mountain_share_specifics_pb);
var nigori_specifics_pb = require('./nigori_specifics_pb.js');
goog.object.extend(proto, nigori_specifics_pb);
var password_specifics_pb = require('./password_specifics_pb.js');
goog.object.extend(proto, password_specifics_pb);
var preference_specifics_pb = require('./preference_specifics_pb.js');
goog.object.extend(proto, preference_specifics_pb);
var printer_specifics_pb = require('./printer_specifics_pb.js');
goog.object.extend(proto, printer_specifics_pb);
var priority_preference_specifics_pb = require('./priority_preference_specifics_pb.js');
goog.object.extend(proto, priority_preference_specifics_pb);
var reading_list_specifics_pb = require('./reading_list_specifics_pb.js');
goog.object.extend(proto, reading_list_specifics_pb);
var search_engine_specifics_pb = require('./search_engine_specifics_pb.js');
goog.object.extend(proto, search_engine_specifics_pb);
var send_tab_to_self_specifics_pb = require('./send_tab_to_self_specifics_pb.js');
goog.object.extend(proto, send_tab_to_self_specifics_pb);
var session_specifics_pb = require('./session_specifics_pb.js');
goog.object.extend(proto, session_specifics_pb);
var sync_enums_pb = require('./sync_enums_pb.js');
goog.object.extend(proto, sync_enums_pb);
var synced_notification_app_info_specifics_pb = require('./synced_notification_app_info_specifics_pb.js');
goog.object.extend(proto, synced_notification_app_info_specifics_pb);
var synced_notification_specifics_pb = require('./synced_notification_specifics_pb.js');
goog.object.extend(proto, synced_notification_specifics_pb);
var theme_specifics_pb = require('./theme_specifics_pb.js');
goog.object.extend(proto, theme_specifics_pb);
var typed_url_specifics_pb = require('./typed_url_specifics_pb.js');
goog.object.extend(proto, typed_url_specifics_pb);
var unique_position_pb = require('./unique_position_pb.js');
goog.object.extend(proto, unique_position_pb);
var user_consent_specifics_pb = require('./user_consent_specifics_pb.js');
goog.object.extend(proto, user_consent_specifics_pb);
var user_event_specifics_pb = require('./user_event_specifics_pb.js');
goog.object.extend(proto, user_event_specifics_pb);
var wifi_credential_specifics_pb = require('./wifi_credential_specifics_pb.js');
goog.object.extend(proto, wifi_credential_specifics_pb);
goog.exportSymbol('proto.sync_pb.AuthenticateMessage', null, global);
goog.exportSymbol('proto.sync_pb.AuthenticateResponse', null, global);
goog.exportSymbol('proto.sync_pb.ChipBag', null, global);
goog.exportSymbol('proto.sync_pb.ChromiumExtensionsActivity', null, global);
goog.exportSymbol('proto.sync_pb.ClearServerDataMessage', null, global);
goog.exportSymbol('proto.sync_pb.ClearServerDataResponse', null, global);
goog.exportSymbol('proto.sync_pb.ClientConfigParams', null, global);
goog.exportSymbol('proto.sync_pb.ClientStatus', null, global);
goog.exportSymbol('proto.sync_pb.ClientToServerMessage', null, global);
goog.exportSymbol('proto.sync_pb.ClientToServerMessage.Contents', null, global);
goog.exportSymbol('proto.sync_pb.ClientToServerResponse', null, global);
goog.exportSymbol('proto.sync_pb.ClientToServerResponse.Error', null, global);
goog.exportSymbol('proto.sync_pb.CommitMessage', null, global);
goog.exportSymbol('proto.sync_pb.CommitResponse', null, global);
goog.exportSymbol('proto.sync_pb.CommitResponse.EntryResponse', null, global);
goog.exportSymbol('proto.sync_pb.CommitResponse.ResponseType', null, global);
goog.exportSymbol('proto.sync_pb.DataTypeContext', null, global);
goog.exportSymbol('proto.sync_pb.DataTypeProgressMarker', null, global);
goog.exportSymbol('proto.sync_pb.EntitySpecifics', null, global);
goog.exportSymbol('proto.sync_pb.EntitySpecifics.SpecificsVariantCase', null, global);
goog.exportSymbol('proto.sync_pb.EventRequest', null, global);
goog.exportSymbol('proto.sync_pb.EventResponse', null, global);
goog.exportSymbol('proto.sync_pb.GarbageCollectionDirective', null, global);
goog.exportSymbol('proto.sync_pb.GarbageCollectionDirective.Type', null, global);
goog.exportSymbol('proto.sync_pb.GetCrashInfoRequest', null, global);
goog.exportSymbol('proto.sync_pb.GetCrashInfoResponse', null, global);
goog.exportSymbol('proto.sync_pb.GetUpdateTriggers', null, global);
goog.exportSymbol('proto.sync_pb.GetUpdatesMessage', null, global);
goog.exportSymbol('proto.sync_pb.GetUpdatesMetadataResponse', null, global);
goog.exportSymbol('proto.sync_pb.GetUpdatesResponse', null, global);
goog.exportSymbol('proto.sync_pb.GetUpdatesStreamingResponse', null, global);
goog.exportSymbol('proto.sync_pb.ProfilingData', null, global);
goog.exportSymbol('proto.sync_pb.SyncDisabledEvent', null, global);
goog.exportSymbol('proto.sync_pb.SyncEntity', null, global);
goog.exportSymbol('proto.sync_pb.SyncEntity.BookmarkData', null, global);
goog.exportSymbol('proto.sync_pb.UserIdentification', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ProfilingData = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ProfilingData, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ProfilingData.displayName = 'proto.sync_pb.ProfilingData';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.EntitySpecifics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, 500, null, proto.sync_pb.EntitySpecifics.oneofGroups_);
};
goog.inherits(proto.sync_pb.EntitySpecifics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.EntitySpecifics.displayName = 'proto.sync_pb.EntitySpecifics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.SyncEntity = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.SyncEntity.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.SyncEntity, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.SyncEntity.displayName = 'proto.sync_pb.SyncEntity';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.SyncEntity.BookmarkData = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.SyncEntity.BookmarkData, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.SyncEntity.BookmarkData.displayName = 'proto.sync_pb.SyncEntity.BookmarkData';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ChromiumExtensionsActivity = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ChromiumExtensionsActivity, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ChromiumExtensionsActivity.displayName = 'proto.sync_pb.ChromiumExtensionsActivity';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientConfigParams = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.ClientConfigParams.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.ClientConfigParams, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientConfigParams.displayName = 'proto.sync_pb.ClientConfigParams';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.CommitMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.CommitMessage.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.CommitMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.CommitMessage.displayName = 'proto.sync_pb.CommitMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetUpdateTriggers = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.GetUpdateTriggers.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GetUpdateTriggers, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetUpdateTriggers.displayName = 'proto.sync_pb.GetUpdateTriggers';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GarbageCollectionDirective = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.GarbageCollectionDirective, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GarbageCollectionDirective.displayName = 'proto.sync_pb.GarbageCollectionDirective';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.DataTypeProgressMarker = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.DataTypeProgressMarker, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.DataTypeProgressMarker.displayName = 'proto.sync_pb.DataTypeProgressMarker';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetUpdatesMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, 500, proto.sync_pb.GetUpdatesMessage.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GetUpdatesMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetUpdatesMessage.displayName = 'proto.sync_pb.GetUpdatesMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.AuthenticateMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.AuthenticateMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.AuthenticateMessage.displayName = 'proto.sync_pb.AuthenticateMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClearServerDataMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ClearServerDataMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClearServerDataMessage.displayName = 'proto.sync_pb.ClearServerDataMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClearServerDataResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ClearServerDataResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClearServerDataResponse.displayName = 'proto.sync_pb.ClearServerDataResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ChipBag = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ChipBag, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ChipBag.displayName = 'proto.sync_pb.ChipBag';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientStatus = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ClientStatus, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientStatus.displayName = 'proto.sync_pb.ClientStatus';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.DataTypeContext = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.DataTypeContext, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.DataTypeContext.displayName = 'proto.sync_pb.DataTypeContext';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientToServerMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.ClientToServerMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientToServerMessage.displayName = 'proto.sync_pb.ClientToServerMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetCrashInfoRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.GetCrashInfoRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetCrashInfoRequest.displayName = 'proto.sync_pb.GetCrashInfoRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetCrashInfoResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.GetCrashInfoResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetCrashInfoResponse.displayName = 'proto.sync_pb.GetCrashInfoResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.CommitResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.CommitResponse.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.CommitResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.CommitResponse.displayName = 'proto.sync_pb.CommitResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.CommitResponse.EntryResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.CommitResponse.EntryResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.CommitResponse.EntryResponse.displayName = 'proto.sync_pb.CommitResponse.EntryResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetUpdatesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.GetUpdatesResponse.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GetUpdatesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetUpdatesResponse.displayName = 'proto.sync_pb.GetUpdatesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetUpdatesMetadataResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.GetUpdatesMetadataResponse.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GetUpdatesMetadataResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetUpdatesMetadataResponse.displayName = 'proto.sync_pb.GetUpdatesMetadataResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GetUpdatesStreamingResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.GetUpdatesStreamingResponse.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GetUpdatesStreamingResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GetUpdatesStreamingResponse.displayName = 'proto.sync_pb.GetUpdatesStreamingResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.UserIdentification = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.UserIdentification, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.UserIdentification.displayName = 'proto.sync_pb.UserIdentification';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.AuthenticateResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.AuthenticateResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.AuthenticateResponse.displayName = 'proto.sync_pb.AuthenticateResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientToServerResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.ClientToServerResponse.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.ClientToServerResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientToServerResponse.displayName = 'proto.sync_pb.ClientToServerResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientToServerResponse.Error = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.ClientToServerResponse.Error.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.ClientToServerResponse.Error, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientToServerResponse.Error.displayName = 'proto.sync_pb.ClientToServerResponse.Error';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.EventRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.EventRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.EventRequest.displayName = 'proto.sync_pb.EventRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.EventResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.EventResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.EventResponse.displayName = 'proto.sync_pb.EventResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.SyncDisabledEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.SyncDisabledEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.SyncDisabledEvent.displayName = 'proto.sync_pb.SyncDisabledEvent';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ProfilingData.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ProfilingData.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ProfilingData} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ProfilingData.toObject = function(includeInstance, msg) {
  var f, obj = {
    metaDataWriteTime: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    fileDataWriteTime: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    userLookupTime: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    metaDataReadTime: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    fileDataReadTime: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    totalRequestTime: (f = jspb.Message.getField(msg, 6)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ProfilingData}
 */
proto.sync_pb.ProfilingData.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ProfilingData;
  return proto.sync_pb.ProfilingData.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ProfilingData} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ProfilingData}
 */
proto.sync_pb.ProfilingData.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMetaDataWriteTime(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFileDataWriteTime(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setUserLookupTime(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMetaDataReadTime(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFileDataReadTime(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTotalRequestTime(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ProfilingData.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ProfilingData.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ProfilingData} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ProfilingData.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeInt64(
      6,
      f
    );
  }
};


/**
 * optional int64 meta_data_write_time = 1;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getMetaDataWriteTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setMetaDataWriteTime = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearMetaDataWriteTime = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasMetaDataWriteTime = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 file_data_write_time = 2;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getFileDataWriteTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setFileDataWriteTime = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearFileDataWriteTime = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasFileDataWriteTime = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int64 user_lookup_time = 3;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getUserLookupTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setUserLookupTime = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearUserLookupTime = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasUserLookupTime = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 meta_data_read_time = 4;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getMetaDataReadTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setMetaDataReadTime = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearMetaDataReadTime = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasMetaDataReadTime = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 file_data_read_time = 5;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getFileDataReadTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setFileDataReadTime = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearFileDataReadTime = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasFileDataReadTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int64 total_request_time = 6;
 * @return {number}
 */
proto.sync_pb.ProfilingData.prototype.getTotalRequestTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.setTotalRequestTime = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ProfilingData} returns this
 */
proto.sync_pb.ProfilingData.prototype.clearTotalRequestTime = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ProfilingData.prototype.hasTotalRequestTime = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.sync_pb.EntitySpecifics.oneofGroups_ = [[31729,32904,37702,40781,41210,45184,45873,47745,48119,48364,50119,63951,88610,96159,103656,150251,153108,235816,154522,161496,163425,170540,181534,182019,186662,194582,202026,306060,223759,229170,218175,306270,330441,340906,410745,411028,455206,556014,545005,601980]];

/**
 * @enum {number}
 */
proto.sync_pb.EntitySpecifics.SpecificsVariantCase = {
  SPECIFICS_VARIANT_NOT_SET: 0,
  AUTOFILL: 31729,
  BOOKMARK: 32904,
  PREFERENCE: 37702,
  TYPED_URL: 40781,
  THEME: 41210,
  APP_NOTIFICATION: 45184,
  PASSWORD: 45873,
  NIGORI: 47745,
  EXTENSION: 48119,
  APP: 48364,
  SESSION: 50119,
  AUTOFILL_PROFILE: 63951,
  SEARCH_ENGINE: 88610,
  EXTENSION_SETTING: 96159,
  APP_SETTING: 103656,
  HISTORY_DELETE_DIRECTIVE: 150251,
  SYNCED_NOTIFICATION: 153108,
  SYNCED_NOTIFICATION_APP_INFO: 235816,
  DEVICE_INFO: 154522,
  EXPERIMENTS: 161496,
  PRIORITY_PREFERENCE: 163425,
  DICTIONARY: 170540,
  FAVICON_TRACKING: 181534,
  FAVICON_IMAGE: 182019,
  MANAGED_USER_SETTING: 186662,
  MANAGED_USER: 194582,
  MANAGED_USER_SHARED_SETTING: 202026,
  MANAGED_USER_WHITELIST: 306060,
  ARTICLE: 223759,
  APP_LIST: 229170,
  WIFI_CREDENTIAL: 218175,
  AUTOFILL_WALLET: 306270,
  WALLET_METADATA: 330441,
  ARC_PACKAGE: 340906,
  PRINTER: 410745,
  READING_LIST: 411028,
  USER_EVENT: 455206,
  USER_CONSENT: 556014,
  MOUNTAIN_SHARE: 545005,
  SEND_TAB_TO_SELF: 601980
};

/**
 * @return {proto.sync_pb.EntitySpecifics.SpecificsVariantCase}
 */
proto.sync_pb.EntitySpecifics.prototype.getSpecificsVariantCase = function() {
  return /** @type {proto.sync_pb.EntitySpecifics.SpecificsVariantCase} */(jspb.Message.computeOneofCase(this, proto.sync_pb.EntitySpecifics.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.EntitySpecifics.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.EntitySpecifics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.EntitySpecifics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EntitySpecifics.toObject = function(includeInstance, msg) {
  var f, obj = {
    encrypted: (f = msg.getEncrypted()) && encryption_pb.EncryptedData.toObject(includeInstance, f),
    autofill: (f = msg.getAutofill()) && autofill_specifics_pb.AutofillSpecifics.toObject(includeInstance, f),
    bookmark: (f = msg.getBookmark()) && bookmark_specifics_pb.BookmarkSpecifics.toObject(includeInstance, f),
    preference: (f = msg.getPreference()) && preference_specifics_pb.PreferenceSpecifics.toObject(includeInstance, f),
    typedUrl: (f = msg.getTypedUrl()) && typed_url_specifics_pb.TypedUrlSpecifics.toObject(includeInstance, f),
    theme: (f = msg.getTheme()) && theme_specifics_pb.ThemeSpecifics.toObject(includeInstance, f),
    appNotification: (f = msg.getAppNotification()) && app_notification_specifics_pb.AppNotification.toObject(includeInstance, f),
    password: (f = msg.getPassword()) && password_specifics_pb.PasswordSpecifics.toObject(includeInstance, f),
    nigori: (f = msg.getNigori()) && nigori_specifics_pb.NigoriSpecifics.toObject(includeInstance, f),
    extension: (f = msg.getExtension$()) && extension_specifics_pb.ExtensionSpecifics.toObject(includeInstance, f),
    app: (f = msg.getApp()) && app_specifics_pb.AppSpecifics.toObject(includeInstance, f),
    session: (f = msg.getSession()) && session_specifics_pb.SessionSpecifics.toObject(includeInstance, f),
    autofillProfile: (f = msg.getAutofillProfile()) && autofill_specifics_pb.AutofillProfileSpecifics.toObject(includeInstance, f),
    searchEngine: (f = msg.getSearchEngine()) && search_engine_specifics_pb.SearchEngineSpecifics.toObject(includeInstance, f),
    extensionSetting: (f = msg.getExtensionSetting()) && extension_setting_specifics_pb.ExtensionSettingSpecifics.toObject(includeInstance, f),
    appSetting: (f = msg.getAppSetting()) && app_setting_specifics_pb.AppSettingSpecifics.toObject(includeInstance, f),
    historyDeleteDirective: (f = msg.getHistoryDeleteDirective()) && history_delete_directive_specifics_pb.HistoryDeleteDirectiveSpecifics.toObject(includeInstance, f),
    syncedNotification: (f = msg.getSyncedNotification()) && synced_notification_specifics_pb.SyncedNotificationSpecifics.toObject(includeInstance, f),
    syncedNotificationAppInfo: (f = msg.getSyncedNotificationAppInfo()) && synced_notification_app_info_specifics_pb.SyncedNotificationAppInfoSpecifics.toObject(includeInstance, f),
    deviceInfo: (f = msg.getDeviceInfo()) && device_info_specifics_pb.DeviceInfoSpecifics.toObject(includeInstance, f),
    experiments: (f = msg.getExperiments()) && experiments_specifics_pb.ExperimentsSpecifics.toObject(includeInstance, f),
    priorityPreference: (f = msg.getPriorityPreference()) && priority_preference_specifics_pb.PriorityPreferenceSpecifics.toObject(includeInstance, f),
    dictionary: (f = msg.getDictionary()) && dictionary_specifics_pb.DictionarySpecifics.toObject(includeInstance, f),
    faviconTracking: (f = msg.getFaviconTracking()) && favicon_tracking_specifics_pb.FaviconTrackingSpecifics.toObject(includeInstance, f),
    faviconImage: (f = msg.getFaviconImage()) && favicon_image_specifics_pb.FaviconImageSpecifics.toObject(includeInstance, f),
    managedUserSetting: (f = msg.getManagedUserSetting()) && managed_user_setting_specifics_pb.ManagedUserSettingSpecifics.toObject(includeInstance, f),
    managedUser: (f = msg.getManagedUser()) && managed_user_specifics_pb.ManagedUserSpecifics.toObject(includeInstance, f),
    managedUserSharedSetting: (f = msg.getManagedUserSharedSetting()) && managed_user_shared_setting_specifics_pb.ManagedUserSharedSettingSpecifics.toObject(includeInstance, f),
    managedUserWhitelist: (f = msg.getManagedUserWhitelist()) && managed_user_whitelist_specifics_pb.ManagedUserWhitelistSpecifics.toObject(includeInstance, f),
    article: (f = msg.getArticle()) && article_specifics_pb.ArticleSpecifics.toObject(includeInstance, f),
    appList: (f = msg.getAppList()) && app_list_specifics_pb.AppListSpecifics.toObject(includeInstance, f),
    wifiCredential: (f = msg.getWifiCredential()) && wifi_credential_specifics_pb.WifiCredentialSpecifics.toObject(includeInstance, f),
    autofillWallet: (f = msg.getAutofillWallet()) && autofill_specifics_pb.AutofillWalletSpecifics.toObject(includeInstance, f),
    walletMetadata: (f = msg.getWalletMetadata()) && autofill_specifics_pb.WalletMetadataSpecifics.toObject(includeInstance, f),
    arcPackage: (f = msg.getArcPackage()) && arc_package_specifics_pb.ArcPackageSpecifics.toObject(includeInstance, f),
    printer: (f = msg.getPrinter()) && printer_specifics_pb.PrinterSpecifics.toObject(includeInstance, f),
    readingList: (f = msg.getReadingList()) && reading_list_specifics_pb.ReadingListSpecifics.toObject(includeInstance, f),
    userEvent: (f = msg.getUserEvent()) && user_event_specifics_pb.UserEventSpecifics.toObject(includeInstance, f),
    userConsent: (f = msg.getUserConsent()) && user_consent_specifics_pb.UserConsentSpecifics.toObject(includeInstance, f),
    mountainShare: (f = msg.getMountainShare()) && mountain_share_specifics_pb.MountainShareSpecifics.toObject(includeInstance, f),
    sendTabToSelf: (f = msg.getSendTabToSelf()) && send_tab_to_self_specifics_pb.SendTabToSelfSpecifics.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.EntitySpecifics}
 */
proto.sync_pb.EntitySpecifics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.EntitySpecifics;
  return proto.sync_pb.EntitySpecifics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.EntitySpecifics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.EntitySpecifics}
 */
proto.sync_pb.EntitySpecifics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new encryption_pb.EncryptedData;
      reader.readMessage(value,encryption_pb.EncryptedData.deserializeBinaryFromReader);
      msg.setEncrypted(value);
      break;
    case 31729:
      var value = new autofill_specifics_pb.AutofillSpecifics;
      reader.readMessage(value,autofill_specifics_pb.AutofillSpecifics.deserializeBinaryFromReader);
      msg.setAutofill(value);
      break;
    case 32904:
      var value = new bookmark_specifics_pb.BookmarkSpecifics;
      reader.readMessage(value,bookmark_specifics_pb.BookmarkSpecifics.deserializeBinaryFromReader);
      msg.setBookmark(value);
      break;
    case 37702:
      var value = new preference_specifics_pb.PreferenceSpecifics;
      reader.readMessage(value,preference_specifics_pb.PreferenceSpecifics.deserializeBinaryFromReader);
      msg.setPreference(value);
      break;
    case 40781:
      var value = new typed_url_specifics_pb.TypedUrlSpecifics;
      reader.readMessage(value,typed_url_specifics_pb.TypedUrlSpecifics.deserializeBinaryFromReader);
      msg.setTypedUrl(value);
      break;
    case 41210:
      var value = new theme_specifics_pb.ThemeSpecifics;
      reader.readMessage(value,theme_specifics_pb.ThemeSpecifics.deserializeBinaryFromReader);
      msg.setTheme(value);
      break;
    case 45184:
      var value = new app_notification_specifics_pb.AppNotification;
      reader.readMessage(value,app_notification_specifics_pb.AppNotification.deserializeBinaryFromReader);
      msg.setAppNotification(value);
      break;
    case 45873:
      var value = new password_specifics_pb.PasswordSpecifics;
      reader.readMessage(value,password_specifics_pb.PasswordSpecifics.deserializeBinaryFromReader);
      msg.setPassword(value);
      break;
    case 47745:
      var value = new nigori_specifics_pb.NigoriSpecifics;
      reader.readMessage(value,nigori_specifics_pb.NigoriSpecifics.deserializeBinaryFromReader);
      msg.setNigori(value);
      break;
    case 48119:
      var value = new extension_specifics_pb.ExtensionSpecifics;
      reader.readMessage(value,extension_specifics_pb.ExtensionSpecifics.deserializeBinaryFromReader);
      msg.setExtension$(value);
      break;
    case 48364:
      var value = new app_specifics_pb.AppSpecifics;
      reader.readMessage(value,app_specifics_pb.AppSpecifics.deserializeBinaryFromReader);
      msg.setApp(value);
      break;
    case 50119:
      var value = new session_specifics_pb.SessionSpecifics;
      reader.readMessage(value,session_specifics_pb.SessionSpecifics.deserializeBinaryFromReader);
      msg.setSession(value);
      break;
    case 63951:
      var value = new autofill_specifics_pb.AutofillProfileSpecifics;
      reader.readMessage(value,autofill_specifics_pb.AutofillProfileSpecifics.deserializeBinaryFromReader);
      msg.setAutofillProfile(value);
      break;
    case 88610:
      var value = new search_engine_specifics_pb.SearchEngineSpecifics;
      reader.readMessage(value,search_engine_specifics_pb.SearchEngineSpecifics.deserializeBinaryFromReader);
      msg.setSearchEngine(value);
      break;
    case 96159:
      var value = new extension_setting_specifics_pb.ExtensionSettingSpecifics;
      reader.readMessage(value,extension_setting_specifics_pb.ExtensionSettingSpecifics.deserializeBinaryFromReader);
      msg.setExtensionSetting(value);
      break;
    case 103656:
      var value = new app_setting_specifics_pb.AppSettingSpecifics;
      reader.readMessage(value,app_setting_specifics_pb.AppSettingSpecifics.deserializeBinaryFromReader);
      msg.setAppSetting(value);
      break;
    case 150251:
      var value = new history_delete_directive_specifics_pb.HistoryDeleteDirectiveSpecifics;
      reader.readMessage(value,history_delete_directive_specifics_pb.HistoryDeleteDirectiveSpecifics.deserializeBinaryFromReader);
      msg.setHistoryDeleteDirective(value);
      break;
    case 153108:
      var value = new synced_notification_specifics_pb.SyncedNotificationSpecifics;
      reader.readMessage(value,synced_notification_specifics_pb.SyncedNotificationSpecifics.deserializeBinaryFromReader);
      msg.setSyncedNotification(value);
      break;
    case 235816:
      var value = new synced_notification_app_info_specifics_pb.SyncedNotificationAppInfoSpecifics;
      reader.readMessage(value,synced_notification_app_info_specifics_pb.SyncedNotificationAppInfoSpecifics.deserializeBinaryFromReader);
      msg.setSyncedNotificationAppInfo(value);
      break;
    case 154522:
      var value = new device_info_specifics_pb.DeviceInfoSpecifics;
      reader.readMessage(value,device_info_specifics_pb.DeviceInfoSpecifics.deserializeBinaryFromReader);
      msg.setDeviceInfo(value);
      break;
    case 161496:
      var value = new experiments_specifics_pb.ExperimentsSpecifics;
      reader.readMessage(value,experiments_specifics_pb.ExperimentsSpecifics.deserializeBinaryFromReader);
      msg.setExperiments(value);
      break;
    case 163425:
      var value = new priority_preference_specifics_pb.PriorityPreferenceSpecifics;
      reader.readMessage(value,priority_preference_specifics_pb.PriorityPreferenceSpecifics.deserializeBinaryFromReader);
      msg.setPriorityPreference(value);
      break;
    case 170540:
      var value = new dictionary_specifics_pb.DictionarySpecifics;
      reader.readMessage(value,dictionary_specifics_pb.DictionarySpecifics.deserializeBinaryFromReader);
      msg.setDictionary(value);
      break;
    case 181534:
      var value = new favicon_tracking_specifics_pb.FaviconTrackingSpecifics;
      reader.readMessage(value,favicon_tracking_specifics_pb.FaviconTrackingSpecifics.deserializeBinaryFromReader);
      msg.setFaviconTracking(value);
      break;
    case 182019:
      var value = new favicon_image_specifics_pb.FaviconImageSpecifics;
      reader.readMessage(value,favicon_image_specifics_pb.FaviconImageSpecifics.deserializeBinaryFromReader);
      msg.setFaviconImage(value);
      break;
    case 186662:
      var value = new managed_user_setting_specifics_pb.ManagedUserSettingSpecifics;
      reader.readMessage(value,managed_user_setting_specifics_pb.ManagedUserSettingSpecifics.deserializeBinaryFromReader);
      msg.setManagedUserSetting(value);
      break;
    case 194582:
      var value = new managed_user_specifics_pb.ManagedUserSpecifics;
      reader.readMessage(value,managed_user_specifics_pb.ManagedUserSpecifics.deserializeBinaryFromReader);
      msg.setManagedUser(value);
      break;
    case 202026:
      var value = new managed_user_shared_setting_specifics_pb.ManagedUserSharedSettingSpecifics;
      reader.readMessage(value,managed_user_shared_setting_specifics_pb.ManagedUserSharedSettingSpecifics.deserializeBinaryFromReader);
      msg.setManagedUserSharedSetting(value);
      break;
    case 306060:
      var value = new managed_user_whitelist_specifics_pb.ManagedUserWhitelistSpecifics;
      reader.readMessage(value,managed_user_whitelist_specifics_pb.ManagedUserWhitelistSpecifics.deserializeBinaryFromReader);
      msg.setManagedUserWhitelist(value);
      break;
    case 223759:
      var value = new article_specifics_pb.ArticleSpecifics;
      reader.readMessage(value,article_specifics_pb.ArticleSpecifics.deserializeBinaryFromReader);
      msg.setArticle(value);
      break;
    case 229170:
      var value = new app_list_specifics_pb.AppListSpecifics;
      reader.readMessage(value,app_list_specifics_pb.AppListSpecifics.deserializeBinaryFromReader);
      msg.setAppList(value);
      break;
    case 218175:
      var value = new wifi_credential_specifics_pb.WifiCredentialSpecifics;
      reader.readMessage(value,wifi_credential_specifics_pb.WifiCredentialSpecifics.deserializeBinaryFromReader);
      msg.setWifiCredential(value);
      break;
    case 306270:
      var value = new autofill_specifics_pb.AutofillWalletSpecifics;
      reader.readMessage(value,autofill_specifics_pb.AutofillWalletSpecifics.deserializeBinaryFromReader);
      msg.setAutofillWallet(value);
      break;
    case 330441:
      var value = new autofill_specifics_pb.WalletMetadataSpecifics;
      reader.readMessage(value,autofill_specifics_pb.WalletMetadataSpecifics.deserializeBinaryFromReader);
      msg.setWalletMetadata(value);
      break;
    case 340906:
      var value = new arc_package_specifics_pb.ArcPackageSpecifics;
      reader.readMessage(value,arc_package_specifics_pb.ArcPackageSpecifics.deserializeBinaryFromReader);
      msg.setArcPackage(value);
      break;
    case 410745:
      var value = new printer_specifics_pb.PrinterSpecifics;
      reader.readMessage(value,printer_specifics_pb.PrinterSpecifics.deserializeBinaryFromReader);
      msg.setPrinter(value);
      break;
    case 411028:
      var value = new reading_list_specifics_pb.ReadingListSpecifics;
      reader.readMessage(value,reading_list_specifics_pb.ReadingListSpecifics.deserializeBinaryFromReader);
      msg.setReadingList(value);
      break;
    case 455206:
      var value = new user_event_specifics_pb.UserEventSpecifics;
      reader.readMessage(value,user_event_specifics_pb.UserEventSpecifics.deserializeBinaryFromReader);
      msg.setUserEvent(value);
      break;
    case 556014:
      var value = new user_consent_specifics_pb.UserConsentSpecifics;
      reader.readMessage(value,user_consent_specifics_pb.UserConsentSpecifics.deserializeBinaryFromReader);
      msg.setUserConsent(value);
      break;
    case 545005:
      var value = new mountain_share_specifics_pb.MountainShareSpecifics;
      reader.readMessage(value,mountain_share_specifics_pb.MountainShareSpecifics.deserializeBinaryFromReader);
      msg.setMountainShare(value);
      break;
    case 601980:
      var value = new send_tab_to_self_specifics_pb.SendTabToSelfSpecifics;
      reader.readMessage(value,send_tab_to_self_specifics_pb.SendTabToSelfSpecifics.deserializeBinaryFromReader);
      msg.setSendTabToSelf(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.EntitySpecifics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.EntitySpecifics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.EntitySpecifics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EntitySpecifics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEncrypted();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      encryption_pb.EncryptedData.serializeBinaryToWriter
    );
  }
  f = message.getAutofill();
  if (f != null) {
    writer.writeMessage(
      31729,
      f,
      autofill_specifics_pb.AutofillSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getBookmark();
  if (f != null) {
    writer.writeMessage(
      32904,
      f,
      bookmark_specifics_pb.BookmarkSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getPreference();
  if (f != null) {
    writer.writeMessage(
      37702,
      f,
      preference_specifics_pb.PreferenceSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getTypedUrl();
  if (f != null) {
    writer.writeMessage(
      40781,
      f,
      typed_url_specifics_pb.TypedUrlSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getTheme();
  if (f != null) {
    writer.writeMessage(
      41210,
      f,
      theme_specifics_pb.ThemeSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getAppNotification();
  if (f != null) {
    writer.writeMessage(
      45184,
      f,
      app_notification_specifics_pb.AppNotification.serializeBinaryToWriter
    );
  }
  f = message.getPassword();
  if (f != null) {
    writer.writeMessage(
      45873,
      f,
      password_specifics_pb.PasswordSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getNigori();
  if (f != null) {
    writer.writeMessage(
      47745,
      f,
      nigori_specifics_pb.NigoriSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getExtension$();
  if (f != null) {
    writer.writeMessage(
      48119,
      f,
      extension_specifics_pb.ExtensionSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getApp();
  if (f != null) {
    writer.writeMessage(
      48364,
      f,
      app_specifics_pb.AppSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getSession();
  if (f != null) {
    writer.writeMessage(
      50119,
      f,
      session_specifics_pb.SessionSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getAutofillProfile();
  if (f != null) {
    writer.writeMessage(
      63951,
      f,
      autofill_specifics_pb.AutofillProfileSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getSearchEngine();
  if (f != null) {
    writer.writeMessage(
      88610,
      f,
      search_engine_specifics_pb.SearchEngineSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getExtensionSetting();
  if (f != null) {
    writer.writeMessage(
      96159,
      f,
      extension_setting_specifics_pb.ExtensionSettingSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getAppSetting();
  if (f != null) {
    writer.writeMessage(
      103656,
      f,
      app_setting_specifics_pb.AppSettingSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getHistoryDeleteDirective();
  if (f != null) {
    writer.writeMessage(
      150251,
      f,
      history_delete_directive_specifics_pb.HistoryDeleteDirectiveSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getSyncedNotification();
  if (f != null) {
    writer.writeMessage(
      153108,
      f,
      synced_notification_specifics_pb.SyncedNotificationSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getSyncedNotificationAppInfo();
  if (f != null) {
    writer.writeMessage(
      235816,
      f,
      synced_notification_app_info_specifics_pb.SyncedNotificationAppInfoSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getDeviceInfo();
  if (f != null) {
    writer.writeMessage(
      154522,
      f,
      device_info_specifics_pb.DeviceInfoSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getExperiments();
  if (f != null) {
    writer.writeMessage(
      161496,
      f,
      experiments_specifics_pb.ExperimentsSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getPriorityPreference();
  if (f != null) {
    writer.writeMessage(
      163425,
      f,
      priority_preference_specifics_pb.PriorityPreferenceSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getDictionary();
  if (f != null) {
    writer.writeMessage(
      170540,
      f,
      dictionary_specifics_pb.DictionarySpecifics.serializeBinaryToWriter
    );
  }
  f = message.getFaviconTracking();
  if (f != null) {
    writer.writeMessage(
      181534,
      f,
      favicon_tracking_specifics_pb.FaviconTrackingSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getFaviconImage();
  if (f != null) {
    writer.writeMessage(
      182019,
      f,
      favicon_image_specifics_pb.FaviconImageSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getManagedUserSetting();
  if (f != null) {
    writer.writeMessage(
      186662,
      f,
      managed_user_setting_specifics_pb.ManagedUserSettingSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getManagedUser();
  if (f != null) {
    writer.writeMessage(
      194582,
      f,
      managed_user_specifics_pb.ManagedUserSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getManagedUserSharedSetting();
  if (f != null) {
    writer.writeMessage(
      202026,
      f,
      managed_user_shared_setting_specifics_pb.ManagedUserSharedSettingSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getManagedUserWhitelist();
  if (f != null) {
    writer.writeMessage(
      306060,
      f,
      managed_user_whitelist_specifics_pb.ManagedUserWhitelistSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getArticle();
  if (f != null) {
    writer.writeMessage(
      223759,
      f,
      article_specifics_pb.ArticleSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getAppList();
  if (f != null) {
    writer.writeMessage(
      229170,
      f,
      app_list_specifics_pb.AppListSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getWifiCredential();
  if (f != null) {
    writer.writeMessage(
      218175,
      f,
      wifi_credential_specifics_pb.WifiCredentialSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getAutofillWallet();
  if (f != null) {
    writer.writeMessage(
      306270,
      f,
      autofill_specifics_pb.AutofillWalletSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getWalletMetadata();
  if (f != null) {
    writer.writeMessage(
      330441,
      f,
      autofill_specifics_pb.WalletMetadataSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getArcPackage();
  if (f != null) {
    writer.writeMessage(
      340906,
      f,
      arc_package_specifics_pb.ArcPackageSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getPrinter();
  if (f != null) {
    writer.writeMessage(
      410745,
      f,
      printer_specifics_pb.PrinterSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getReadingList();
  if (f != null) {
    writer.writeMessage(
      411028,
      f,
      reading_list_specifics_pb.ReadingListSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getUserEvent();
  if (f != null) {
    writer.writeMessage(
      455206,
      f,
      user_event_specifics_pb.UserEventSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getUserConsent();
  if (f != null) {
    writer.writeMessage(
      556014,
      f,
      user_consent_specifics_pb.UserConsentSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getMountainShare();
  if (f != null) {
    writer.writeMessage(
      545005,
      f,
      mountain_share_specifics_pb.MountainShareSpecifics.serializeBinaryToWriter
    );
  }
  f = message.getSendTabToSelf();
  if (f != null) {
    writer.writeMessage(
      601980,
      f,
      send_tab_to_self_specifics_pb.SendTabToSelfSpecifics.serializeBinaryToWriter
    );
  }
};


/**
 * optional EncryptedData encrypted = 1;
 * @return {?proto.sync_pb.EncryptedData}
 */
proto.sync_pb.EntitySpecifics.prototype.getEncrypted = function() {
  return /** @type{?proto.sync_pb.EncryptedData} */ (
    jspb.Message.getWrapperField(this, encryption_pb.EncryptedData, 1));
};


/**
 * @param {?proto.sync_pb.EncryptedData|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setEncrypted = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearEncrypted = function() {
  return this.setEncrypted(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasEncrypted = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional AutofillSpecifics autofill = 31729;
 * @return {?proto.sync_pb.AutofillSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getAutofill = function() {
  return /** @type{?proto.sync_pb.AutofillSpecifics} */ (
    jspb.Message.getWrapperField(this, autofill_specifics_pb.AutofillSpecifics, 31729));
};


/**
 * @param {?proto.sync_pb.AutofillSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAutofill = function(value) {
  return jspb.Message.setOneofWrapperField(this, 31729, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAutofill = function() {
  return this.setAutofill(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAutofill = function() {
  return jspb.Message.getField(this, 31729) != null;
};


/**
 * optional BookmarkSpecifics bookmark = 32904;
 * @return {?proto.sync_pb.BookmarkSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getBookmark = function() {
  return /** @type{?proto.sync_pb.BookmarkSpecifics} */ (
    jspb.Message.getWrapperField(this, bookmark_specifics_pb.BookmarkSpecifics, 32904));
};


/**
 * @param {?proto.sync_pb.BookmarkSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setBookmark = function(value) {
  return jspb.Message.setOneofWrapperField(this, 32904, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearBookmark = function() {
  return this.setBookmark(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasBookmark = function() {
  return jspb.Message.getField(this, 32904) != null;
};


/**
 * optional PreferenceSpecifics preference = 37702;
 * @return {?proto.sync_pb.PreferenceSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getPreference = function() {
  return /** @type{?proto.sync_pb.PreferenceSpecifics} */ (
    jspb.Message.getWrapperField(this, preference_specifics_pb.PreferenceSpecifics, 37702));
};


/**
 * @param {?proto.sync_pb.PreferenceSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setPreference = function(value) {
  return jspb.Message.setOneofWrapperField(this, 37702, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearPreference = function() {
  return this.setPreference(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasPreference = function() {
  return jspb.Message.getField(this, 37702) != null;
};


/**
 * optional TypedUrlSpecifics typed_url = 40781;
 * @return {?proto.sync_pb.TypedUrlSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getTypedUrl = function() {
  return /** @type{?proto.sync_pb.TypedUrlSpecifics} */ (
    jspb.Message.getWrapperField(this, typed_url_specifics_pb.TypedUrlSpecifics, 40781));
};


/**
 * @param {?proto.sync_pb.TypedUrlSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setTypedUrl = function(value) {
  return jspb.Message.setOneofWrapperField(this, 40781, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearTypedUrl = function() {
  return this.setTypedUrl(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasTypedUrl = function() {
  return jspb.Message.getField(this, 40781) != null;
};


/**
 * optional ThemeSpecifics theme = 41210;
 * @return {?proto.sync_pb.ThemeSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getTheme = function() {
  return /** @type{?proto.sync_pb.ThemeSpecifics} */ (
    jspb.Message.getWrapperField(this, theme_specifics_pb.ThemeSpecifics, 41210));
};


/**
 * @param {?proto.sync_pb.ThemeSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setTheme = function(value) {
  return jspb.Message.setOneofWrapperField(this, 41210, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearTheme = function() {
  return this.setTheme(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasTheme = function() {
  return jspb.Message.getField(this, 41210) != null;
};


/**
 * optional AppNotification app_notification = 45184;
 * @return {?proto.sync_pb.AppNotification}
 */
proto.sync_pb.EntitySpecifics.prototype.getAppNotification = function() {
  return /** @type{?proto.sync_pb.AppNotification} */ (
    jspb.Message.getWrapperField(this, app_notification_specifics_pb.AppNotification, 45184));
};


/**
 * @param {?proto.sync_pb.AppNotification|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAppNotification = function(value) {
  return jspb.Message.setOneofWrapperField(this, 45184, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAppNotification = function() {
  return this.setAppNotification(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAppNotification = function() {
  return jspb.Message.getField(this, 45184) != null;
};


/**
 * optional PasswordSpecifics password = 45873;
 * @return {?proto.sync_pb.PasswordSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getPassword = function() {
  return /** @type{?proto.sync_pb.PasswordSpecifics} */ (
    jspb.Message.getWrapperField(this, password_specifics_pb.PasswordSpecifics, 45873));
};


/**
 * @param {?proto.sync_pb.PasswordSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setPassword = function(value) {
  return jspb.Message.setOneofWrapperField(this, 45873, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearPassword = function() {
  return this.setPassword(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasPassword = function() {
  return jspb.Message.getField(this, 45873) != null;
};


/**
 * optional NigoriSpecifics nigori = 47745;
 * @return {?proto.sync_pb.NigoriSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getNigori = function() {
  return /** @type{?proto.sync_pb.NigoriSpecifics} */ (
    jspb.Message.getWrapperField(this, nigori_specifics_pb.NigoriSpecifics, 47745));
};


/**
 * @param {?proto.sync_pb.NigoriSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setNigori = function(value) {
  return jspb.Message.setOneofWrapperField(this, 47745, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearNigori = function() {
  return this.setNigori(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasNigori = function() {
  return jspb.Message.getField(this, 47745) != null;
};


/**
 * optional ExtensionSpecifics extension = 48119;
 * @return {?proto.sync_pb.ExtensionSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getExtension$ = function() {
  return /** @type{?proto.sync_pb.ExtensionSpecifics} */ (
    jspb.Message.getWrapperField(this, extension_specifics_pb.ExtensionSpecifics, 48119));
};


/**
 * @param {?proto.sync_pb.ExtensionSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setExtension$ = function(value) {
  return jspb.Message.setOneofWrapperField(this, 48119, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearExtension$ = function() {
  return this.setExtension$(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasExtension$ = function() {
  return jspb.Message.getField(this, 48119) != null;
};


/**
 * optional AppSpecifics app = 48364;
 * @return {?proto.sync_pb.AppSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getApp = function() {
  return /** @type{?proto.sync_pb.AppSpecifics} */ (
    jspb.Message.getWrapperField(this, app_specifics_pb.AppSpecifics, 48364));
};


/**
 * @param {?proto.sync_pb.AppSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setApp = function(value) {
  return jspb.Message.setOneofWrapperField(this, 48364, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearApp = function() {
  return this.setApp(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasApp = function() {
  return jspb.Message.getField(this, 48364) != null;
};


/**
 * optional SessionSpecifics session = 50119;
 * @return {?proto.sync_pb.SessionSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getSession = function() {
  return /** @type{?proto.sync_pb.SessionSpecifics} */ (
    jspb.Message.getWrapperField(this, session_specifics_pb.SessionSpecifics, 50119));
};


/**
 * @param {?proto.sync_pb.SessionSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setSession = function(value) {
  return jspb.Message.setOneofWrapperField(this, 50119, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearSession = function() {
  return this.setSession(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasSession = function() {
  return jspb.Message.getField(this, 50119) != null;
};


/**
 * optional AutofillProfileSpecifics autofill_profile = 63951;
 * @return {?proto.sync_pb.AutofillProfileSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getAutofillProfile = function() {
  return /** @type{?proto.sync_pb.AutofillProfileSpecifics} */ (
    jspb.Message.getWrapperField(this, autofill_specifics_pb.AutofillProfileSpecifics, 63951));
};


/**
 * @param {?proto.sync_pb.AutofillProfileSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAutofillProfile = function(value) {
  return jspb.Message.setOneofWrapperField(this, 63951, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAutofillProfile = function() {
  return this.setAutofillProfile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAutofillProfile = function() {
  return jspb.Message.getField(this, 63951) != null;
};


/**
 * optional SearchEngineSpecifics search_engine = 88610;
 * @return {?proto.sync_pb.SearchEngineSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getSearchEngine = function() {
  return /** @type{?proto.sync_pb.SearchEngineSpecifics} */ (
    jspb.Message.getWrapperField(this, search_engine_specifics_pb.SearchEngineSpecifics, 88610));
};


/**
 * @param {?proto.sync_pb.SearchEngineSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setSearchEngine = function(value) {
  return jspb.Message.setOneofWrapperField(this, 88610, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearSearchEngine = function() {
  return this.setSearchEngine(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasSearchEngine = function() {
  return jspb.Message.getField(this, 88610) != null;
};


/**
 * optional ExtensionSettingSpecifics extension_setting = 96159;
 * @return {?proto.sync_pb.ExtensionSettingSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getExtensionSetting = function() {
  return /** @type{?proto.sync_pb.ExtensionSettingSpecifics} */ (
    jspb.Message.getWrapperField(this, extension_setting_specifics_pb.ExtensionSettingSpecifics, 96159));
};


/**
 * @param {?proto.sync_pb.ExtensionSettingSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setExtensionSetting = function(value) {
  return jspb.Message.setOneofWrapperField(this, 96159, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearExtensionSetting = function() {
  return this.setExtensionSetting(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasExtensionSetting = function() {
  return jspb.Message.getField(this, 96159) != null;
};


/**
 * optional AppSettingSpecifics app_setting = 103656;
 * @return {?proto.sync_pb.AppSettingSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getAppSetting = function() {
  return /** @type{?proto.sync_pb.AppSettingSpecifics} */ (
    jspb.Message.getWrapperField(this, app_setting_specifics_pb.AppSettingSpecifics, 103656));
};


/**
 * @param {?proto.sync_pb.AppSettingSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAppSetting = function(value) {
  return jspb.Message.setOneofWrapperField(this, 103656, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAppSetting = function() {
  return this.setAppSetting(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAppSetting = function() {
  return jspb.Message.getField(this, 103656) != null;
};


/**
 * optional HistoryDeleteDirectiveSpecifics history_delete_directive = 150251;
 * @return {?proto.sync_pb.HistoryDeleteDirectiveSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getHistoryDeleteDirective = function() {
  return /** @type{?proto.sync_pb.HistoryDeleteDirectiveSpecifics} */ (
    jspb.Message.getWrapperField(this, history_delete_directive_specifics_pb.HistoryDeleteDirectiveSpecifics, 150251));
};


/**
 * @param {?proto.sync_pb.HistoryDeleteDirectiveSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setHistoryDeleteDirective = function(value) {
  return jspb.Message.setOneofWrapperField(this, 150251, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearHistoryDeleteDirective = function() {
  return this.setHistoryDeleteDirective(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasHistoryDeleteDirective = function() {
  return jspb.Message.getField(this, 150251) != null;
};


/**
 * optional SyncedNotificationSpecifics synced_notification = 153108;
 * @return {?proto.sync_pb.SyncedNotificationSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getSyncedNotification = function() {
  return /** @type{?proto.sync_pb.SyncedNotificationSpecifics} */ (
    jspb.Message.getWrapperField(this, synced_notification_specifics_pb.SyncedNotificationSpecifics, 153108));
};


/**
 * @param {?proto.sync_pb.SyncedNotificationSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setSyncedNotification = function(value) {
  return jspb.Message.setOneofWrapperField(this, 153108, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearSyncedNotification = function() {
  return this.setSyncedNotification(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasSyncedNotification = function() {
  return jspb.Message.getField(this, 153108) != null;
};


/**
 * optional SyncedNotificationAppInfoSpecifics synced_notification_app_info = 235816;
 * @return {?proto.sync_pb.SyncedNotificationAppInfoSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getSyncedNotificationAppInfo = function() {
  return /** @type{?proto.sync_pb.SyncedNotificationAppInfoSpecifics} */ (
    jspb.Message.getWrapperField(this, synced_notification_app_info_specifics_pb.SyncedNotificationAppInfoSpecifics, 235816));
};


/**
 * @param {?proto.sync_pb.SyncedNotificationAppInfoSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setSyncedNotificationAppInfo = function(value) {
  return jspb.Message.setOneofWrapperField(this, 235816, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearSyncedNotificationAppInfo = function() {
  return this.setSyncedNotificationAppInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasSyncedNotificationAppInfo = function() {
  return jspb.Message.getField(this, 235816) != null;
};


/**
 * optional DeviceInfoSpecifics device_info = 154522;
 * @return {?proto.sync_pb.DeviceInfoSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getDeviceInfo = function() {
  return /** @type{?proto.sync_pb.DeviceInfoSpecifics} */ (
    jspb.Message.getWrapperField(this, device_info_specifics_pb.DeviceInfoSpecifics, 154522));
};


/**
 * @param {?proto.sync_pb.DeviceInfoSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setDeviceInfo = function(value) {
  return jspb.Message.setOneofWrapperField(this, 154522, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearDeviceInfo = function() {
  return this.setDeviceInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasDeviceInfo = function() {
  return jspb.Message.getField(this, 154522) != null;
};


/**
 * optional ExperimentsSpecifics experiments = 161496;
 * @return {?proto.sync_pb.ExperimentsSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getExperiments = function() {
  return /** @type{?proto.sync_pb.ExperimentsSpecifics} */ (
    jspb.Message.getWrapperField(this, experiments_specifics_pb.ExperimentsSpecifics, 161496));
};


/**
 * @param {?proto.sync_pb.ExperimentsSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setExperiments = function(value) {
  return jspb.Message.setOneofWrapperField(this, 161496, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearExperiments = function() {
  return this.setExperiments(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasExperiments = function() {
  return jspb.Message.getField(this, 161496) != null;
};


/**
 * optional PriorityPreferenceSpecifics priority_preference = 163425;
 * @return {?proto.sync_pb.PriorityPreferenceSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getPriorityPreference = function() {
  return /** @type{?proto.sync_pb.PriorityPreferenceSpecifics} */ (
    jspb.Message.getWrapperField(this, priority_preference_specifics_pb.PriorityPreferenceSpecifics, 163425));
};


/**
 * @param {?proto.sync_pb.PriorityPreferenceSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setPriorityPreference = function(value) {
  return jspb.Message.setOneofWrapperField(this, 163425, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearPriorityPreference = function() {
  return this.setPriorityPreference(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasPriorityPreference = function() {
  return jspb.Message.getField(this, 163425) != null;
};


/**
 * optional DictionarySpecifics dictionary = 170540;
 * @return {?proto.sync_pb.DictionarySpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getDictionary = function() {
  return /** @type{?proto.sync_pb.DictionarySpecifics} */ (
    jspb.Message.getWrapperField(this, dictionary_specifics_pb.DictionarySpecifics, 170540));
};


/**
 * @param {?proto.sync_pb.DictionarySpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setDictionary = function(value) {
  return jspb.Message.setOneofWrapperField(this, 170540, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearDictionary = function() {
  return this.setDictionary(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasDictionary = function() {
  return jspb.Message.getField(this, 170540) != null;
};


/**
 * optional FaviconTrackingSpecifics favicon_tracking = 181534;
 * @return {?proto.sync_pb.FaviconTrackingSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getFaviconTracking = function() {
  return /** @type{?proto.sync_pb.FaviconTrackingSpecifics} */ (
    jspb.Message.getWrapperField(this, favicon_tracking_specifics_pb.FaviconTrackingSpecifics, 181534));
};


/**
 * @param {?proto.sync_pb.FaviconTrackingSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setFaviconTracking = function(value) {
  return jspb.Message.setOneofWrapperField(this, 181534, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearFaviconTracking = function() {
  return this.setFaviconTracking(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasFaviconTracking = function() {
  return jspb.Message.getField(this, 181534) != null;
};


/**
 * optional FaviconImageSpecifics favicon_image = 182019;
 * @return {?proto.sync_pb.FaviconImageSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getFaviconImage = function() {
  return /** @type{?proto.sync_pb.FaviconImageSpecifics} */ (
    jspb.Message.getWrapperField(this, favicon_image_specifics_pb.FaviconImageSpecifics, 182019));
};


/**
 * @param {?proto.sync_pb.FaviconImageSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setFaviconImage = function(value) {
  return jspb.Message.setOneofWrapperField(this, 182019, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearFaviconImage = function() {
  return this.setFaviconImage(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasFaviconImage = function() {
  return jspb.Message.getField(this, 182019) != null;
};


/**
 * optional ManagedUserSettingSpecifics managed_user_setting = 186662;
 * @return {?proto.sync_pb.ManagedUserSettingSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getManagedUserSetting = function() {
  return /** @type{?proto.sync_pb.ManagedUserSettingSpecifics} */ (
    jspb.Message.getWrapperField(this, managed_user_setting_specifics_pb.ManagedUserSettingSpecifics, 186662));
};


/**
 * @param {?proto.sync_pb.ManagedUserSettingSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setManagedUserSetting = function(value) {
  return jspb.Message.setOneofWrapperField(this, 186662, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearManagedUserSetting = function() {
  return this.setManagedUserSetting(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasManagedUserSetting = function() {
  return jspb.Message.getField(this, 186662) != null;
};


/**
 * optional ManagedUserSpecifics managed_user = 194582;
 * @return {?proto.sync_pb.ManagedUserSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getManagedUser = function() {
  return /** @type{?proto.sync_pb.ManagedUserSpecifics} */ (
    jspb.Message.getWrapperField(this, managed_user_specifics_pb.ManagedUserSpecifics, 194582));
};


/**
 * @param {?proto.sync_pb.ManagedUserSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setManagedUser = function(value) {
  return jspb.Message.setOneofWrapperField(this, 194582, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearManagedUser = function() {
  return this.setManagedUser(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasManagedUser = function() {
  return jspb.Message.getField(this, 194582) != null;
};


/**
 * optional ManagedUserSharedSettingSpecifics managed_user_shared_setting = 202026;
 * @return {?proto.sync_pb.ManagedUserSharedSettingSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getManagedUserSharedSetting = function() {
  return /** @type{?proto.sync_pb.ManagedUserSharedSettingSpecifics} */ (
    jspb.Message.getWrapperField(this, managed_user_shared_setting_specifics_pb.ManagedUserSharedSettingSpecifics, 202026));
};


/**
 * @param {?proto.sync_pb.ManagedUserSharedSettingSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setManagedUserSharedSetting = function(value) {
  return jspb.Message.setOneofWrapperField(this, 202026, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearManagedUserSharedSetting = function() {
  return this.setManagedUserSharedSetting(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasManagedUserSharedSetting = function() {
  return jspb.Message.getField(this, 202026) != null;
};


/**
 * optional ManagedUserWhitelistSpecifics managed_user_whitelist = 306060;
 * @return {?proto.sync_pb.ManagedUserWhitelistSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getManagedUserWhitelist = function() {
  return /** @type{?proto.sync_pb.ManagedUserWhitelistSpecifics} */ (
    jspb.Message.getWrapperField(this, managed_user_whitelist_specifics_pb.ManagedUserWhitelistSpecifics, 306060));
};


/**
 * @param {?proto.sync_pb.ManagedUserWhitelistSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setManagedUserWhitelist = function(value) {
  return jspb.Message.setOneofWrapperField(this, 306060, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearManagedUserWhitelist = function() {
  return this.setManagedUserWhitelist(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasManagedUserWhitelist = function() {
  return jspb.Message.getField(this, 306060) != null;
};


/**
 * optional ArticleSpecifics article = 223759;
 * @return {?proto.sync_pb.ArticleSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getArticle = function() {
  return /** @type{?proto.sync_pb.ArticleSpecifics} */ (
    jspb.Message.getWrapperField(this, article_specifics_pb.ArticleSpecifics, 223759));
};


/**
 * @param {?proto.sync_pb.ArticleSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setArticle = function(value) {
  return jspb.Message.setOneofWrapperField(this, 223759, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearArticle = function() {
  return this.setArticle(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasArticle = function() {
  return jspb.Message.getField(this, 223759) != null;
};


/**
 * optional AppListSpecifics app_list = 229170;
 * @return {?proto.sync_pb.AppListSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getAppList = function() {
  return /** @type{?proto.sync_pb.AppListSpecifics} */ (
    jspb.Message.getWrapperField(this, app_list_specifics_pb.AppListSpecifics, 229170));
};


/**
 * @param {?proto.sync_pb.AppListSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAppList = function(value) {
  return jspb.Message.setOneofWrapperField(this, 229170, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAppList = function() {
  return this.setAppList(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAppList = function() {
  return jspb.Message.getField(this, 229170) != null;
};


/**
 * optional WifiCredentialSpecifics wifi_credential = 218175;
 * @return {?proto.sync_pb.WifiCredentialSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getWifiCredential = function() {
  return /** @type{?proto.sync_pb.WifiCredentialSpecifics} */ (
    jspb.Message.getWrapperField(this, wifi_credential_specifics_pb.WifiCredentialSpecifics, 218175));
};


/**
 * @param {?proto.sync_pb.WifiCredentialSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setWifiCredential = function(value) {
  return jspb.Message.setOneofWrapperField(this, 218175, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearWifiCredential = function() {
  return this.setWifiCredential(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasWifiCredential = function() {
  return jspb.Message.getField(this, 218175) != null;
};


/**
 * optional AutofillWalletSpecifics autofill_wallet = 306270;
 * @return {?proto.sync_pb.AutofillWalletSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getAutofillWallet = function() {
  return /** @type{?proto.sync_pb.AutofillWalletSpecifics} */ (
    jspb.Message.getWrapperField(this, autofill_specifics_pb.AutofillWalletSpecifics, 306270));
};


/**
 * @param {?proto.sync_pb.AutofillWalletSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setAutofillWallet = function(value) {
  return jspb.Message.setOneofWrapperField(this, 306270, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearAutofillWallet = function() {
  return this.setAutofillWallet(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasAutofillWallet = function() {
  return jspb.Message.getField(this, 306270) != null;
};


/**
 * optional WalletMetadataSpecifics wallet_metadata = 330441;
 * @return {?proto.sync_pb.WalletMetadataSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getWalletMetadata = function() {
  return /** @type{?proto.sync_pb.WalletMetadataSpecifics} */ (
    jspb.Message.getWrapperField(this, autofill_specifics_pb.WalletMetadataSpecifics, 330441));
};


/**
 * @param {?proto.sync_pb.WalletMetadataSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setWalletMetadata = function(value) {
  return jspb.Message.setOneofWrapperField(this, 330441, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearWalletMetadata = function() {
  return this.setWalletMetadata(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasWalletMetadata = function() {
  return jspb.Message.getField(this, 330441) != null;
};


/**
 * optional ArcPackageSpecifics arc_package = 340906;
 * @return {?proto.sync_pb.ArcPackageSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getArcPackage = function() {
  return /** @type{?proto.sync_pb.ArcPackageSpecifics} */ (
    jspb.Message.getWrapperField(this, arc_package_specifics_pb.ArcPackageSpecifics, 340906));
};


/**
 * @param {?proto.sync_pb.ArcPackageSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setArcPackage = function(value) {
  return jspb.Message.setOneofWrapperField(this, 340906, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearArcPackage = function() {
  return this.setArcPackage(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasArcPackage = function() {
  return jspb.Message.getField(this, 340906) != null;
};


/**
 * optional PrinterSpecifics printer = 410745;
 * @return {?proto.sync_pb.PrinterSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getPrinter = function() {
  return /** @type{?proto.sync_pb.PrinterSpecifics} */ (
    jspb.Message.getWrapperField(this, printer_specifics_pb.PrinterSpecifics, 410745));
};


/**
 * @param {?proto.sync_pb.PrinterSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setPrinter = function(value) {
  return jspb.Message.setOneofWrapperField(this, 410745, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearPrinter = function() {
  return this.setPrinter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasPrinter = function() {
  return jspb.Message.getField(this, 410745) != null;
};


/**
 * optional ReadingListSpecifics reading_list = 411028;
 * @return {?proto.sync_pb.ReadingListSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getReadingList = function() {
  return /** @type{?proto.sync_pb.ReadingListSpecifics} */ (
    jspb.Message.getWrapperField(this, reading_list_specifics_pb.ReadingListSpecifics, 411028));
};


/**
 * @param {?proto.sync_pb.ReadingListSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setReadingList = function(value) {
  return jspb.Message.setOneofWrapperField(this, 411028, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearReadingList = function() {
  return this.setReadingList(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasReadingList = function() {
  return jspb.Message.getField(this, 411028) != null;
};


/**
 * optional UserEventSpecifics user_event = 455206;
 * @return {?proto.sync_pb.UserEventSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getUserEvent = function() {
  return /** @type{?proto.sync_pb.UserEventSpecifics} */ (
    jspb.Message.getWrapperField(this, user_event_specifics_pb.UserEventSpecifics, 455206));
};


/**
 * @param {?proto.sync_pb.UserEventSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setUserEvent = function(value) {
  return jspb.Message.setOneofWrapperField(this, 455206, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearUserEvent = function() {
  return this.setUserEvent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasUserEvent = function() {
  return jspb.Message.getField(this, 455206) != null;
};


/**
 * optional UserConsentSpecifics user_consent = 556014;
 * @return {?proto.sync_pb.UserConsentSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getUserConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentSpecifics} */ (
    jspb.Message.getWrapperField(this, user_consent_specifics_pb.UserConsentSpecifics, 556014));
};


/**
 * @param {?proto.sync_pb.UserConsentSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setUserConsent = function(value) {
  return jspb.Message.setOneofWrapperField(this, 556014, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearUserConsent = function() {
  return this.setUserConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasUserConsent = function() {
  return jspb.Message.getField(this, 556014) != null;
};


/**
 * optional MountainShareSpecifics mountain_share = 545005;
 * @return {?proto.sync_pb.MountainShareSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getMountainShare = function() {
  return /** @type{?proto.sync_pb.MountainShareSpecifics} */ (
    jspb.Message.getWrapperField(this, mountain_share_specifics_pb.MountainShareSpecifics, 545005));
};


/**
 * @param {?proto.sync_pb.MountainShareSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setMountainShare = function(value) {
  return jspb.Message.setOneofWrapperField(this, 545005, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearMountainShare = function() {
  return this.setMountainShare(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasMountainShare = function() {
  return jspb.Message.getField(this, 545005) != null;
};


/**
 * optional SendTabToSelfSpecifics send_tab_to_self = 601980;
 * @return {?proto.sync_pb.SendTabToSelfSpecifics}
 */
proto.sync_pb.EntitySpecifics.prototype.getSendTabToSelf = function() {
  return /** @type{?proto.sync_pb.SendTabToSelfSpecifics} */ (
    jspb.Message.getWrapperField(this, send_tab_to_self_specifics_pb.SendTabToSelfSpecifics, 601980));
};


/**
 * @param {?proto.sync_pb.SendTabToSelfSpecifics|undefined} value
 * @return {!proto.sync_pb.EntitySpecifics} returns this
*/
proto.sync_pb.EntitySpecifics.prototype.setSendTabToSelf = function(value) {
  return jspb.Message.setOneofWrapperField(this, 601980, proto.sync_pb.EntitySpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EntitySpecifics} returns this
 */
proto.sync_pb.EntitySpecifics.prototype.clearSendTabToSelf = function() {
  return this.setSendTabToSelf(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntitySpecifics.prototype.hasSendTabToSelf = function() {
  return jspb.Message.getField(this, 601980) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.SyncEntity.repeatedFields_ = [26];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.SyncEntity.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.SyncEntity.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.SyncEntity} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncEntity.toObject = function(includeInstance, msg) {
  var f, obj = {
    idString: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    parentIdString: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    oldParentId: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    version: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    mtime: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    ctime: (f = jspb.Message.getField(msg, 6)) == null ? undefined : f,
    name: (f = jspb.Message.getField(msg, 7)) == null ? undefined : f,
    nonUniqueName: (f = jspb.Message.getField(msg, 8)) == null ? undefined : f,
    syncTimestamp: (f = jspb.Message.getField(msg, 9)) == null ? undefined : f,
    serverDefinedUniqueTag: (f = jspb.Message.getField(msg, 10)) == null ? undefined : f,
    bookmarkData: (f = msg.getBookmarkData()) && proto.sync_pb.SyncEntity.BookmarkData.toObject(includeInstance, f),
    positionInParent: (f = jspb.Message.getField(msg, 15)) == null ? undefined : f,
    insertAfterItemId: (f = jspb.Message.getField(msg, 16)) == null ? undefined : f,
    deleted: jspb.Message.getBooleanFieldWithDefault(msg, 18, false),
    originatorCacheGuid: (f = jspb.Message.getField(msg, 19)) == null ? undefined : f,
    originatorClientItemId: (f = jspb.Message.getField(msg, 20)) == null ? undefined : f,
    specifics: (f = msg.getSpecifics()) && proto.sync_pb.EntitySpecifics.toObject(includeInstance, f),
    folder: jspb.Message.getBooleanFieldWithDefault(msg, 22, false),
    clientDefinedUniqueTag: (f = jspb.Message.getField(msg, 23)) == null ? undefined : f,
    ordinalInParent: msg.getOrdinalInParent_asB64(),
    uniquePosition: (f = msg.getUniquePosition()) && unique_position_pb.UniquePosition.toObject(includeInstance, f),
    attachmentIdList: jspb.Message.toObjectList(msg.getAttachmentIdList(),
    client_debug_info_pb.DeprecatedMessage.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.SyncEntity}
 */
proto.sync_pb.SyncEntity.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.SyncEntity;
  return proto.sync_pb.SyncEntity.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.SyncEntity} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.SyncEntity}
 */
proto.sync_pb.SyncEntity.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setIdString(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setParentIdString(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setOldParentId(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVersion(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMtime(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCtime(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setNonUniqueName(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSyncTimestamp(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setServerDefinedUniqueTag(value);
      break;
    case 11:
      var value = new proto.sync_pb.SyncEntity.BookmarkData;
      reader.readGroup(11, value,proto.sync_pb.SyncEntity.BookmarkData.deserializeBinaryFromReader);
      msg.setBookmarkData(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPositionInParent(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setInsertAfterItemId(value);
      break;
    case 18:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDeleted(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setOriginatorCacheGuid(value);
      break;
    case 20:
      var value = /** @type {string} */ (reader.readString());
      msg.setOriginatorClientItemId(value);
      break;
    case 21:
      var value = new proto.sync_pb.EntitySpecifics;
      reader.readMessage(value,proto.sync_pb.EntitySpecifics.deserializeBinaryFromReader);
      msg.setSpecifics(value);
      break;
    case 22:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setFolder(value);
      break;
    case 23:
      var value = /** @type {string} */ (reader.readString());
      msg.setClientDefinedUniqueTag(value);
      break;
    case 24:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setOrdinalInParent(value);
      break;
    case 25:
      var value = new unique_position_pb.UniquePosition;
      reader.readMessage(value,unique_position_pb.UniquePosition.deserializeBinaryFromReader);
      msg.setUniquePosition(value);
      break;
    case 26:
      var value = new client_debug_info_pb.DeprecatedMessage;
      reader.readMessage(value,client_debug_info_pb.DeprecatedMessage.deserializeBinaryFromReader);
      msg.addAttachmentId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.SyncEntity.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.SyncEntity.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.SyncEntity} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncEntity.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeString(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeString(
      7,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeString(
      8,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeInt64(
      9,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 10));
  if (f != null) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getBookmarkData();
  if (f != null) {
    writer.writeGroup(
      11,
      f,
      proto.sync_pb.SyncEntity.BookmarkData.serializeBinaryToWriter
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 15));
  if (f != null) {
    writer.writeInt64(
      15,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 16));
  if (f != null) {
    writer.writeString(
      16,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 18));
  if (f != null) {
    writer.writeBool(
      18,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 19));
  if (f != null) {
    writer.writeString(
      19,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 20));
  if (f != null) {
    writer.writeString(
      20,
      f
    );
  }
  f = message.getSpecifics();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      proto.sync_pb.EntitySpecifics.serializeBinaryToWriter
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 22));
  if (f != null) {
    writer.writeBool(
      22,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 23));
  if (f != null) {
    writer.writeString(
      23,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 24));
  if (f != null) {
    writer.writeBytes(
      24,
      f
    );
  }
  f = message.getUniquePosition();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      unique_position_pb.UniquePosition.serializeBinaryToWriter
    );
  }
  f = message.getAttachmentIdList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      26,
      f,
      client_debug_info_pb.DeprecatedMessage.serializeBinaryToWriter
    );
  }
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.SyncEntity.BookmarkData.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.SyncEntity.BookmarkData} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncEntity.BookmarkData.toObject = function(includeInstance, msg) {
  var f, obj = {
    bookmarkFolder: (f = jspb.Message.getBooleanField(msg, 1)) == null ? undefined : f,
    bookmarkUrl: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    bookmarkFavicon: msg.getBookmarkFavicon_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.SyncEntity.BookmarkData}
 */
proto.sync_pb.SyncEntity.BookmarkData.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.SyncEntity.BookmarkData;
  return proto.sync_pb.SyncEntity.BookmarkData.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.SyncEntity.BookmarkData} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.SyncEntity.BookmarkData}
 */
proto.sync_pb.SyncEntity.BookmarkData.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 12:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setBookmarkFolder(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setBookmarkUrl(value);
      break;
    case 14:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setBookmarkFavicon(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.SyncEntity.BookmarkData.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.SyncEntity.BookmarkData} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncEntity.BookmarkData.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {boolean} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeBool(
      12,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      13,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBytes(
      14,
      f
    );
  }
};


/**
 * required bool bookmark_folder = 12;
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.getBookmarkFolder = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.setBookmarkFolder = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.clearBookmarkFolder = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.hasBookmarkFolder = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string bookmark_url = 13;
 * @return {string}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.getBookmarkUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.setBookmarkUrl = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.clearBookmarkUrl = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.hasBookmarkUrl = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bytes bookmark_favicon = 14;
 * @return {!(string|Uint8Array)}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.getBookmarkFavicon = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * optional bytes bookmark_favicon = 14;
 * This is a type-conversion wrapper around `getBookmarkFavicon()`
 * @return {string}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.getBookmarkFavicon_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getBookmarkFavicon()));
};


/**
 * optional bytes bookmark_favicon = 14;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getBookmarkFavicon()`
 * @return {!Uint8Array}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.getBookmarkFavicon_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getBookmarkFavicon()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.setBookmarkFavicon = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity.BookmarkData} returns this
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.clearBookmarkFavicon = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.BookmarkData.prototype.hasBookmarkFavicon = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional string id_string = 1;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getIdString = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setIdString = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearIdString = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasIdString = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string parent_id_string = 2;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getParentIdString = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setParentIdString = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearParentIdString = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasParentIdString = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string old_parent_id = 3;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getOldParentId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setOldParentId = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearOldParentId = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasOldParentId = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 version = 4;
 * @return {number}
 */
proto.sync_pb.SyncEntity.prototype.getVersion = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setVersion = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearVersion = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasVersion = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 mtime = 5;
 * @return {number}
 */
proto.sync_pb.SyncEntity.prototype.getMtime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setMtime = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearMtime = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasMtime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int64 ctime = 6;
 * @return {number}
 */
proto.sync_pb.SyncEntity.prototype.getCtime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setCtime = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearCtime = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasCtime = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional string name = 7;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setName = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearName = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasName = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional string non_unique_name = 8;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getNonUniqueName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setNonUniqueName = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearNonUniqueName = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasNonUniqueName = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional int64 sync_timestamp = 9;
 * @return {number}
 */
proto.sync_pb.SyncEntity.prototype.getSyncTimestamp = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setSyncTimestamp = function(value) {
  return jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearSyncTimestamp = function() {
  return jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasSyncTimestamp = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string server_defined_unique_tag = 10;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getServerDefinedUniqueTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setServerDefinedUniqueTag = function(value) {
  return jspb.Message.setField(this, 10, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearServerDefinedUniqueTag = function() {
  return jspb.Message.setField(this, 10, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasServerDefinedUniqueTag = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional group BookmarkData = 11;
 * @return {?proto.sync_pb.SyncEntity.BookmarkData}
 */
proto.sync_pb.SyncEntity.prototype.getBookmarkData = function() {
  return /** @type{?proto.sync_pb.SyncEntity.BookmarkData} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.SyncEntity.BookmarkData, 11));
};


/**
 * @param {?proto.sync_pb.SyncEntity.BookmarkData|undefined} value
 * @return {!proto.sync_pb.SyncEntity} returns this
*/
proto.sync_pb.SyncEntity.prototype.setBookmarkData = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearBookmarkData = function() {
  return this.setBookmarkData(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasBookmarkData = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional int64 position_in_parent = 15;
 * @return {number}
 */
proto.sync_pb.SyncEntity.prototype.getPositionInParent = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 15, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setPositionInParent = function(value) {
  return jspb.Message.setField(this, 15, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearPositionInParent = function() {
  return jspb.Message.setField(this, 15, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasPositionInParent = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional string insert_after_item_id = 16;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getInsertAfterItemId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setInsertAfterItemId = function(value) {
  return jspb.Message.setField(this, 16, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearInsertAfterItemId = function() {
  return jspb.Message.setField(this, 16, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasInsertAfterItemId = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional bool deleted = 18;
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.getDeleted = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 18, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setDeleted = function(value) {
  return jspb.Message.setField(this, 18, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearDeleted = function() {
  return jspb.Message.setField(this, 18, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasDeleted = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional string originator_cache_guid = 19;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getOriginatorCacheGuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setOriginatorCacheGuid = function(value) {
  return jspb.Message.setField(this, 19, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearOriginatorCacheGuid = function() {
  return jspb.Message.setField(this, 19, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasOriginatorCacheGuid = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional string originator_client_item_id = 20;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getOriginatorClientItemId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 20, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setOriginatorClientItemId = function(value) {
  return jspb.Message.setField(this, 20, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearOriginatorClientItemId = function() {
  return jspb.Message.setField(this, 20, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasOriginatorClientItemId = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional EntitySpecifics specifics = 21;
 * @return {?proto.sync_pb.EntitySpecifics}
 */
proto.sync_pb.SyncEntity.prototype.getSpecifics = function() {
  return /** @type{?proto.sync_pb.EntitySpecifics} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.EntitySpecifics, 21));
};


/**
 * @param {?proto.sync_pb.EntitySpecifics|undefined} value
 * @return {!proto.sync_pb.SyncEntity} returns this
*/
proto.sync_pb.SyncEntity.prototype.setSpecifics = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearSpecifics = function() {
  return this.setSpecifics(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasSpecifics = function() {
  return jspb.Message.getField(this, 21) != null;
};


/**
 * optional bool folder = 22;
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.getFolder = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 22, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setFolder = function(value) {
  return jspb.Message.setField(this, 22, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearFolder = function() {
  return jspb.Message.setField(this, 22, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasFolder = function() {
  return jspb.Message.getField(this, 22) != null;
};


/**
 * optional string client_defined_unique_tag = 23;
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getClientDefinedUniqueTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 23, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setClientDefinedUniqueTag = function(value) {
  return jspb.Message.setField(this, 23, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearClientDefinedUniqueTag = function() {
  return jspb.Message.setField(this, 23, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasClientDefinedUniqueTag = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional bytes ordinal_in_parent = 24;
 * @return {!(string|Uint8Array)}
 */
proto.sync_pb.SyncEntity.prototype.getOrdinalInParent = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 24, ""));
};


/**
 * optional bytes ordinal_in_parent = 24;
 * This is a type-conversion wrapper around `getOrdinalInParent()`
 * @return {string}
 */
proto.sync_pb.SyncEntity.prototype.getOrdinalInParent_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getOrdinalInParent()));
};


/**
 * optional bytes ordinal_in_parent = 24;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getOrdinalInParent()`
 * @return {!Uint8Array}
 */
proto.sync_pb.SyncEntity.prototype.getOrdinalInParent_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getOrdinalInParent()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.setOrdinalInParent = function(value) {
  return jspb.Message.setField(this, 24, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearOrdinalInParent = function() {
  return jspb.Message.setField(this, 24, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasOrdinalInParent = function() {
  return jspb.Message.getField(this, 24) != null;
};


/**
 * optional UniquePosition unique_position = 25;
 * @return {?proto.sync_pb.UniquePosition}
 */
proto.sync_pb.SyncEntity.prototype.getUniquePosition = function() {
  return /** @type{?proto.sync_pb.UniquePosition} */ (
    jspb.Message.getWrapperField(this, unique_position_pb.UniquePosition, 25));
};


/**
 * @param {?proto.sync_pb.UniquePosition|undefined} value
 * @return {!proto.sync_pb.SyncEntity} returns this
*/
proto.sync_pb.SyncEntity.prototype.setUniquePosition = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearUniquePosition = function() {
  return this.setUniquePosition(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncEntity.prototype.hasUniquePosition = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * repeated DeprecatedMessage attachment_id = 26;
 * @return {!Array<!proto.sync_pb.DeprecatedMessage>}
 */
proto.sync_pb.SyncEntity.prototype.getAttachmentIdList = function() {
  return /** @type{!Array<!proto.sync_pb.DeprecatedMessage>} */ (
    jspb.Message.getRepeatedWrapperField(this, client_debug_info_pb.DeprecatedMessage, 26));
};


/**
 * @param {!Array<!proto.sync_pb.DeprecatedMessage>} value
 * @return {!proto.sync_pb.SyncEntity} returns this
*/
proto.sync_pb.SyncEntity.prototype.setAttachmentIdList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 26, value);
};


/**
 * @param {!proto.sync_pb.DeprecatedMessage=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DeprecatedMessage}
 */
proto.sync_pb.SyncEntity.prototype.addAttachmentId = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 26, opt_value, proto.sync_pb.DeprecatedMessage, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.SyncEntity} returns this
 */
proto.sync_pb.SyncEntity.prototype.clearAttachmentIdList = function() {
  return this.setAttachmentIdList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ChromiumExtensionsActivity.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ChromiumExtensionsActivity} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ChromiumExtensionsActivity.toObject = function(includeInstance, msg) {
  var f, obj = {
    extensionId: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    bookmarkWritesSinceLastCommit: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ChromiumExtensionsActivity}
 */
proto.sync_pb.ChromiumExtensionsActivity.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ChromiumExtensionsActivity;
  return proto.sync_pb.ChromiumExtensionsActivity.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ChromiumExtensionsActivity} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ChromiumExtensionsActivity}
 */
proto.sync_pb.ChromiumExtensionsActivity.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setExtensionId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setBookmarkWritesSinceLastCommit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ChromiumExtensionsActivity.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ChromiumExtensionsActivity} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ChromiumExtensionsActivity.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeUint32(
      2,
      f
    );
  }
};


/**
 * optional string extension_id = 1;
 * @return {string}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.getExtensionId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ChromiumExtensionsActivity} returns this
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.setExtensionId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ChromiumExtensionsActivity} returns this
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.clearExtensionId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.hasExtensionId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional uint32 bookmark_writes_since_last_commit = 2;
 * @return {number}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.getBookmarkWritesSinceLastCommit = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ChromiumExtensionsActivity} returns this
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.setBookmarkWritesSinceLastCommit = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ChromiumExtensionsActivity} returns this
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.clearBookmarkWritesSinceLastCommit = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ChromiumExtensionsActivity.prototype.hasBookmarkWritesSinceLastCommit = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.ClientConfigParams.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientConfigParams.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientConfigParams.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientConfigParams} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientConfigParams.toObject = function(includeInstance, msg) {
  var f, obj = {
    enabledTypeIdsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    tabsDatatypeEnabled: (f = jspb.Message.getBooleanField(msg, 2)) == null ? undefined : f,
    cookieJarMismatch: (f = jspb.Message.getBooleanField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientConfigParams}
 */
proto.sync_pb.ClientConfigParams.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientConfigParams;
  return proto.sync_pb.ClientConfigParams.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientConfigParams} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientConfigParams}
 */
proto.sync_pb.ClientConfigParams.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.addEnabledTypeIds(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setTabsDatatypeEnabled(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCookieJarMismatch(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientConfigParams.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientConfigParams.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientConfigParams} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientConfigParams.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEnabledTypeIdsList();
  if (f.length > 0) {
    writer.writeRepeatedInt32(
      1,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBool(
      2,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * repeated int32 enabled_type_ids = 1;
 * @return {!Array<number>}
 */
proto.sync_pb.ClientConfigParams.prototype.getEnabledTypeIdsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.setEnabledTypeIdsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.addEnabledTypeIds = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.clearEnabledTypeIdsList = function() {
  return this.setEnabledTypeIdsList([]);
};


/**
 * optional bool tabs_datatype_enabled = 2;
 * @return {boolean}
 */
proto.sync_pb.ClientConfigParams.prototype.getTabsDatatypeEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.setTabsDatatypeEnabled = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.clearTabsDatatypeEnabled = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientConfigParams.prototype.hasTabsDatatypeEnabled = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool cookie_jar_mismatch = 3;
 * @return {boolean}
 */
proto.sync_pb.ClientConfigParams.prototype.getCookieJarMismatch = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.setCookieJarMismatch = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientConfigParams} returns this
 */
proto.sync_pb.ClientConfigParams.prototype.clearCookieJarMismatch = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientConfigParams.prototype.hasCookieJarMismatch = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.CommitMessage.repeatedFields_ = [1,3,5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.CommitMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.CommitMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.CommitMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitMessage.toObject = function(includeInstance, msg) {
  var f, obj = {
    entriesList: jspb.Message.toObjectList(msg.getEntriesList(),
    proto.sync_pb.SyncEntity.toObject, includeInstance),
    cacheGuid: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    extensionsActivityList: jspb.Message.toObjectList(msg.getExtensionsActivityList(),
    proto.sync_pb.ChromiumExtensionsActivity.toObject, includeInstance),
    configParams: (f = msg.getConfigParams()) && proto.sync_pb.ClientConfigParams.toObject(includeInstance, f),
    clientContextsList: jspb.Message.toObjectList(msg.getClientContextsList(),
    proto.sync_pb.DataTypeContext.toObject, includeInstance),
    padding: (f = jspb.Message.getField(msg, 6)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.CommitMessage}
 */
proto.sync_pb.CommitMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.CommitMessage;
  return proto.sync_pb.CommitMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.CommitMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.CommitMessage}
 */
proto.sync_pb.CommitMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.SyncEntity;
      reader.readMessage(value,proto.sync_pb.SyncEntity.deserializeBinaryFromReader);
      msg.addEntries(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCacheGuid(value);
      break;
    case 3:
      var value = new proto.sync_pb.ChromiumExtensionsActivity;
      reader.readMessage(value,proto.sync_pb.ChromiumExtensionsActivity.deserializeBinaryFromReader);
      msg.addExtensionsActivity(value);
      break;
    case 4:
      var value = new proto.sync_pb.ClientConfigParams;
      reader.readMessage(value,proto.sync_pb.ClientConfigParams.deserializeBinaryFromReader);
      msg.setConfigParams(value);
      break;
    case 5:
      var value = new proto.sync_pb.DataTypeContext;
      reader.readMessage(value,proto.sync_pb.DataTypeContext.deserializeBinaryFromReader);
      msg.addClientContexts(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setPadding(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.CommitMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.CommitMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.CommitMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.sync_pb.SyncEntity.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getExtensionsActivityList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.sync_pb.ChromiumExtensionsActivity.serializeBinaryToWriter
    );
  }
  f = message.getConfigParams();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.sync_pb.ClientConfigParams.serializeBinaryToWriter
    );
  }
  f = message.getClientContextsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.sync_pb.DataTypeContext.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * repeated SyncEntity entries = 1;
 * @return {!Array<!proto.sync_pb.SyncEntity>}
 */
proto.sync_pb.CommitMessage.prototype.getEntriesList = function() {
  return /** @type{!Array<!proto.sync_pb.SyncEntity>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.SyncEntity, 1));
};


/**
 * @param {!Array<!proto.sync_pb.SyncEntity>} value
 * @return {!proto.sync_pb.CommitMessage} returns this
*/
proto.sync_pb.CommitMessage.prototype.setEntriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.sync_pb.SyncEntity=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.SyncEntity}
 */
proto.sync_pb.CommitMessage.prototype.addEntries = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.sync_pb.SyncEntity, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearEntriesList = function() {
  return this.setEntriesList([]);
};


/**
 * optional string cache_guid = 2;
 * @return {string}
 */
proto.sync_pb.CommitMessage.prototype.getCacheGuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.setCacheGuid = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearCacheGuid = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitMessage.prototype.hasCacheGuid = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * repeated ChromiumExtensionsActivity extensions_activity = 3;
 * @return {!Array<!proto.sync_pb.ChromiumExtensionsActivity>}
 */
proto.sync_pb.CommitMessage.prototype.getExtensionsActivityList = function() {
  return /** @type{!Array<!proto.sync_pb.ChromiumExtensionsActivity>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.ChromiumExtensionsActivity, 3));
};


/**
 * @param {!Array<!proto.sync_pb.ChromiumExtensionsActivity>} value
 * @return {!proto.sync_pb.CommitMessage} returns this
*/
proto.sync_pb.CommitMessage.prototype.setExtensionsActivityList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.sync_pb.ChromiumExtensionsActivity=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.ChromiumExtensionsActivity}
 */
proto.sync_pb.CommitMessage.prototype.addExtensionsActivity = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.sync_pb.ChromiumExtensionsActivity, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearExtensionsActivityList = function() {
  return this.setExtensionsActivityList([]);
};


/**
 * optional ClientConfigParams config_params = 4;
 * @return {?proto.sync_pb.ClientConfigParams}
 */
proto.sync_pb.CommitMessage.prototype.getConfigParams = function() {
  return /** @type{?proto.sync_pb.ClientConfigParams} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ClientConfigParams, 4));
};


/**
 * @param {?proto.sync_pb.ClientConfigParams|undefined} value
 * @return {!proto.sync_pb.CommitMessage} returns this
*/
proto.sync_pb.CommitMessage.prototype.setConfigParams = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearConfigParams = function() {
  return this.setConfigParams(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitMessage.prototype.hasConfigParams = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated DataTypeContext client_contexts = 5;
 * @return {!Array<!proto.sync_pb.DataTypeContext>}
 */
proto.sync_pb.CommitMessage.prototype.getClientContextsList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeContext>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeContext, 5));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeContext>} value
 * @return {!proto.sync_pb.CommitMessage} returns this
*/
proto.sync_pb.CommitMessage.prototype.setClientContextsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.sync_pb.DataTypeContext=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeContext}
 */
proto.sync_pb.CommitMessage.prototype.addClientContexts = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.sync_pb.DataTypeContext, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearClientContextsList = function() {
  return this.setClientContextsList([]);
};


/**
 * optional string padding = 6;
 * @return {string}
 */
proto.sync_pb.CommitMessage.prototype.getPadding = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.setPadding = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitMessage} returns this
 */
proto.sync_pb.CommitMessage.prototype.clearPadding = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitMessage.prototype.hasPadding = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GetUpdateTriggers.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetUpdateTriggers.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetUpdateTriggers.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetUpdateTriggers} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdateTriggers.toObject = function(includeInstance, msg) {
  var f, obj = {
    notificationHintList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    clientDroppedHints: (f = jspb.Message.getBooleanField(msg, 2)) == null ? undefined : f,
    invalidationsOutOfSync: (f = jspb.Message.getBooleanField(msg, 3)) == null ? undefined : f,
    localModificationNudges: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    datatypeRefreshNudges: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    serverDroppedHints: (f = jspb.Message.getBooleanField(msg, 6)) == null ? undefined : f,
    initialSyncInProgress: (f = jspb.Message.getBooleanField(msg, 7)) == null ? undefined : f,
    syncForResolveConflictInProgress: (f = jspb.Message.getBooleanField(msg, 8)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetUpdateTriggers}
 */
proto.sync_pb.GetUpdateTriggers.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetUpdateTriggers;
  return proto.sync_pb.GetUpdateTriggers.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetUpdateTriggers} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetUpdateTriggers}
 */
proto.sync_pb.GetUpdateTriggers.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addNotificationHint(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setClientDroppedHints(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setInvalidationsOutOfSync(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setLocalModificationNudges(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDatatypeRefreshNudges(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setServerDroppedHints(value);
      break;
    case 7:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setInitialSyncInProgress(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSyncForResolveConflictInProgress(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetUpdateTriggers.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetUpdateTriggers.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetUpdateTriggers} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdateTriggers.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNotificationHintList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBool(
      2,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBool(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeBool(
      6,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeBool(
      7,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeBool(
      8,
      f
    );
  }
};


/**
 * repeated string notification_hint = 1;
 * @return {!Array<string>}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getNotificationHintList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setNotificationHintList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.addNotificationHint = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearNotificationHintList = function() {
  return this.setNotificationHintList([]);
};


/**
 * optional bool client_dropped_hints = 2;
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getClientDroppedHints = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setClientDroppedHints = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearClientDroppedHints = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasClientDroppedHints = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool invalidations_out_of_sync = 3;
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getInvalidationsOutOfSync = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setInvalidationsOutOfSync = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearInvalidationsOutOfSync = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasInvalidationsOutOfSync = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 local_modification_nudges = 4;
 * @return {number}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getLocalModificationNudges = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setLocalModificationNudges = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearLocalModificationNudges = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasLocalModificationNudges = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 datatype_refresh_nudges = 5;
 * @return {number}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getDatatypeRefreshNudges = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setDatatypeRefreshNudges = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearDatatypeRefreshNudges = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasDatatypeRefreshNudges = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional bool server_dropped_hints = 6;
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getServerDroppedHints = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setServerDroppedHints = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearServerDroppedHints = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasServerDroppedHints = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional bool initial_sync_in_progress = 7;
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getInitialSyncInProgress = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 7, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setInitialSyncInProgress = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearInitialSyncInProgress = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasInitialSyncInProgress = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional bool sync_for_resolve_conflict_in_progress = 8;
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.getSyncForResolveConflictInProgress = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.setSyncForResolveConflictInProgress = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdateTriggers} returns this
 */
proto.sync_pb.GetUpdateTriggers.prototype.clearSyncForResolveConflictInProgress = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdateTriggers.prototype.hasSyncForResolveConflictInProgress = function() {
  return jspb.Message.getField(this, 8) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GarbageCollectionDirective.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GarbageCollectionDirective} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GarbageCollectionDirective.toObject = function(includeInstance, msg) {
  var f, obj = {
    type: jspb.Message.getFieldWithDefault(msg, 1, 0),
    versionWatermark: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    ageWatermarkInDays: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    maxNumberOfItems: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GarbageCollectionDirective}
 */
proto.sync_pb.GarbageCollectionDirective.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GarbageCollectionDirective;
  return proto.sync_pb.GarbageCollectionDirective.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GarbageCollectionDirective} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GarbageCollectionDirective}
 */
proto.sync_pb.GarbageCollectionDirective.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.sync_pb.GarbageCollectionDirective.Type} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVersionWatermark(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setAgeWatermarkInDays(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxNumberOfItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GarbageCollectionDirective.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GarbageCollectionDirective} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GarbageCollectionDirective.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!proto.sync_pb.GarbageCollectionDirective.Type} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt32(
      4,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.sync_pb.GarbageCollectionDirective.Type = {
  UNKNOWN: 0,
  VERSION_WATERMARK: 1,
  AGE_WATERMARK: 2,
  MAX_ITEM_COUNT: 3
};

/**
 * optional Type type = 1;
 * @return {!proto.sync_pb.GarbageCollectionDirective.Type}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.getType = function() {
  return /** @type {!proto.sync_pb.GarbageCollectionDirective.Type} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.sync_pb.GarbageCollectionDirective.Type} value
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.setType = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.clearType = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.hasType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 version_watermark = 2;
 * @return {number}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.getVersionWatermark = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.setVersionWatermark = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.clearVersionWatermark = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.hasVersionWatermark = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int32 age_watermark_in_days = 3;
 * @return {number}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.getAgeWatermarkInDays = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.setAgeWatermarkInDays = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.clearAgeWatermarkInDays = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.hasAgeWatermarkInDays = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int32 max_number_of_items = 4;
 * @return {number}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.getMaxNumberOfItems = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.setMaxNumberOfItems = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GarbageCollectionDirective} returns this
 */
proto.sync_pb.GarbageCollectionDirective.prototype.clearMaxNumberOfItems = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GarbageCollectionDirective.prototype.hasMaxNumberOfItems = function() {
  return jspb.Message.getField(this, 4) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.DataTypeProgressMarker.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.DataTypeProgressMarker} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.DataTypeProgressMarker.toObject = function(includeInstance, msg) {
  var f, obj = {
    dataTypeId: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    token: msg.getToken_asB64(),
    timestampTokenForMigration: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    notificationHint: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    getUpdateTriggers: (f = msg.getGetUpdateTriggers()) && proto.sync_pb.GetUpdateTriggers.toObject(includeInstance, f),
    gcDirective: (f = msg.getGcDirective()) && proto.sync_pb.GarbageCollectionDirective.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.DataTypeProgressMarker}
 */
proto.sync_pb.DataTypeProgressMarker.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.DataTypeProgressMarker;
  return proto.sync_pb.DataTypeProgressMarker.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.DataTypeProgressMarker} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.DataTypeProgressMarker}
 */
proto.sync_pb.DataTypeProgressMarker.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDataTypeId(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setToken(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimestampTokenForMigration(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNotificationHint(value);
      break;
    case 5:
      var value = new proto.sync_pb.GetUpdateTriggers;
      reader.readMessage(value,proto.sync_pb.GetUpdateTriggers.deserializeBinaryFromReader);
      msg.setGetUpdateTriggers(value);
      break;
    case 6:
      var value = new proto.sync_pb.GarbageCollectionDirective;
      reader.readMessage(value,proto.sync_pb.GarbageCollectionDirective.deserializeBinaryFromReader);
      msg.setGcDirective(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.DataTypeProgressMarker.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.DataTypeProgressMarker} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.DataTypeProgressMarker.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBytes(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getGetUpdateTriggers();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.sync_pb.GetUpdateTriggers.serializeBinaryToWriter
    );
  }
  f = message.getGcDirective();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.sync_pb.GarbageCollectionDirective.serializeBinaryToWriter
    );
  }
};


/**
 * optional int32 data_type_id = 1;
 * @return {number}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getDataTypeId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.setDataTypeId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearDataTypeId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasDataTypeId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bytes token = 2;
 * @return {!(string|Uint8Array)}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getToken = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes token = 2;
 * This is a type-conversion wrapper around `getToken()`
 * @return {string}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getToken_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getToken()));
};


/**
 * optional bytes token = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getToken()`
 * @return {!Uint8Array}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getToken_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getToken()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.setToken = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearToken = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasToken = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int64 timestamp_token_for_migration = 3;
 * @return {number}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getTimestampTokenForMigration = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.setTimestampTokenForMigration = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearTimestampTokenForMigration = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasTimestampTokenForMigration = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional string notification_hint = 4;
 * @return {string}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getNotificationHint = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.setNotificationHint = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearNotificationHint = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasNotificationHint = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional GetUpdateTriggers get_update_triggers = 5;
 * @return {?proto.sync_pb.GetUpdateTriggers}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getGetUpdateTriggers = function() {
  return /** @type{?proto.sync_pb.GetUpdateTriggers} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GetUpdateTriggers, 5));
};


/**
 * @param {?proto.sync_pb.GetUpdateTriggers|undefined} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
*/
proto.sync_pb.DataTypeProgressMarker.prototype.setGetUpdateTriggers = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearGetUpdateTriggers = function() {
  return this.setGetUpdateTriggers(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasGetUpdateTriggers = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional GarbageCollectionDirective gc_directive = 6;
 * @return {?proto.sync_pb.GarbageCollectionDirective}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.getGcDirective = function() {
  return /** @type{?proto.sync_pb.GarbageCollectionDirective} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GarbageCollectionDirective, 6));
};


/**
 * @param {?proto.sync_pb.GarbageCollectionDirective|undefined} value
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
*/
proto.sync_pb.DataTypeProgressMarker.prototype.setGcDirective = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.DataTypeProgressMarker} returns this
 */
proto.sync_pb.DataTypeProgressMarker.prototype.clearGcDirective = function() {
  return this.setGcDirective(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeProgressMarker.prototype.hasGcDirective = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GetUpdatesMessage.repeatedFields_ = [6,11];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetUpdatesMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetUpdatesMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetUpdatesMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesMessage.toObject = function(includeInstance, msg) {
  var f, obj = {
    fromTimestamp: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    callerInfo: (f = msg.getCallerInfo()) && get_updates_caller_info_pb.GetUpdatesCallerInfo.toObject(includeInstance, f),
    fetchFolders: jspb.Message.getBooleanFieldWithDefault(msg, 3, true),
    batchSize: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    fromProgressMarkerList: jspb.Message.toObjectList(msg.getFromProgressMarkerList(),
    proto.sync_pb.DataTypeProgressMarker.toObject, includeInstance),
    streaming: jspb.Message.getBooleanFieldWithDefault(msg, 7, false),
    needEncryptionKey: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    createMobileBookmarksFolder: jspb.Message.getBooleanFieldWithDefault(msg, 1000, false),
    getUpdatesOrigin: (f = jspb.Message.getField(msg, 9)) == null ? undefined : f,
    isRetry: jspb.Message.getBooleanFieldWithDefault(msg, 10, false),
    clientContextsList: jspb.Message.toObjectList(msg.getClientContextsList(),
    proto.sync_pb.DataTypeContext.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetUpdatesMessage}
 */
proto.sync_pb.GetUpdatesMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetUpdatesMessage;
  return proto.sync_pb.GetUpdatesMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetUpdatesMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetUpdatesMessage}
 */
proto.sync_pb.GetUpdatesMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFromTimestamp(value);
      break;
    case 2:
      var value = new get_updates_caller_info_pb.GetUpdatesCallerInfo;
      reader.readMessage(value,get_updates_caller_info_pb.GetUpdatesCallerInfo.deserializeBinaryFromReader);
      msg.setCallerInfo(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setFetchFolders(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setBatchSize(value);
      break;
    case 6:
      var value = new proto.sync_pb.DataTypeProgressMarker;
      reader.readMessage(value,proto.sync_pb.DataTypeProgressMarker.deserializeBinaryFromReader);
      msg.addFromProgressMarker(value);
      break;
    case 7:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setStreaming(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setNeedEncryptionKey(value);
      break;
    case 1000:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCreateMobileBookmarksFolder(value);
      break;
    case 9:
      var value = /** @type {!proto.sync_pb.SyncEnums.GetUpdatesOrigin} */ (reader.readEnum());
      msg.setGetUpdatesOrigin(value);
      break;
    case 10:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsRetry(value);
      break;
    case 11:
      var value = new proto.sync_pb.DataTypeContext;
      reader.readMessage(value,proto.sync_pb.DataTypeContext.deserializeBinaryFromReader);
      msg.addClientContexts(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetUpdatesMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetUpdatesMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetUpdatesMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getCallerInfo();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      get_updates_caller_info_pb.GetUpdatesCallerInfo.serializeBinaryToWriter
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBool(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getFromProgressMarkerList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.sync_pb.DataTypeProgressMarker.serializeBinaryToWriter
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeBool(
      7,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeBool(
      8,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 1000));
  if (f != null) {
    writer.writeBool(
      1000,
      f
    );
  }
  f = /** @type {!proto.sync_pb.SyncEnums.GetUpdatesOrigin} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeEnum(
      9,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 10));
  if (f != null) {
    writer.writeBool(
      10,
      f
    );
  }
  f = message.getClientContextsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      proto.sync_pb.DataTypeContext.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 from_timestamp = 1;
 * @return {number}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getFromTimestamp = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setFromTimestamp = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearFromTimestamp = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasFromTimestamp = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional GetUpdatesCallerInfo caller_info = 2;
 * @return {?proto.sync_pb.GetUpdatesCallerInfo}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getCallerInfo = function() {
  return /** @type{?proto.sync_pb.GetUpdatesCallerInfo} */ (
    jspb.Message.getWrapperField(this, get_updates_caller_info_pb.GetUpdatesCallerInfo, 2));
};


/**
 * @param {?proto.sync_pb.GetUpdatesCallerInfo|undefined} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
*/
proto.sync_pb.GetUpdatesMessage.prototype.setCallerInfo = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearCallerInfo = function() {
  return this.setCallerInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasCallerInfo = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool fetch_folders = 3;
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getFetchFolders = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, true));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setFetchFolders = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearFetchFolders = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasFetchFolders = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int32 batch_size = 5;
 * @return {number}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getBatchSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setBatchSize = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearBatchSize = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasBatchSize = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * repeated DataTypeProgressMarker from_progress_marker = 6;
 * @return {!Array<!proto.sync_pb.DataTypeProgressMarker>}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getFromProgressMarkerList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeProgressMarker>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeProgressMarker, 6));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeProgressMarker>} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
*/
proto.sync_pb.GetUpdatesMessage.prototype.setFromProgressMarkerList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.sync_pb.DataTypeProgressMarker=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeProgressMarker}
 */
proto.sync_pb.GetUpdatesMessage.prototype.addFromProgressMarker = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.sync_pb.DataTypeProgressMarker, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearFromProgressMarkerList = function() {
  return this.setFromProgressMarkerList([]);
};


/**
 * optional bool streaming = 7;
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getStreaming = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 7, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setStreaming = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearStreaming = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasStreaming = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional bool need_encryption_key = 8;
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getNeedEncryptionKey = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setNeedEncryptionKey = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearNeedEncryptionKey = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasNeedEncryptionKey = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional bool create_mobile_bookmarks_folder = 1000;
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getCreateMobileBookmarksFolder = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1000, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setCreateMobileBookmarksFolder = function(value) {
  return jspb.Message.setField(this, 1000, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearCreateMobileBookmarksFolder = function() {
  return jspb.Message.setField(this, 1000, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasCreateMobileBookmarksFolder = function() {
  return jspb.Message.getField(this, 1000) != null;
};


/**
 * optional SyncEnums.GetUpdatesOrigin get_updates_origin = 9;
 * @return {!proto.sync_pb.SyncEnums.GetUpdatesOrigin}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getGetUpdatesOrigin = function() {
  return /** @type {!proto.sync_pb.SyncEnums.GetUpdatesOrigin} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {!proto.sync_pb.SyncEnums.GetUpdatesOrigin} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setGetUpdatesOrigin = function(value) {
  return jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearGetUpdatesOrigin = function() {
  return jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasGetUpdatesOrigin = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional bool is_retry = 10;
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getIsRetry = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 10, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.setIsRetry = function(value) {
  return jspb.Message.setField(this, 10, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearIsRetry = function() {
  return jspb.Message.setField(this, 10, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMessage.prototype.hasIsRetry = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * repeated DataTypeContext client_contexts = 11;
 * @return {!Array<!proto.sync_pb.DataTypeContext>}
 */
proto.sync_pb.GetUpdatesMessage.prototype.getClientContextsList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeContext>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeContext, 11));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeContext>} value
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
*/
proto.sync_pb.GetUpdatesMessage.prototype.setClientContextsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.sync_pb.DataTypeContext=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeContext}
 */
proto.sync_pb.GetUpdatesMessage.prototype.addClientContexts = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.sync_pb.DataTypeContext, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesMessage} returns this
 */
proto.sync_pb.GetUpdatesMessage.prototype.clearClientContextsList = function() {
  return this.setClientContextsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.AuthenticateMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.AuthenticateMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.AuthenticateMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.AuthenticateMessage.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.AuthenticateMessage}
 */
proto.sync_pb.AuthenticateMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.AuthenticateMessage;
  return proto.sync_pb.AuthenticateMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.AuthenticateMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.AuthenticateMessage}
 */
proto.sync_pb.AuthenticateMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.AuthenticateMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.AuthenticateMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.AuthenticateMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.AuthenticateMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * required string auth_token = 1;
 * @return {string}
 */
proto.sync_pb.AuthenticateMessage.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.AuthenticateMessage} returns this
 */
proto.sync_pb.AuthenticateMessage.prototype.setAuthToken = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.AuthenticateMessage} returns this
 */
proto.sync_pb.AuthenticateMessage.prototype.clearAuthToken = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.AuthenticateMessage.prototype.hasAuthToken = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClearServerDataMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClearServerDataMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClearServerDataMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClearServerDataMessage.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClearServerDataMessage}
 */
proto.sync_pb.ClearServerDataMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClearServerDataMessage;
  return proto.sync_pb.ClearServerDataMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClearServerDataMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClearServerDataMessage}
 */
proto.sync_pb.ClearServerDataMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClearServerDataMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClearServerDataMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClearServerDataMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClearServerDataMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClearServerDataResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClearServerDataResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClearServerDataResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClearServerDataResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClearServerDataResponse}
 */
proto.sync_pb.ClearServerDataResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClearServerDataResponse;
  return proto.sync_pb.ClearServerDataResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClearServerDataResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClearServerDataResponse}
 */
proto.sync_pb.ClearServerDataResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClearServerDataResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClearServerDataResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClearServerDataResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClearServerDataResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ChipBag.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ChipBag.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ChipBag} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ChipBag.toObject = function(includeInstance, msg) {
  var f, obj = {
    serverChips: msg.getServerChips_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ChipBag}
 */
proto.sync_pb.ChipBag.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ChipBag;
  return proto.sync_pb.ChipBag.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ChipBag} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ChipBag}
 */
proto.sync_pb.ChipBag.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setServerChips(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ChipBag.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ChipBag.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ChipBag} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ChipBag.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeBytes(
      1,
      f
    );
  }
};


/**
 * optional bytes server_chips = 1;
 * @return {!(string|Uint8Array)}
 */
proto.sync_pb.ChipBag.prototype.getServerChips = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * optional bytes server_chips = 1;
 * This is a type-conversion wrapper around `getServerChips()`
 * @return {string}
 */
proto.sync_pb.ChipBag.prototype.getServerChips_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getServerChips()));
};


/**
 * optional bytes server_chips = 1;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getServerChips()`
 * @return {!Uint8Array}
 */
proto.sync_pb.ChipBag.prototype.getServerChips_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getServerChips()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.sync_pb.ChipBag} returns this
 */
proto.sync_pb.ChipBag.prototype.setServerChips = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ChipBag} returns this
 */
proto.sync_pb.ChipBag.prototype.clearServerChips = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ChipBag.prototype.hasServerChips = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientStatus.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientStatus.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientStatus} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientStatus.toObject = function(includeInstance, msg) {
  var f, obj = {
    hierarchyConflictDetected: (f = jspb.Message.getBooleanField(msg, 1)) == null ? undefined : f,
    isSyncFeatureEnabled: (f = jspb.Message.getBooleanField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientStatus}
 */
proto.sync_pb.ClientStatus.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientStatus;
  return proto.sync_pb.ClientStatus.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientStatus} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientStatus}
 */
proto.sync_pb.ClientStatus.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHierarchyConflictDetected(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsSyncFeatureEnabled(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientStatus.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientStatus.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientStatus} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientStatus.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {boolean} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeBool(
      1,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBool(
      2,
      f
    );
  }
};


/**
 * optional bool hierarchy_conflict_detected = 1;
 * @return {boolean}
 */
proto.sync_pb.ClientStatus.prototype.getHierarchyConflictDetected = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.ClientStatus} returns this
 */
proto.sync_pb.ClientStatus.prototype.setHierarchyConflictDetected = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientStatus} returns this
 */
proto.sync_pb.ClientStatus.prototype.clearHierarchyConflictDetected = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientStatus.prototype.hasHierarchyConflictDetected = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bool is_sync_feature_enabled = 2;
 * @return {boolean}
 */
proto.sync_pb.ClientStatus.prototype.getIsSyncFeatureEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.ClientStatus} returns this
 */
proto.sync_pb.ClientStatus.prototype.setIsSyncFeatureEnabled = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientStatus} returns this
 */
proto.sync_pb.ClientStatus.prototype.clearIsSyncFeatureEnabled = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientStatus.prototype.hasIsSyncFeatureEnabled = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.DataTypeContext.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.DataTypeContext.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.DataTypeContext} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.DataTypeContext.toObject = function(includeInstance, msg) {
  var f, obj = {
    dataTypeId: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    context: msg.getContext_asB64(),
    version: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.DataTypeContext}
 */
proto.sync_pb.DataTypeContext.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.DataTypeContext;
  return proto.sync_pb.DataTypeContext.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.DataTypeContext} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.DataTypeContext}
 */
proto.sync_pb.DataTypeContext.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDataTypeId(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setContext(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVersion(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.DataTypeContext.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.DataTypeContext.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.DataTypeContext} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.DataTypeContext.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = /** @type {!(string|Uint8Array)} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeBytes(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * optional int32 data_type_id = 1;
 * @return {number}
 */
proto.sync_pb.DataTypeContext.prototype.getDataTypeId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.setDataTypeId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.clearDataTypeId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeContext.prototype.hasDataTypeId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bytes context = 2;
 * @return {!(string|Uint8Array)}
 */
proto.sync_pb.DataTypeContext.prototype.getContext = function() {
  return /** @type {!(string|Uint8Array)} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes context = 2;
 * This is a type-conversion wrapper around `getContext()`
 * @return {string}
 */
proto.sync_pb.DataTypeContext.prototype.getContext_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getContext()));
};


/**
 * optional bytes context = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getContext()`
 * @return {!Uint8Array}
 */
proto.sync_pb.DataTypeContext.prototype.getContext_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getContext()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.setContext = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.clearContext = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeContext.prototype.hasContext = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int64 version = 3;
 * @return {number}
 */
proto.sync_pb.DataTypeContext.prototype.getVersion = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.setVersion = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.DataTypeContext} returns this
 */
proto.sync_pb.DataTypeContext.prototype.clearVersion = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.DataTypeContext.prototype.hasVersion = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientToServerMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientToServerMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientToServerMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerMessage.toObject = function(includeInstance, msg) {
  var f, obj = {
    share: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    protocolVersion: jspb.Message.getFieldWithDefault(msg, 2, 52),
    messageContents: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    commit: (f = msg.getCommit()) && proto.sync_pb.CommitMessage.toObject(includeInstance, f),
    getUpdates: (f = msg.getGetUpdates()) && proto.sync_pb.GetUpdatesMessage.toObject(includeInstance, f),
    authenticate: (f = msg.getAuthenticate()) && proto.sync_pb.AuthenticateMessage.toObject(includeInstance, f),
    deprecatedField9: (f = msg.getDeprecatedField9()) && client_debug_info_pb.DeprecatedMessage.toObject(includeInstance, f),
    storeBirthday: (f = jspb.Message.getField(msg, 7)) == null ? undefined : f,
    syncProblemDetected: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    debugInfo: (f = msg.getDebugInfo()) && client_debug_info_pb.DebugInfo.toObject(includeInstance, f),
    bagOfChips: (f = msg.getBagOfChips()) && proto.sync_pb.ChipBag.toObject(includeInstance, f),
    apiKey: (f = jspb.Message.getField(msg, 12)) == null ? undefined : f,
    clientStatus: (f = msg.getClientStatus()) && proto.sync_pb.ClientStatus.toObject(includeInstance, f),
    invalidatorClientId: (f = jspb.Message.getField(msg, 14)) == null ? undefined : f,
    clearServerData: (f = msg.getClearServerData()) && proto.sync_pb.ClearServerDataMessage.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientToServerMessage}
 */
proto.sync_pb.ClientToServerMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientToServerMessage;
  return proto.sync_pb.ClientToServerMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientToServerMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientToServerMessage}
 */
proto.sync_pb.ClientToServerMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setShare(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setProtocolVersion(value);
      break;
    case 3:
      var value = /** @type {!proto.sync_pb.ClientToServerMessage.Contents} */ (reader.readEnum());
      msg.setMessageContents(value);
      break;
    case 4:
      var value = new proto.sync_pb.CommitMessage;
      reader.readMessage(value,proto.sync_pb.CommitMessage.deserializeBinaryFromReader);
      msg.setCommit(value);
      break;
    case 5:
      var value = new proto.sync_pb.GetUpdatesMessage;
      reader.readMessage(value,proto.sync_pb.GetUpdatesMessage.deserializeBinaryFromReader);
      msg.setGetUpdates(value);
      break;
    case 6:
      var value = new proto.sync_pb.AuthenticateMessage;
      reader.readMessage(value,proto.sync_pb.AuthenticateMessage.deserializeBinaryFromReader);
      msg.setAuthenticate(value);
      break;
    case 9:
      var value = new client_debug_info_pb.DeprecatedMessage;
      reader.readMessage(value,client_debug_info_pb.DeprecatedMessage.deserializeBinaryFromReader);
      msg.setDeprecatedField9(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setStoreBirthday(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSyncProblemDetected(value);
      break;
    case 10:
      var value = new client_debug_info_pb.DebugInfo;
      reader.readMessage(value,client_debug_info_pb.DebugInfo.deserializeBinaryFromReader);
      msg.setDebugInfo(value);
      break;
    case 11:
      var value = new proto.sync_pb.ChipBag;
      reader.readMessage(value,proto.sync_pb.ChipBag.deserializeBinaryFromReader);
      msg.setBagOfChips(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setApiKey(value);
      break;
    case 13:
      var value = new proto.sync_pb.ClientStatus;
      reader.readMessage(value,proto.sync_pb.ClientStatus.deserializeBinaryFromReader);
      msg.setClientStatus(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setInvalidatorClientId(value);
      break;
    case 15:
      var value = new proto.sync_pb.ClearServerDataMessage;
      reader.readMessage(value,proto.sync_pb.ClearServerDataMessage.deserializeBinaryFromReader);
      msg.setClearServerData(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientToServerMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientToServerMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientToServerMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = /** @type {!proto.sync_pb.ClientToServerMessage.Contents} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getCommit();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.sync_pb.CommitMessage.serializeBinaryToWriter
    );
  }
  f = message.getGetUpdates();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.sync_pb.GetUpdatesMessage.serializeBinaryToWriter
    );
  }
  f = message.getAuthenticate();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.sync_pb.AuthenticateMessage.serializeBinaryToWriter
    );
  }
  f = message.getDeprecatedField9();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      client_debug_info_pb.DeprecatedMessage.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeString(
      7,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getDebugInfo();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      client_debug_info_pb.DebugInfo.serializeBinaryToWriter
    );
  }
  f = message.getBagOfChips();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.sync_pb.ChipBag.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 12));
  if (f != null) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getClientStatus();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.sync_pb.ClientStatus.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 14));
  if (f != null) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getClearServerData();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.sync_pb.ClearServerDataMessage.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.sync_pb.ClientToServerMessage.Contents = {
  COMMIT: 1,
  GET_UPDATES: 2,
  AUTHENTICATE: 3,
  DEPRECATED_4: 4,
  CLEAR_SERVER_DATA: 5
};

/**
 * required string share = 1;
 * @return {string}
 */
proto.sync_pb.ClientToServerMessage.prototype.getShare = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setShare = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearShare = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasShare = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int32 protocol_version = 2;
 * @return {number}
 */
proto.sync_pb.ClientToServerMessage.prototype.getProtocolVersion = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 52));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setProtocolVersion = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearProtocolVersion = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasProtocolVersion = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * required Contents message_contents = 3;
 * @return {!proto.sync_pb.ClientToServerMessage.Contents}
 */
proto.sync_pb.ClientToServerMessage.prototype.getMessageContents = function() {
  return /** @type {!proto.sync_pb.ClientToServerMessage.Contents} */ (jspb.Message.getFieldWithDefault(this, 3, 1));
};


/**
 * @param {!proto.sync_pb.ClientToServerMessage.Contents} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setMessageContents = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearMessageContents = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasMessageContents = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional CommitMessage commit = 4;
 * @return {?proto.sync_pb.CommitMessage}
 */
proto.sync_pb.ClientToServerMessage.prototype.getCommit = function() {
  return /** @type{?proto.sync_pb.CommitMessage} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.CommitMessage, 4));
};


/**
 * @param {?proto.sync_pb.CommitMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setCommit = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearCommit = function() {
  return this.setCommit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasCommit = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional GetUpdatesMessage get_updates = 5;
 * @return {?proto.sync_pb.GetUpdatesMessage}
 */
proto.sync_pb.ClientToServerMessage.prototype.getGetUpdates = function() {
  return /** @type{?proto.sync_pb.GetUpdatesMessage} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GetUpdatesMessage, 5));
};


/**
 * @param {?proto.sync_pb.GetUpdatesMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setGetUpdates = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearGetUpdates = function() {
  return this.setGetUpdates(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasGetUpdates = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional AuthenticateMessage authenticate = 6;
 * @return {?proto.sync_pb.AuthenticateMessage}
 */
proto.sync_pb.ClientToServerMessage.prototype.getAuthenticate = function() {
  return /** @type{?proto.sync_pb.AuthenticateMessage} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.AuthenticateMessage, 6));
};


/**
 * @param {?proto.sync_pb.AuthenticateMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setAuthenticate = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearAuthenticate = function() {
  return this.setAuthenticate(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasAuthenticate = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional DeprecatedMessage deprecated_field_9 = 9;
 * @return {?proto.sync_pb.DeprecatedMessage}
 */
proto.sync_pb.ClientToServerMessage.prototype.getDeprecatedField9 = function() {
  return /** @type{?proto.sync_pb.DeprecatedMessage} */ (
    jspb.Message.getWrapperField(this, client_debug_info_pb.DeprecatedMessage, 9));
};


/**
 * @param {?proto.sync_pb.DeprecatedMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setDeprecatedField9 = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearDeprecatedField9 = function() {
  return this.setDeprecatedField9(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasDeprecatedField9 = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string store_birthday = 7;
 * @return {string}
 */
proto.sync_pb.ClientToServerMessage.prototype.getStoreBirthday = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setStoreBirthday = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearStoreBirthday = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasStoreBirthday = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional bool sync_problem_detected = 8;
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.getSyncProblemDetected = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setSyncProblemDetected = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearSyncProblemDetected = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasSyncProblemDetected = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional DebugInfo debug_info = 10;
 * @return {?proto.sync_pb.DebugInfo}
 */
proto.sync_pb.ClientToServerMessage.prototype.getDebugInfo = function() {
  return /** @type{?proto.sync_pb.DebugInfo} */ (
    jspb.Message.getWrapperField(this, client_debug_info_pb.DebugInfo, 10));
};


/**
 * @param {?proto.sync_pb.DebugInfo|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setDebugInfo = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearDebugInfo = function() {
  return this.setDebugInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasDebugInfo = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional ChipBag bag_of_chips = 11;
 * @return {?proto.sync_pb.ChipBag}
 */
proto.sync_pb.ClientToServerMessage.prototype.getBagOfChips = function() {
  return /** @type{?proto.sync_pb.ChipBag} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ChipBag, 11));
};


/**
 * @param {?proto.sync_pb.ChipBag|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setBagOfChips = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearBagOfChips = function() {
  return this.setBagOfChips(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasBagOfChips = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional string api_key = 12;
 * @return {string}
 */
proto.sync_pb.ClientToServerMessage.prototype.getApiKey = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setApiKey = function(value) {
  return jspb.Message.setField(this, 12, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearApiKey = function() {
  return jspb.Message.setField(this, 12, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasApiKey = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional ClientStatus client_status = 13;
 * @return {?proto.sync_pb.ClientStatus}
 */
proto.sync_pb.ClientToServerMessage.prototype.getClientStatus = function() {
  return /** @type{?proto.sync_pb.ClientStatus} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ClientStatus, 13));
};


/**
 * @param {?proto.sync_pb.ClientStatus|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setClientStatus = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearClientStatus = function() {
  return this.setClientStatus(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasClientStatus = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional string invalidator_client_id = 14;
 * @return {string}
 */
proto.sync_pb.ClientToServerMessage.prototype.getInvalidatorClientId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.setInvalidatorClientId = function(value) {
  return jspb.Message.setField(this, 14, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearInvalidatorClientId = function() {
  return jspb.Message.setField(this, 14, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasInvalidatorClientId = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional ClearServerDataMessage clear_server_data = 15;
 * @return {?proto.sync_pb.ClearServerDataMessage}
 */
proto.sync_pb.ClientToServerMessage.prototype.getClearServerData = function() {
  return /** @type{?proto.sync_pb.ClearServerDataMessage} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ClearServerDataMessage, 15));
};


/**
 * @param {?proto.sync_pb.ClearServerDataMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
*/
proto.sync_pb.ClientToServerMessage.prototype.setClearServerData = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerMessage} returns this
 */
proto.sync_pb.ClientToServerMessage.prototype.clearClearServerData = function() {
  return this.setClearServerData(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerMessage.prototype.hasClearServerData = function() {
  return jspb.Message.getField(this, 15) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetCrashInfoRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetCrashInfoRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetCrashInfoRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    crashId: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    crashTimeMillis: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetCrashInfoRequest}
 */
proto.sync_pb.GetCrashInfoRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetCrashInfoRequest;
  return proto.sync_pb.GetCrashInfoRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetCrashInfoRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetCrashInfoRequest}
 */
proto.sync_pb.GetCrashInfoRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCrashId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCrashTimeMillis(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetCrashInfoRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetCrashInfoRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetCrashInfoRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional string crash_id = 1;
 * @return {string}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.getCrashId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.GetCrashInfoRequest} returns this
 */
proto.sync_pb.GetCrashInfoRequest.prototype.setCrashId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetCrashInfoRequest} returns this
 */
proto.sync_pb.GetCrashInfoRequest.prototype.clearCrashId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.hasCrashId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 crash_time_millis = 2;
 * @return {number}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.getCrashTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetCrashInfoRequest} returns this
 */
proto.sync_pb.GetCrashInfoRequest.prototype.setCrashTimeMillis = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetCrashInfoRequest} returns this
 */
proto.sync_pb.GetCrashInfoRequest.prototype.clearCrashTimeMillis = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetCrashInfoRequest.prototype.hasCrashTimeMillis = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetCrashInfoResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetCrashInfoResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetCrashInfoResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    stackId: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    crashTimeMillis: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetCrashInfoResponse}
 */
proto.sync_pb.GetCrashInfoResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetCrashInfoResponse;
  return proto.sync_pb.GetCrashInfoResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetCrashInfoResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetCrashInfoResponse}
 */
proto.sync_pb.GetCrashInfoResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStackId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCrashTimeMillis(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetCrashInfoResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetCrashInfoResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetCrashInfoResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional string stack_id = 1;
 * @return {string}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.getStackId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.GetCrashInfoResponse} returns this
 */
proto.sync_pb.GetCrashInfoResponse.prototype.setStackId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetCrashInfoResponse} returns this
 */
proto.sync_pb.GetCrashInfoResponse.prototype.clearStackId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.hasStackId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 crash_time_millis = 2;
 * @return {number}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.getCrashTimeMillis = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetCrashInfoResponse} returns this
 */
proto.sync_pb.GetCrashInfoResponse.prototype.setCrashTimeMillis = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetCrashInfoResponse} returns this
 */
proto.sync_pb.GetCrashInfoResponse.prototype.clearCrashTimeMillis = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetCrashInfoResponse.prototype.hasCrashTimeMillis = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.CommitResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.CommitResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.CommitResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.CommitResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    entryResponseList: jspb.Message.toObjectList(msg.getEntryResponseList(),
    proto.sync_pb.CommitResponse.EntryResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.CommitResponse}
 */
proto.sync_pb.CommitResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.CommitResponse;
  return proto.sync_pb.CommitResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.CommitResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.CommitResponse}
 */
proto.sync_pb.CommitResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.CommitResponse.EntryResponse;
      reader.readGroup(1, value,proto.sync_pb.CommitResponse.EntryResponse.deserializeBinaryFromReader);
      msg.addEntryResponse(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.CommitResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.CommitResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.CommitResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntryResponseList();
  if (f.length > 0) {
    writer.writeRepeatedGroup(
      1,
      f,
      proto.sync_pb.CommitResponse.EntryResponse.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.sync_pb.CommitResponse.ResponseType = {
  SUCCESS: 1,
  CONFLICT: 2,
  RETRY: 3,
  INVALID_MESSAGE: 4,
  OVER_QUOTA: 5,
  TRANSIENT_ERROR: 6
};




if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.CommitResponse.EntryResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.CommitResponse.EntryResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitResponse.EntryResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    responseType: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    idString: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    parentIdString: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    positionInParent: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    version: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    name: (f = jspb.Message.getField(msg, 6)) == null ? undefined : f,
    nonUniqueName: (f = jspb.Message.getField(msg, 7)) == null ? undefined : f,
    errorMessage: (f = jspb.Message.getField(msg, 8)) == null ? undefined : f,
    mtime: (f = jspb.Message.getField(msg, 9)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse}
 */
proto.sync_pb.CommitResponse.EntryResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.CommitResponse.EntryResponse;
  return proto.sync_pb.CommitResponse.EntryResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.CommitResponse.EntryResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse}
 */
proto.sync_pb.CommitResponse.EntryResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = /** @type {!proto.sync_pb.CommitResponse.ResponseType} */ (reader.readEnum());
      msg.setResponseType(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setIdString(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setParentIdString(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPositionInParent(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVersion(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setNonUniqueName(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setErrorMessage(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMtime(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.CommitResponse.EntryResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.CommitResponse.EntryResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CommitResponse.EntryResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!proto.sync_pb.CommitResponse.ResponseType} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      3,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeString(
      7,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeString(
      8,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeString(
      9,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeInt64(
      10,
      f
    );
  }
};


/**
 * required ResponseType response_type = 2;
 * @return {!proto.sync_pb.CommitResponse.ResponseType}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getResponseType = function() {
  return /** @type {!proto.sync_pb.CommitResponse.ResponseType} */ (jspb.Message.getFieldWithDefault(this, 1, 1));
};


/**
 * @param {!proto.sync_pb.CommitResponse.ResponseType} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setResponseType = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearResponseType = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasResponseType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string id_string = 3;
 * @return {string}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getIdString = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setIdString = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearIdString = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasIdString = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string parent_id_string = 4;
 * @return {string}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getParentIdString = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setParentIdString = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearParentIdString = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasParentIdString = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 position_in_parent = 5;
 * @return {number}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getPositionInParent = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setPositionInParent = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearPositionInParent = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasPositionInParent = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 version = 6;
 * @return {number}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getVersion = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setVersion = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearVersion = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasVersion = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional string name = 7;
 * @return {string}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setName = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearName = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasName = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional string non_unique_name = 8;
 * @return {string}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getNonUniqueName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setNonUniqueName = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearNonUniqueName = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasNonUniqueName = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional string error_message = 9;
 * @return {string}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getErrorMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setErrorMessage = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearErrorMessage = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasErrorMessage = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional int64 mtime = 10;
 * @return {number}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.getMtime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.setMtime = function(value) {
  return jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.CommitResponse.EntryResponse} returns this
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.clearMtime = function() {
  return jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CommitResponse.EntryResponse.prototype.hasMtime = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * repeated group EntryResponse = 1;
 * @return {!Array<!proto.sync_pb.CommitResponse.EntryResponse>}
 */
proto.sync_pb.CommitResponse.prototype.getEntryResponseList = function() {
  return /** @type{!Array<!proto.sync_pb.CommitResponse.EntryResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.CommitResponse.EntryResponse, 1));
};


/**
 * @param {!Array<!proto.sync_pb.CommitResponse.EntryResponse>} value
 * @return {!proto.sync_pb.CommitResponse} returns this
*/
proto.sync_pb.CommitResponse.prototype.setEntryResponseList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.sync_pb.CommitResponse.EntryResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.CommitResponse.EntryResponse}
 */
proto.sync_pb.CommitResponse.prototype.addEntryResponse = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.sync_pb.CommitResponse.EntryResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.CommitResponse} returns this
 */
proto.sync_pb.CommitResponse.prototype.clearEntryResponseList = function() {
  return this.setEntryResponseList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GetUpdatesResponse.repeatedFields_ = [1,5,6,7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetUpdatesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetUpdatesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetUpdatesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    entriesList: jspb.Message.toObjectList(msg.getEntriesList(),
    proto.sync_pb.SyncEntity.toObject, includeInstance),
    newTimestamp: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    deprecatedNewestTimestamp: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    changesRemaining: (f = jspb.Message.getField(msg, 4)) == null ? undefined : f,
    newProgressMarkerList: jspb.Message.toObjectList(msg.getNewProgressMarkerList(),
    proto.sync_pb.DataTypeProgressMarker.toObject, includeInstance),
    encryptionKeysList: msg.getEncryptionKeysList_asB64(),
    contextMutationsList: jspb.Message.toObjectList(msg.getContextMutationsList(),
    proto.sync_pb.DataTypeContext.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetUpdatesResponse}
 */
proto.sync_pb.GetUpdatesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetUpdatesResponse;
  return proto.sync_pb.GetUpdatesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetUpdatesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetUpdatesResponse}
 */
proto.sync_pb.GetUpdatesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.SyncEntity;
      reader.readMessage(value,proto.sync_pb.SyncEntity.deserializeBinaryFromReader);
      msg.addEntries(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNewTimestamp(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDeprecatedNewestTimestamp(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setChangesRemaining(value);
      break;
    case 5:
      var value = new proto.sync_pb.DataTypeProgressMarker;
      reader.readMessage(value,proto.sync_pb.DataTypeProgressMarker.deserializeBinaryFromReader);
      msg.addNewProgressMarker(value);
      break;
    case 6:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.addEncryptionKeys(value);
      break;
    case 7:
      var value = new proto.sync_pb.DataTypeContext;
      reader.readMessage(value,proto.sync_pb.DataTypeContext.deserializeBinaryFromReader);
      msg.addContextMutations(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetUpdatesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetUpdatesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetUpdatesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.sync_pb.SyncEntity.serializeBinaryToWriter
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = message.getNewProgressMarkerList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.sync_pb.DataTypeProgressMarker.serializeBinaryToWriter
    );
  }
  f = message.getEncryptionKeysList_asU8();
  if (f.length > 0) {
    writer.writeRepeatedBytes(
      6,
      f
    );
  }
  f = message.getContextMutationsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      proto.sync_pb.DataTypeContext.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SyncEntity entries = 1;
 * @return {!Array<!proto.sync_pb.SyncEntity>}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getEntriesList = function() {
  return /** @type{!Array<!proto.sync_pb.SyncEntity>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.SyncEntity, 1));
};


/**
 * @param {!Array<!proto.sync_pb.SyncEntity>} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
*/
proto.sync_pb.GetUpdatesResponse.prototype.setEntriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.sync_pb.SyncEntity=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.SyncEntity}
 */
proto.sync_pb.GetUpdatesResponse.prototype.addEntries = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.sync_pb.SyncEntity, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearEntriesList = function() {
  return this.setEntriesList([]);
};


/**
 * optional int64 new_timestamp = 2;
 * @return {number}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getNewTimestamp = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.setNewTimestamp = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearNewTimestamp = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesResponse.prototype.hasNewTimestamp = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int64 deprecated_newest_timestamp = 3;
 * @return {number}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getDeprecatedNewestTimestamp = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.setDeprecatedNewestTimestamp = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearDeprecatedNewestTimestamp = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesResponse.prototype.hasDeprecatedNewestTimestamp = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 changes_remaining = 4;
 * @return {number}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getChangesRemaining = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.setChangesRemaining = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearChangesRemaining = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesResponse.prototype.hasChangesRemaining = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated DataTypeProgressMarker new_progress_marker = 5;
 * @return {!Array<!proto.sync_pb.DataTypeProgressMarker>}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getNewProgressMarkerList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeProgressMarker>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeProgressMarker, 5));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeProgressMarker>} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
*/
proto.sync_pb.GetUpdatesResponse.prototype.setNewProgressMarkerList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.sync_pb.DataTypeProgressMarker=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeProgressMarker}
 */
proto.sync_pb.GetUpdatesResponse.prototype.addNewProgressMarker = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.sync_pb.DataTypeProgressMarker, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearNewProgressMarkerList = function() {
  return this.setNewProgressMarkerList([]);
};


/**
 * repeated bytes encryption_keys = 6;
 * @return {!(Array<!Uint8Array>|Array<string>)}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getEncryptionKeysList = function() {
  return /** @type {!(Array<!Uint8Array>|Array<string>)} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * repeated bytes encryption_keys = 6;
 * This is a type-conversion wrapper around `getEncryptionKeysList()`
 * @return {!Array<string>}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getEncryptionKeysList_asB64 = function() {
  return /** @type {!Array<string>} */ (jspb.Message.bytesListAsB64(
      this.getEncryptionKeysList()));
};


/**
 * repeated bytes encryption_keys = 6;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getEncryptionKeysList()`
 * @return {!Array<!Uint8Array>}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getEncryptionKeysList_asU8 = function() {
  return /** @type {!Array<!Uint8Array>} */ (jspb.Message.bytesListAsU8(
      this.getEncryptionKeysList()));
};


/**
 * @param {!(Array<!Uint8Array>|Array<string>)} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.setEncryptionKeysList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {!(string|Uint8Array)} value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.addEncryptionKeys = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearEncryptionKeysList = function() {
  return this.setEncryptionKeysList([]);
};


/**
 * repeated DataTypeContext context_mutations = 7;
 * @return {!Array<!proto.sync_pb.DataTypeContext>}
 */
proto.sync_pb.GetUpdatesResponse.prototype.getContextMutationsList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeContext>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeContext, 7));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeContext>} value
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
*/
proto.sync_pb.GetUpdatesResponse.prototype.setContextMutationsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.sync_pb.DataTypeContext=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeContext}
 */
proto.sync_pb.GetUpdatesResponse.prototype.addContextMutations = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.sync_pb.DataTypeContext, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesResponse} returns this
 */
proto.sync_pb.GetUpdatesResponse.prototype.clearContextMutationsList = function() {
  return this.setContextMutationsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GetUpdatesMetadataResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetUpdatesMetadataResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetUpdatesMetadataResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesMetadataResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    changesRemaining: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    newProgressMarkerList: jspb.Message.toObjectList(msg.getNewProgressMarkerList(),
    proto.sync_pb.DataTypeProgressMarker.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse}
 */
proto.sync_pb.GetUpdatesMetadataResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetUpdatesMetadataResponse;
  return proto.sync_pb.GetUpdatesMetadataResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetUpdatesMetadataResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse}
 */
proto.sync_pb.GetUpdatesMetadataResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setChangesRemaining(value);
      break;
    case 2:
      var value = new proto.sync_pb.DataTypeProgressMarker;
      reader.readMessage(value,proto.sync_pb.DataTypeProgressMarker.deserializeBinaryFromReader);
      msg.addNewProgressMarker(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetUpdatesMetadataResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetUpdatesMetadataResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesMetadataResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getNewProgressMarkerList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.sync_pb.DataTypeProgressMarker.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 changes_remaining = 1;
 * @return {number}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.getChangesRemaining = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse} returns this
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.setChangesRemaining = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse} returns this
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.clearChangesRemaining = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.hasChangesRemaining = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated DataTypeProgressMarker new_progress_marker = 2;
 * @return {!Array<!proto.sync_pb.DataTypeProgressMarker>}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.getNewProgressMarkerList = function() {
  return /** @type{!Array<!proto.sync_pb.DataTypeProgressMarker>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.DataTypeProgressMarker, 2));
};


/**
 * @param {!Array<!proto.sync_pb.DataTypeProgressMarker>} value
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse} returns this
*/
proto.sync_pb.GetUpdatesMetadataResponse.prototype.setNewProgressMarkerList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.sync_pb.DataTypeProgressMarker=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.DataTypeProgressMarker}
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.addNewProgressMarker = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.sync_pb.DataTypeProgressMarker, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesMetadataResponse} returns this
 */
proto.sync_pb.GetUpdatesMetadataResponse.prototype.clearNewProgressMarkerList = function() {
  return this.setNewProgressMarkerList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GetUpdatesStreamingResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GetUpdatesStreamingResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GetUpdatesStreamingResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GetUpdatesStreamingResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesStreamingResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    entriesList: jspb.Message.toObjectList(msg.getEntriesList(),
    proto.sync_pb.SyncEntity.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GetUpdatesStreamingResponse}
 */
proto.sync_pb.GetUpdatesStreamingResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GetUpdatesStreamingResponse;
  return proto.sync_pb.GetUpdatesStreamingResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GetUpdatesStreamingResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GetUpdatesStreamingResponse}
 */
proto.sync_pb.GetUpdatesStreamingResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.SyncEntity;
      reader.readMessage(value,proto.sync_pb.SyncEntity.deserializeBinaryFromReader);
      msg.addEntries(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GetUpdatesStreamingResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GetUpdatesStreamingResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GetUpdatesStreamingResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GetUpdatesStreamingResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.sync_pb.SyncEntity.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SyncEntity entries = 1;
 * @return {!Array<!proto.sync_pb.SyncEntity>}
 */
proto.sync_pb.GetUpdatesStreamingResponse.prototype.getEntriesList = function() {
  return /** @type{!Array<!proto.sync_pb.SyncEntity>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.SyncEntity, 1));
};


/**
 * @param {!Array<!proto.sync_pb.SyncEntity>} value
 * @return {!proto.sync_pb.GetUpdatesStreamingResponse} returns this
*/
proto.sync_pb.GetUpdatesStreamingResponse.prototype.setEntriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.sync_pb.SyncEntity=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.SyncEntity}
 */
proto.sync_pb.GetUpdatesStreamingResponse.prototype.addEntries = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.sync_pb.SyncEntity, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.GetUpdatesStreamingResponse} returns this
 */
proto.sync_pb.GetUpdatesStreamingResponse.prototype.clearEntriesList = function() {
  return this.setEntriesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.UserIdentification.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.UserIdentification.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.UserIdentification} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.UserIdentification.toObject = function(includeInstance, msg) {
  var f, obj = {
    email: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    displayName: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    obfuscatedId: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.UserIdentification}
 */
proto.sync_pb.UserIdentification.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.UserIdentification;
  return proto.sync_pb.UserIdentification.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.UserIdentification} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.UserIdentification}
 */
proto.sync_pb.UserIdentification.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDisplayName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setObfuscatedId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.UserIdentification.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.UserIdentification.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.UserIdentification} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.UserIdentification.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * required string email = 1;
 * @return {string}
 */
proto.sync_pb.UserIdentification.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.setEmail = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.clearEmail = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserIdentification.prototype.hasEmail = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string display_name = 2;
 * @return {string}
 */
proto.sync_pb.UserIdentification.prototype.getDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.setDisplayName = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.clearDisplayName = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserIdentification.prototype.hasDisplayName = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string obfuscated_id = 3;
 * @return {string}
 */
proto.sync_pb.UserIdentification.prototype.getObfuscatedId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.setObfuscatedId = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.UserIdentification} returns this
 */
proto.sync_pb.UserIdentification.prototype.clearObfuscatedId = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserIdentification.prototype.hasObfuscatedId = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.AuthenticateResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.AuthenticateResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.AuthenticateResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.AuthenticateResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    user: (f = msg.getUser()) && proto.sync_pb.UserIdentification.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.AuthenticateResponse}
 */
proto.sync_pb.AuthenticateResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.AuthenticateResponse;
  return proto.sync_pb.AuthenticateResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.AuthenticateResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.AuthenticateResponse}
 */
proto.sync_pb.AuthenticateResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.UserIdentification;
      reader.readMessage(value,proto.sync_pb.UserIdentification.deserializeBinaryFromReader);
      msg.setUser(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.AuthenticateResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.AuthenticateResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.AuthenticateResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.AuthenticateResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUser();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.sync_pb.UserIdentification.serializeBinaryToWriter
    );
  }
};


/**
 * optional UserIdentification user = 1;
 * @return {?proto.sync_pb.UserIdentification}
 */
proto.sync_pb.AuthenticateResponse.prototype.getUser = function() {
  return /** @type{?proto.sync_pb.UserIdentification} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.UserIdentification, 1));
};


/**
 * @param {?proto.sync_pb.UserIdentification|undefined} value
 * @return {!proto.sync_pb.AuthenticateResponse} returns this
*/
proto.sync_pb.AuthenticateResponse.prototype.setUser = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.AuthenticateResponse} returns this
 */
proto.sync_pb.AuthenticateResponse.prototype.clearUser = function() {
  return this.setUser(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.AuthenticateResponse.prototype.hasUser = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.ClientToServerResponse.repeatedFields_ = [12];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientToServerResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientToServerResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientToServerResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    commit: (f = msg.getCommit()) && proto.sync_pb.CommitResponse.toObject(includeInstance, f),
    getUpdates: (f = msg.getGetUpdates()) && proto.sync_pb.GetUpdatesResponse.toObject(includeInstance, f),
    authenticate: (f = msg.getAuthenticate()) && proto.sync_pb.AuthenticateResponse.toObject(includeInstance, f),
    errorCode: jspb.Message.getFieldWithDefault(msg, 4, 100),
    errorMessage: (f = jspb.Message.getField(msg, 5)) == null ? undefined : f,
    storeBirthday: (f = jspb.Message.getField(msg, 6)) == null ? undefined : f,
    clientCommand: (f = msg.getClientCommand()) && client_commands_pb.ClientCommand.toObject(includeInstance, f),
    profilingData: (f = msg.getProfilingData()) && proto.sync_pb.ProfilingData.toObject(includeInstance, f),
    deprecatedField9: (f = msg.getDeprecatedField9()) && client_debug_info_pb.DeprecatedMessage.toObject(includeInstance, f),
    streamMetadata: (f = msg.getStreamMetadata()) && proto.sync_pb.GetUpdatesMetadataResponse.toObject(includeInstance, f),
    streamData: (f = msg.getStreamData()) && proto.sync_pb.GetUpdatesStreamingResponse.toObject(includeInstance, f),
    migratedDataTypeIdList: (f = jspb.Message.getRepeatedField(msg, 12)) == null ? undefined : f,
    error: (f = msg.getError()) && proto.sync_pb.ClientToServerResponse.Error.toObject(includeInstance, f),
    newBagOfChips: (f = msg.getNewBagOfChips()) && proto.sync_pb.ChipBag.toObject(includeInstance, f),
    clearServerData: (f = msg.getClearServerData()) && proto.sync_pb.ClearServerDataResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientToServerResponse}
 */
proto.sync_pb.ClientToServerResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientToServerResponse;
  return proto.sync_pb.ClientToServerResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientToServerResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientToServerResponse}
 */
proto.sync_pb.ClientToServerResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.CommitResponse;
      reader.readMessage(value,proto.sync_pb.CommitResponse.deserializeBinaryFromReader);
      msg.setCommit(value);
      break;
    case 2:
      var value = new proto.sync_pb.GetUpdatesResponse;
      reader.readMessage(value,proto.sync_pb.GetUpdatesResponse.deserializeBinaryFromReader);
      msg.setGetUpdates(value);
      break;
    case 3:
      var value = new proto.sync_pb.AuthenticateResponse;
      reader.readMessage(value,proto.sync_pb.AuthenticateResponse.deserializeBinaryFromReader);
      msg.setAuthenticate(value);
      break;
    case 4:
      var value = /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (reader.readEnum());
      msg.setErrorCode(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setErrorMessage(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setStoreBirthday(value);
      break;
    case 7:
      var value = new client_commands_pb.ClientCommand;
      reader.readMessage(value,client_commands_pb.ClientCommand.deserializeBinaryFromReader);
      msg.setClientCommand(value);
      break;
    case 8:
      var value = new proto.sync_pb.ProfilingData;
      reader.readMessage(value,proto.sync_pb.ProfilingData.deserializeBinaryFromReader);
      msg.setProfilingData(value);
      break;
    case 9:
      var value = new client_debug_info_pb.DeprecatedMessage;
      reader.readMessage(value,client_debug_info_pb.DeprecatedMessage.deserializeBinaryFromReader);
      msg.setDeprecatedField9(value);
      break;
    case 10:
      var value = new proto.sync_pb.GetUpdatesMetadataResponse;
      reader.readMessage(value,proto.sync_pb.GetUpdatesMetadataResponse.deserializeBinaryFromReader);
      msg.setStreamMetadata(value);
      break;
    case 11:
      var value = new proto.sync_pb.GetUpdatesStreamingResponse;
      reader.readMessage(value,proto.sync_pb.GetUpdatesStreamingResponse.deserializeBinaryFromReader);
      msg.setStreamData(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt32());
      msg.addMigratedDataTypeId(value);
      break;
    case 13:
      var value = new proto.sync_pb.ClientToServerResponse.Error;
      reader.readMessage(value,proto.sync_pb.ClientToServerResponse.Error.deserializeBinaryFromReader);
      msg.setError(value);
      break;
    case 14:
      var value = new proto.sync_pb.ChipBag;
      reader.readMessage(value,proto.sync_pb.ChipBag.deserializeBinaryFromReader);
      msg.setNewBagOfChips(value);
      break;
    case 15:
      var value = new proto.sync_pb.ClearServerDataResponse;
      reader.readMessage(value,proto.sync_pb.ClearServerDataResponse.deserializeBinaryFromReader);
      msg.setClearServerData(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientToServerResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientToServerResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientToServerResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCommit();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.sync_pb.CommitResponse.serializeBinaryToWriter
    );
  }
  f = message.getGetUpdates();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.sync_pb.GetUpdatesResponse.serializeBinaryToWriter
    );
  }
  f = message.getAuthenticate();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.sync_pb.AuthenticateResponse.serializeBinaryToWriter
    );
  }
  f = /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeEnum(
      4,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeString(
      5,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getClientCommand();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      client_commands_pb.ClientCommand.serializeBinaryToWriter
    );
  }
  f = message.getProfilingData();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.sync_pb.ProfilingData.serializeBinaryToWriter
    );
  }
  f = message.getDeprecatedField9();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      client_debug_info_pb.DeprecatedMessage.serializeBinaryToWriter
    );
  }
  f = message.getStreamMetadata();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.sync_pb.GetUpdatesMetadataResponse.serializeBinaryToWriter
    );
  }
  f = message.getStreamData();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.sync_pb.GetUpdatesStreamingResponse.serializeBinaryToWriter
    );
  }
  f = message.getMigratedDataTypeIdList();
  if (f.length > 0) {
    writer.writeRepeatedInt32(
      12,
      f
    );
  }
  f = message.getError();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.sync_pb.ClientToServerResponse.Error.serializeBinaryToWriter
    );
  }
  f = message.getNewBagOfChips();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.sync_pb.ChipBag.serializeBinaryToWriter
    );
  }
  f = message.getClearServerData();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.sync_pb.ClearServerDataResponse.serializeBinaryToWriter
    );
  }
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.ClientToServerResponse.Error.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientToServerResponse.Error.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientToServerResponse.Error} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerResponse.Error.toObject = function(includeInstance, msg) {
  var f, obj = {
    errorType: jspb.Message.getFieldWithDefault(msg, 1, 100),
    errorDescription: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f,
    url: (f = jspb.Message.getField(msg, 3)) == null ? undefined : f,
    action: jspb.Message.getFieldWithDefault(msg, 4, 5),
    errorDataTypeIdsList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientToServerResponse.Error}
 */
proto.sync_pb.ClientToServerResponse.Error.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientToServerResponse.Error;
  return proto.sync_pb.ClientToServerResponse.Error.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientToServerResponse.Error} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientToServerResponse.Error}
 */
proto.sync_pb.ClientToServerResponse.Error.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (reader.readEnum());
      msg.setErrorType(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setErrorDescription(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    case 4:
      var value = /** @type {!proto.sync_pb.SyncEnums.Action} */ (reader.readEnum());
      msg.setAction(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.addErrorDataTypeIds(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientToServerResponse.Error.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientToServerResponse.Error} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientToServerResponse.Error.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeString(
      3,
      f
    );
  }
  f = /** @type {!proto.sync_pb.SyncEnums.Action} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeEnum(
      4,
      f
    );
  }
  f = message.getErrorDataTypeIdsList();
  if (f.length > 0) {
    writer.writeRepeatedInt32(
      5,
      f
    );
  }
};


/**
 * optional SyncEnums.ErrorType error_type = 1;
 * @return {!proto.sync_pb.SyncEnums.ErrorType}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.getErrorType = function() {
  return /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (jspb.Message.getFieldWithDefault(this, 1, 100));
};


/**
 * @param {!proto.sync_pb.SyncEnums.ErrorType} value
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.setErrorType = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.clearErrorType = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.hasErrorType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string error_description = 2;
 * @return {string}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.getErrorDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.setErrorDescription = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.clearErrorDescription = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.hasErrorDescription = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string url = 3;
 * @return {string}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.setUrl = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.clearUrl = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.hasUrl = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional SyncEnums.Action action = 4;
 * @return {!proto.sync_pb.SyncEnums.Action}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.getAction = function() {
  return /** @type {!proto.sync_pb.SyncEnums.Action} */ (jspb.Message.getFieldWithDefault(this, 4, 5));
};


/**
 * @param {!proto.sync_pb.SyncEnums.Action} value
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.setAction = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.clearAction = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.hasAction = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated int32 error_data_type_ids = 5;
 * @return {!Array<number>}
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.getErrorDataTypeIdsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.setErrorDataTypeIdsList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.addErrorDataTypeIds = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.ClientToServerResponse.Error} returns this
 */
proto.sync_pb.ClientToServerResponse.Error.prototype.clearErrorDataTypeIdsList = function() {
  return this.setErrorDataTypeIdsList([]);
};


/**
 * optional CommitResponse commit = 1;
 * @return {?proto.sync_pb.CommitResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getCommit = function() {
  return /** @type{?proto.sync_pb.CommitResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.CommitResponse, 1));
};


/**
 * @param {?proto.sync_pb.CommitResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setCommit = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearCommit = function() {
  return this.setCommit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasCommit = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional GetUpdatesResponse get_updates = 2;
 * @return {?proto.sync_pb.GetUpdatesResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getGetUpdates = function() {
  return /** @type{?proto.sync_pb.GetUpdatesResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GetUpdatesResponse, 2));
};


/**
 * @param {?proto.sync_pb.GetUpdatesResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setGetUpdates = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearGetUpdates = function() {
  return this.setGetUpdates(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasGetUpdates = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional AuthenticateResponse authenticate = 3;
 * @return {?proto.sync_pb.AuthenticateResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getAuthenticate = function() {
  return /** @type{?proto.sync_pb.AuthenticateResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.AuthenticateResponse, 3));
};


/**
 * @param {?proto.sync_pb.AuthenticateResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setAuthenticate = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearAuthenticate = function() {
  return this.setAuthenticate(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasAuthenticate = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional SyncEnums.ErrorType error_code = 4;
 * @return {!proto.sync_pb.SyncEnums.ErrorType}
 */
proto.sync_pb.ClientToServerResponse.prototype.getErrorCode = function() {
  return /** @type {!proto.sync_pb.SyncEnums.ErrorType} */ (jspb.Message.getFieldWithDefault(this, 4, 100));
};


/**
 * @param {!proto.sync_pb.SyncEnums.ErrorType} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.setErrorCode = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearErrorCode = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasErrorCode = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional string error_message = 5;
 * @return {string}
 */
proto.sync_pb.ClientToServerResponse.prototype.getErrorMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.setErrorMessage = function(value) {
  return jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearErrorMessage = function() {
  return jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasErrorMessage = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional string store_birthday = 6;
 * @return {string}
 */
proto.sync_pb.ClientToServerResponse.prototype.getStoreBirthday = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.setStoreBirthday = function(value) {
  return jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearStoreBirthday = function() {
  return jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasStoreBirthday = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional ClientCommand client_command = 7;
 * @return {?proto.sync_pb.ClientCommand}
 */
proto.sync_pb.ClientToServerResponse.prototype.getClientCommand = function() {
  return /** @type{?proto.sync_pb.ClientCommand} */ (
    jspb.Message.getWrapperField(this, client_commands_pb.ClientCommand, 7));
};


/**
 * @param {?proto.sync_pb.ClientCommand|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setClientCommand = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearClientCommand = function() {
  return this.setClientCommand(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasClientCommand = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional ProfilingData profiling_data = 8;
 * @return {?proto.sync_pb.ProfilingData}
 */
proto.sync_pb.ClientToServerResponse.prototype.getProfilingData = function() {
  return /** @type{?proto.sync_pb.ProfilingData} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ProfilingData, 8));
};


/**
 * @param {?proto.sync_pb.ProfilingData|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setProfilingData = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearProfilingData = function() {
  return this.setProfilingData(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasProfilingData = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional DeprecatedMessage deprecated_field_9 = 9;
 * @return {?proto.sync_pb.DeprecatedMessage}
 */
proto.sync_pb.ClientToServerResponse.prototype.getDeprecatedField9 = function() {
  return /** @type{?proto.sync_pb.DeprecatedMessage} */ (
    jspb.Message.getWrapperField(this, client_debug_info_pb.DeprecatedMessage, 9));
};


/**
 * @param {?proto.sync_pb.DeprecatedMessage|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setDeprecatedField9 = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearDeprecatedField9 = function() {
  return this.setDeprecatedField9(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasDeprecatedField9 = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional GetUpdatesMetadataResponse stream_metadata = 10;
 * @return {?proto.sync_pb.GetUpdatesMetadataResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getStreamMetadata = function() {
  return /** @type{?proto.sync_pb.GetUpdatesMetadataResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GetUpdatesMetadataResponse, 10));
};


/**
 * @param {?proto.sync_pb.GetUpdatesMetadataResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setStreamMetadata = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearStreamMetadata = function() {
  return this.setStreamMetadata(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasStreamMetadata = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional GetUpdatesStreamingResponse stream_data = 11;
 * @return {?proto.sync_pb.GetUpdatesStreamingResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getStreamData = function() {
  return /** @type{?proto.sync_pb.GetUpdatesStreamingResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GetUpdatesStreamingResponse, 11));
};


/**
 * @param {?proto.sync_pb.GetUpdatesStreamingResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setStreamData = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearStreamData = function() {
  return this.setStreamData(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasStreamData = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * repeated int32 migrated_data_type_id = 12;
 * @return {!Array<number>}
 */
proto.sync_pb.ClientToServerResponse.prototype.getMigratedDataTypeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 12));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.setMigratedDataTypeIdList = function(value) {
  return jspb.Message.setField(this, 12, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.addMigratedDataTypeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 12, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearMigratedDataTypeIdList = function() {
  return this.setMigratedDataTypeIdList([]);
};


/**
 * optional Error error = 13;
 * @return {?proto.sync_pb.ClientToServerResponse.Error}
 */
proto.sync_pb.ClientToServerResponse.prototype.getError = function() {
  return /** @type{?proto.sync_pb.ClientToServerResponse.Error} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ClientToServerResponse.Error, 13));
};


/**
 * @param {?proto.sync_pb.ClientToServerResponse.Error|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setError = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearError = function() {
  return this.setError(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasError = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional ChipBag new_bag_of_chips = 14;
 * @return {?proto.sync_pb.ChipBag}
 */
proto.sync_pb.ClientToServerResponse.prototype.getNewBagOfChips = function() {
  return /** @type{?proto.sync_pb.ChipBag} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ChipBag, 14));
};


/**
 * @param {?proto.sync_pb.ChipBag|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setNewBagOfChips = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearNewBagOfChips = function() {
  return this.setNewBagOfChips(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasNewBagOfChips = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional ClearServerDataResponse clear_server_data = 15;
 * @return {?proto.sync_pb.ClearServerDataResponse}
 */
proto.sync_pb.ClientToServerResponse.prototype.getClearServerData = function() {
  return /** @type{?proto.sync_pb.ClearServerDataResponse} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.ClearServerDataResponse, 15));
};


/**
 * @param {?proto.sync_pb.ClearServerDataResponse|undefined} value
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
*/
proto.sync_pb.ClientToServerResponse.prototype.setClearServerData = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.ClientToServerResponse} returns this
 */
proto.sync_pb.ClientToServerResponse.prototype.clearClearServerData = function() {
  return this.setClearServerData(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientToServerResponse.prototype.hasClearServerData = function() {
  return jspb.Message.getField(this, 15) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.EventRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.EventRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.EventRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EventRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    syncDisabled: (f = msg.getSyncDisabled()) && proto.sync_pb.SyncDisabledEvent.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.EventRequest}
 */
proto.sync_pb.EventRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.EventRequest;
  return proto.sync_pb.EventRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.EventRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.EventRequest}
 */
proto.sync_pb.EventRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.SyncDisabledEvent;
      reader.readMessage(value,proto.sync_pb.SyncDisabledEvent.deserializeBinaryFromReader);
      msg.setSyncDisabled(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.EventRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.EventRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.EventRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EventRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSyncDisabled();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.sync_pb.SyncDisabledEvent.serializeBinaryToWriter
    );
  }
};


/**
 * optional SyncDisabledEvent sync_disabled = 1;
 * @return {?proto.sync_pb.SyncDisabledEvent}
 */
proto.sync_pb.EventRequest.prototype.getSyncDisabled = function() {
  return /** @type{?proto.sync_pb.SyncDisabledEvent} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.SyncDisabledEvent, 1));
};


/**
 * @param {?proto.sync_pb.SyncDisabledEvent|undefined} value
 * @return {!proto.sync_pb.EventRequest} returns this
*/
proto.sync_pb.EventRequest.prototype.setSyncDisabled = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.sync_pb.EventRequest} returns this
 */
proto.sync_pb.EventRequest.prototype.clearSyncDisabled = function() {
  return this.setSyncDisabled(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EventRequest.prototype.hasSyncDisabled = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.EventResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.EventResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.EventResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EventResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.EventResponse}
 */
proto.sync_pb.EventResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.EventResponse;
  return proto.sync_pb.EventResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.EventResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.EventResponse}
 */
proto.sync_pb.EventResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.EventResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.EventResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.EventResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EventResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.SyncDisabledEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.SyncDisabledEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.SyncDisabledEvent} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncDisabledEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    cacheGuid: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    storeBirthday: (f = jspb.Message.getField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.SyncDisabledEvent}
 */
proto.sync_pb.SyncDisabledEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.SyncDisabledEvent;
  return proto.sync_pb.SyncDisabledEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.SyncDisabledEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.SyncDisabledEvent}
 */
proto.sync_pb.SyncDisabledEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCacheGuid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setStoreBirthday(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.SyncDisabledEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.SyncDisabledEvent.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.SyncDisabledEvent} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.SyncDisabledEvent.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string cache_guid = 1;
 * @return {string}
 */
proto.sync_pb.SyncDisabledEvent.prototype.getCacheGuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncDisabledEvent} returns this
 */
proto.sync_pb.SyncDisabledEvent.prototype.setCacheGuid = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncDisabledEvent} returns this
 */
proto.sync_pb.SyncDisabledEvent.prototype.clearCacheGuid = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncDisabledEvent.prototype.hasCacheGuid = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string store_birthday = 2;
 * @return {string}
 */
proto.sync_pb.SyncDisabledEvent.prototype.getStoreBirthday = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.sync_pb.SyncDisabledEvent} returns this
 */
proto.sync_pb.SyncDisabledEvent.prototype.setStoreBirthday = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.sync_pb.SyncDisabledEvent} returns this
 */
proto.sync_pb.SyncDisabledEvent.prototype.clearStoreBirthday = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.SyncDisabledEvent.prototype.hasStoreBirthday = function() {
  return jspb.Message.getField(this, 2) != null;
};


goog.object.extend(exports, proto.sync_pb);
