var str = 'aaa';
console.log(str);
console.log(str.length);
console.log('abcdefghijklmn'.length);

//헐ㅋㅋ 변수 ''에서도 삽입할 수 있네
var name = '로켓단';
var letter = '편지에 \
'+name+'을 여러번 적을 수 있을까? \n'+name+' '+name+' 가능한가 봐??';
console.log(letter);

//template literal (\, \n, .. 같은 escape 없어도 됨)
var letter2 = `이건 안되나? ${name}
오, 이건 escape안써도 ${1+5} 그냥 된다 ㄷㄷ`; 
console.log(letter2);