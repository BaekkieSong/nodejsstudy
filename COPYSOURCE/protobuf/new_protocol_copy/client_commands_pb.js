/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.sync_pb.ClientCommand', null, global);
goog.exportSymbol('proto.sync_pb.CustomNudgeDelay', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.CustomNudgeDelay = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.CustomNudgeDelay, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.CustomNudgeDelay.displayName = 'proto.sync_pb.CustomNudgeDelay';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.ClientCommand = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.ClientCommand.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.ClientCommand, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.ClientCommand.displayName = 'proto.sync_pb.ClientCommand';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.CustomNudgeDelay.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.CustomNudgeDelay.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.CustomNudgeDelay} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CustomNudgeDelay.toObject = function(includeInstance, msg) {
  var obj = {
    datatypeId: jspb.Message.getField(msg, 1),
    delayMs: jspb.Message.getField(msg, 2)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.CustomNudgeDelay}
 */
proto.sync_pb.CustomNudgeDelay.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.CustomNudgeDelay;
  return proto.sync_pb.CustomNudgeDelay.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.CustomNudgeDelay} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.CustomNudgeDelay}
 */
proto.sync_pb.CustomNudgeDelay.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDatatypeId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDelayMs(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.CustomNudgeDelay.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.CustomNudgeDelay.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.CustomNudgeDelay} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.CustomNudgeDelay.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int32 datatype_id = 1;
 * @return {number}
 */
proto.sync_pb.CustomNudgeDelay.prototype.getDatatypeId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.sync_pb.CustomNudgeDelay.prototype.setDatatypeId = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.CustomNudgeDelay.prototype.clearDatatypeId = function() {
  jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CustomNudgeDelay.prototype.hasDatatypeId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int32 delay_ms = 2;
 * @return {number}
 */
proto.sync_pb.CustomNudgeDelay.prototype.getDelayMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {number} value */
proto.sync_pb.CustomNudgeDelay.prototype.setDelayMs = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.CustomNudgeDelay.prototype.clearDelayMs = function() {
  jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.CustomNudgeDelay.prototype.hasDelayMs = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.ClientCommand.repeatedFields_ = [8];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.ClientCommand.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.ClientCommand.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.ClientCommand} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientCommand.toObject = function(includeInstance, msg) {
  var obj = {
    setSyncPollInterval: jspb.Message.getField(msg, 1),
    setSyncLongPollInterval: jspb.Message.getField(msg, 2),
    maxCommitBatchSize: jspb.Message.getField(msg, 3),
    sessionsCommitDelaySeconds: jspb.Message.getField(msg, 4),
    throttleDelaySeconds: jspb.Message.getField(msg, 5),
    clientInvalidationHintBufferSize: jspb.Message.getField(msg, 6),
    guRetryDelaySeconds: jspb.Message.getField(msg, 7),
    customNudgeDelaysList: jspb.Message.toObjectList(msg.getCustomNudgeDelaysList(),
    proto.sync_pb.CustomNudgeDelay.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.ClientCommand}
 */
proto.sync_pb.ClientCommand.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.ClientCommand;
  return proto.sync_pb.ClientCommand.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.ClientCommand} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.ClientCommand}
 */
proto.sync_pb.ClientCommand.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSetSyncPollInterval(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSetSyncLongPollInterval(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMaxCommitBatchSize(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSessionsCommitDelaySeconds(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setThrottleDelaySeconds(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setClientInvalidationHintBufferSize(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setGuRetryDelaySeconds(value);
      break;
    case 8:
      var value = new proto.sync_pb.CustomNudgeDelay;
      reader.readMessage(value,proto.sync_pb.CustomNudgeDelay.deserializeBinaryFromReader);
      msg.addCustomNudgeDelays(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.ClientCommand.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.ClientCommand.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.ClientCommand} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.ClientCommand.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getCustomNudgeDelaysList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      proto.sync_pb.CustomNudgeDelay.serializeBinaryToWriter
    );
  }
};


/**
 * optional int32 set_sync_poll_interval = 1;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getSetSyncPollInterval = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setSetSyncPollInterval = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearSetSyncPollInterval = function() {
  jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasSetSyncPollInterval = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int32 set_sync_long_poll_interval = 2;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getSetSyncLongPollInterval = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setSetSyncLongPollInterval = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearSetSyncLongPollInterval = function() {
  jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasSetSyncLongPollInterval = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int32 max_commit_batch_size = 3;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getMaxCommitBatchSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setMaxCommitBatchSize = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearMaxCommitBatchSize = function() {
  jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasMaxCommitBatchSize = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int32 sessions_commit_delay_seconds = 4;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getSessionsCommitDelaySeconds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setSessionsCommitDelaySeconds = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearSessionsCommitDelaySeconds = function() {
  jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasSessionsCommitDelaySeconds = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int32 throttle_delay_seconds = 5;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getThrottleDelaySeconds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setThrottleDelaySeconds = function(value) {
  jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearThrottleDelaySeconds = function() {
  jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasThrottleDelaySeconds = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int32 client_invalidation_hint_buffer_size = 6;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getClientInvalidationHintBufferSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setClientInvalidationHintBufferSize = function(value) {
  jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearClientInvalidationHintBufferSize = function() {
  jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasClientInvalidationHintBufferSize = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional int32 gu_retry_delay_seconds = 7;
 * @return {number}
 */
proto.sync_pb.ClientCommand.prototype.getGuRetryDelaySeconds = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/** @param {number} value */
proto.sync_pb.ClientCommand.prototype.setGuRetryDelaySeconds = function(value) {
  jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.ClientCommand.prototype.clearGuRetryDelaySeconds = function() {
  jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.ClientCommand.prototype.hasGuRetryDelaySeconds = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * repeated CustomNudgeDelay custom_nudge_delays = 8;
 * @return {!Array<!proto.sync_pb.CustomNudgeDelay>}
 */
proto.sync_pb.ClientCommand.prototype.getCustomNudgeDelaysList = function() {
  return /** @type{!Array<!proto.sync_pb.CustomNudgeDelay>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.sync_pb.CustomNudgeDelay, 8));
};


/** @param {!Array<!proto.sync_pb.CustomNudgeDelay>} value */
proto.sync_pb.ClientCommand.prototype.setCustomNudgeDelaysList = function(value) {
  jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.sync_pb.CustomNudgeDelay=} opt_value
 * @param {number=} opt_index
 * @return {!proto.sync_pb.CustomNudgeDelay}
 */
proto.sync_pb.ClientCommand.prototype.addCustomNudgeDelays = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.sync_pb.CustomNudgeDelay, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.sync_pb.ClientCommand.prototype.clearCustomNudgeDelaysList = function() {
  this.setCustomNudgeDelaysList([]);
};


goog.object.extend(exports, proto.sync_pb);
