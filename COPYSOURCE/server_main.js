/* 라이브러리 */
const http = require('http'); // for HTTP Req/Res
let url = require('url');
const asynced = require('async');
const express = require('express');
var bodyParser = require('body-parser')
var rawParser = bodyParser.json();
const app = express();
//app.use(express.compress());
const port = process.env.PORT || 1337;
let zlib = require('zlib');
var atob = require('atob')

var messages = require('./Protobuf/new_protocol_copy/sync_pb');

let mydb = require('./db/api');
let sync_pb = require('./protobuf/proto_process');
let pb = new sync_pb();
let protojs = pb.getSyncProto();
let sync = require('./chromiumsync');

let fs = require('fs'); // for file system
const { serialize } = require('v8');
const { assert } = require('console');  // ==? assert = require('assert')
const { kMaxLength } = require('buffer');
const { SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION } = require('constants');
const { object } = require('google-protobuf');
// const util = require('util');
// const { hasUncaughtExceptionCaptureCallback } = require('process');

let clientCommandMsg = protojs.root.lookupType('ClientCommand');
let clientCommand = clientCommandMsg.create();

/* 서버 초기화 */
function initSetting(request, response) {
  console.log('initialize settings');
};

function isFaviconURL(request, response) {
  if (request.url == '/favicon.ico') {
    return true;
  }
  return false;
};

function parseRequestURL(request) { // parse ?[key=value]&[key2=value2]
  let parsedData = url.parse(request.url, true);
  //  console.log(parsedData.auth); //null
  //  console.log(parsedData.host); //null
  //  console.log("path: " +  parsedData.pathname);
  console.log(parsedData.query);
  return parsedData;
};

function lastSetting() {
  console.log('last settings');
}

// /* 서버 ON */
let server = http.createServer(app);
server.listen(port);
// var server = http.createServer((request, response) => {
//   } else {
//     response.end('UnExpected error occured.');

// app.all('/', (req, res) => {  
//   console.log('aaa');
//   let body='';
//   if (req.method === 'POST') {
//     req.on('data', chunk => {
//       body += chunk;//.toString(); // convert Buffer to string
//     });
//     req.on('end', () => { // 얘 자체가 비동기 이벤트기 때문에 내부에서 처리되어야 함
//       console.log("request body:", body);
//     });
//   }
//   res.writeHead(200);
//   res.end('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
// });
app.get('/', (request, response) => {
  let parsedData = parseRequestURL(request);
  response.writeHead(200);
  response.end('aaaa');
})
var BASE64_MARKER = ';base64,';

app.post('/', (request, response) => {
  console.log('start:', request.body);
  if (isFaviconURL(request, response)) {
    response.writeHead(404);
    response.end();
    return;
  }
  initSetting(request, response);
  let body = '';
  if (request.method === 'POST') {
    request.on('data', chunk => {
      body += chunk;//.toString(); // convert Buffer to string
    });
    request.on('end', () => { // 얘 자체가 비동기 이벤트기 때문에 내부에서 처리되어야 함
      console.log("request body:", body);
      console.log('request length:', body.length);
      //let bodydata = uncompressRequestBody(body, request); //요청에서 GZIP 사용할 때 호출할 것

      // if (typeof (body) == 'string') {
      //   console.log('\x1b[33m%s\x1b[0m', '\n\nMain Server Test\n');
      //   body = Buffer.from(body);
      // }
      let jsonbody = JSON.parse(body);
      let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
      let bodydata = requestMsg.create(jsonbody);
      //console.log('create body:', bodydata)
      let pbMessage = new messages.ClientToServerMessage();
      pbMessage.setProtocolVersion(33);
      console.log('object body:', pbMessage.toObject())
      console.log('json:', bodydata.toJSON())
      for (let key in bodydata.toJSON()) {
        switch(key) {
          case 'share': pbMessage.setShare(bodydata[key]);
          break;
          case 'protocolVersion': pbMessage.setProtocolVersion(bodydata[key]);
          break;
          case 'debugInfo': 
            let debugInfo = new messages.DebugInfo();
            if (bodydata[key].hasOwnProperty('events')) {
              for (let i of bodydata[key]['events']) {
                let debugEventInfo = new messages.DebugEventInfo();
                debugEventInfo.setSingletonEvent(bodydata[key].hasOwnProperty('events') && bodydata[key]['events'].hasOwnProperty('singletonEvent') ? bodydata[key]['events']['singletonEvent'] : undefined)
                debugInfo.addEvents(debugEventInfo);
              }
            }
            debugInfo.setCryptographerReady(bodydata[key].hasOwnProperty('cryptographerReady') ? bodydata[key]['cryptographerReady'] : undefined);
            pbMessage.setDebugInfo(debugInfo);
          default:
            break;
        }
      }
      console.log('storeBirthday:', bodydata['storeBirthday']);
      pbMessage.setStoreBirthday(bodydata.hasOwnProperty('storeBirthday') ? bodydata['storeBirthday'] : undefined );
      console.log(pbMessage.toObject())
      console.log(JSON.stringify(requestMsg.create(pbMessage.toObject()).toJSON()));
      if (pbMessage.hasShare()) {
        console.log('share:', pbMessage.getShare());
      } else {
        console.log('no share:', pbMessage.getShare());
      }
      if (pbMessage.hasProtocolVersion()) {
        console.log('version:', pbMessage.getProtocolVersion());
      }
      if (pbMessage.hasMessageContents()) {
        console.log('messageContents:', pbMessage.getMessageContents());
      }
      if (pbMessage.hasGetUpdates()) {
        console.log('getUpdates:', pbMessage.getGetUpdates());
        if (pbMessage.getGetUpdates().hasFetchFolders()) {
          console.log('fetchFolders:', pbMessage.getFetchFolders());
        }
      }
      let pbResponse = new messages.ClientToServerResponse();
      pbResponse.setErrorCode(proto.sync_pb.SyncEnums.ErrorType.SUCCESS);
      pbResponse.setStoreBirthday(sync.internalServer.accountModel.getStoreBirthday());
      let responseMsg = protojs.root.lookupType('sync_pb.ClientToServerResponse');
      let resbody = responseMsg.create(pbResponse.toObject());
      
      response.end(JSON.stringify(resbody))
      return 200;
      // console.log('bodylength:', body.length)
      // console.log(Buffer.from(body))
      // body = new Buffer(body, 'utf-8');

      // zlib.gunzip(body, (err, data) => {
      //   console.log(err);
      //   if (err) console.error(err.message);
      //   console.log('unzipped')
      //   console.log(data);
      //   console.log(typeof (data));
      //   if (typeof (data) == 'Buffer' || 'object') {
      //     console.log('\x1b[33m%s\x1b[0m', '\n\nMain Internal Server Test11111\n');
      //     console.log("body:", data);
      //     let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(data);
      //     console.log("bodyeeeddd:", bodydata);
      //   }
      //   let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
      //    let bodydata = requestMsg.decode(data);
      //    console.log("bodydata:", bodydata);
      //    response.status(200).set('Content-Type', 'text/plain').send('body datadsafsdfsdf');
      // });

      if (typeof (body) == 'Buffer' || 'object' || 'string') {
        console.log('\x1b[33m%s\x1b[0m', '\n\nMain Server Test\n');
        console.log('size:', body.length)
        //let b = convertDataURIToBinary(body);
        body = Buffer.from(body, 'binary');
        console.log('size:', body.length)
        
        let b = new Uint8Array(body.length);
        for (i=0;i<body.length;i++) {
          b[i] = body[i];
        }
        console.log('arraybody:', b);
        console.log('arraybodylength:', b.length)
        //b = new Uint8Array(b);
        //body = [10,5,53,53,53,53,53,24,2,42,13,50,11,8,1,18,7,182,137,30,158,246,165,185];
        //let b = body;//Array.from(Buffer.from(body));
        console.log("body:", b);
        //let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(b);
         let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
         let bodydata = requestMsg.decode(b);
        //  let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
        //  let bodydata = requestMsg.decode(body);
        console.log("bodydata:", bodydata);
        console.log("bodydata storebirthday:", bodydata.getStoreBirthday());
        console.log("bodydata progressMarker:", bodydata.getGetUpdates().getFromProgressMarkerList());
        body = bodydata;
        //response.status(200).set('Content-Type', 'text/plain').send('body datadsafsdfsdf');
        
      }

      // let sync_msg = getSyncedProtobufMessage();
      // processDB(function (db) {
      //   //console.log(db);
      //   console.log('processDB end');
      //   closeDB();
      // });

      // let parsedData = parseRequestURL(request);
      // console.log('parsedData:', parsedData.query);
      // const result = handle(body, response, parsedData);

      //response.status(200).set('Content-Type', 'text/plain').send('body datadsafsdfsdf');
      //response.setHeader('Content-Type', 'text/plain');//'application/octet-stream');
      //response.setHeader('Content-Length', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'.length);
      //response.writeHead(200);
      //response.send('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
      //response.end('body');
      lastSetting();
    });
  }
});
function uncompressRequestBody(body, request) {
  if (request.headers['content-encoding'] == 'gzip') {
    console.log('start gunzip...')
    console.log('type:', typeof (body))
    let rawdata;
    // zlib.inflate(body, (err, data) => {
    //   console.log(err);
    //   console.log('inflate');
    //   console.log(data);
    //   let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(data);
    //   console.log("inflatedata:", bodydata);
    // })
    // zlib.inflateRaw(body, (err, data) => {
    //   console.log(err);
    //   console.log('inflateRaw');
    //   console.log(data);
    //   let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(data);
    //   console.log("inflatedata:", bodydata);
    // })
    if (typeof (body) == 'string') {
      console.log('\x1b[33m%s\x1b[0m', '\n\nMain Internal Test111111\n');
      rawdata = Buffer.from(body);
    } else {
      rawdata = body;
    }
    let data = zlib.gunzipSync(rawdata, [zlib.Z_DEFAULT_COMPRESSION]);
    console.log('body:', data);
    let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(data);
    // let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
    // let bodydata = requestMsg.decode(data);
    console.log("bodyeeeddd:", bodydata);
    return bodydata;

    zlib.gunzip(rawdata, (err, data) => {
      console.log(err);
      console.log('unzipped')
      console.log(data);
      console.log(typeof (data));
      if (typeof (data) == 'Buffer' || 'object') {
        console.log('\x1b[33m%s\x1b[0m', '\n\nMain Internal Server Test11111\n');
        console.log("body:", data);
        let bodydata = proto.sync_pb.ClientToServerMessage.deserializeBinary(data);
        // let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
        // let bodydata = requestMsg.decode(data);
        console.log("bodyeeeddd:", bodydata);
        body = bodydata;//requestMsg.encode(bodydata).finish();
        //response.end(requestMsg.encode(body).finish());
      }
    });
  };
};

function testcodeRawdataToMessage() {
  //let msg = `\n111111*2: 0.807778037799704806687373093155ruser@gmail.comgood`;
  // let msg2 = `\n\\033browsertester1337@gmail.com\\020\\064\\030\\002*\\270\\003\\022\\004\b\\000\\020\\000\\030\\001\\062\\241\\001\b\\210\\201\\002\\022\\210\\001\\022v\b\\002\\022\\070\n\\006\n\\002bm\\020\\001\\020\\063\\032\\022\t\\200\\224;\\2
  // 16a\\253\\005\\000!\\200\\224;\\216a\\253\\005\\000\\062\\030\n\\026\b\\002\\021\bX\\250\\236a\\253\\005\\000\\030\\000!\\000\\000\\000\\000\\000\\000\\000\\000\\022\\070\n\\006\n\\002bd\\020\\000\\020\\063\\032\\022\t\\200\\224;\\216a\\253\\005\\000!\\200\\
  // 224;\\216a\\253\\005\\000\\062\\030\n\\026\b\\002\\021\bX\\250\\236a\\253\\005\\000\\030\\000!\\000\\000\\000\\000\\000\\000\\000\\000 \\000H\\203\\212\\350\\355\\270.p\\273\\321\\322\\355\\270.\"\\000*\\016\\020\\000\\030\\001 \\006(\\000\\060\\000\\070\\00
  // 0@\\000\\062\\237\\001\b\\232\\267\t\\022\\210\\001\\022v\b\\002\\022\\070\n\\006\n\\002di\\020\\000\\020\\063\\032\\022\t\\200\\224;\\216a\\253\\005\\000!\\200\\224;\\216a\\253\\005\\000\\062\\030\n\\026\b\\002\\021\bX\\250\\236a\\253\\005\\000\\030\\000!
  // \\000\\000\\000\\000\\000\\000\\000\\000\\022\\070\n\\006\n\\002bd\\020\\000\\020\\063\\032\\022\t\\200\\224;\\216a\\253\\005\\000!\\200\\224;\\216a\\253\\005\\000\\062\\030\n\\026\b\\002\\021\bX\\250\\236a\\253\\005\\000\\030\\000!\\000\\000\\000\\000\\000\\
  // 000\\000\\000 \\000H\\203\\212\\350\\355\\270.p\\273\\321\\322\\355\\270.*\\016\\020\\000\\030\\001 \\000(\\000\\060\\000\\070\\000@\\000\\062\\061\b\\201\\365\\002\\022\\033 \\000H\\203\\212\\350\\355\\270.p\\273\\321\\322\\355\\270.\\210\\001\\277\\276\\245
  // \\200\\234\\222\\274\\264\\001*\\016\\020\\000\\030\\001 \\000(\\000\\060\\000\\070\\000@\\000\\062,\b\\330\\355\t\\022\\026 \\000H\\203\\212\\350\\355\\270.P\\272ޥ\\230\\001p\\273\\321\\322\\355\\270.*\\016\\020\\000\\030\\001 \\000(\\000\\060\\000\\070\\000
  // @\\000@\\000H\fP\\000\\300>\\000:%z00000173-7b3b-b5cf-0000-00005f196759R^\n\\024\\022\\022 \\000(\\000\\070\\000@\\000H\\000R\\004\b\\000\\020\\000\`\f\n\\004\\030\\210\\201\\002\n\\004\\030\\210\\201\\002\n\\004\\030\\210\\201\\002\n\\004\\030\\21
  // 0\\201\\002\n\\004\\030\\210\\201\\002\n\\002\b\\001\n\\024\\022\\022 \\000(\\000\\070\\000@\\000H\\000R\\004\b\\000\\020\\000\`\f\n\\002\b\\001\n\\004\\030\\210\\201\\002\\020\\001\\030\\000 \\000Z2\n0\\022.Chrome LINUX 72.0.3626.109 (0)-devel,gz
  // ip(gfe)b'AIzaSyAA6TqfS29Rz-WoayYDX8uPTT7om4nNX-4j\\004\b\\000\\020\\001r\\030iJTdNDdkViblYr9CROBAmg==`

  //let msg_buf = Buffer.from(msg);
  //console.log("Raw:", requestMsg.decode(msg_buf));
};

function handle(request, response, parsedData) {
  console.log(request);

  const query = parsedData.query;
  /* wallet은 internal대신 메인 서버에서 처리 
  internal은 progress tokens 구성에 대한 강한 기대를 가짐
  이를 방해하지 않기 위해 WalletProgressMarker를 제거하여 internal로 전달. 
  response날리기전에 처리해서 다시 추가 */
  let requestMsg = protojs.root.lookupType('sync_pb.ClientToServerMessage');
  const Contents = requestMsg.getEnum('Contents');
  // TODO: requestMsg.MergeFromString(request); //request에서 protobuf를 읽어 requestMsg에 Set
  // 일단 encoding된 버퍼 데이터 형식으로 온다고 가정함

  let requestJSON = {
    invalidatorClientId: 'user@gmail.com',
    aa: '2222',
    share: "111111",
    messageContents: 1,
    storeBirthday: sync.internalServer.accountModel.getStoreBirthday(),
    getUpdates: {
      fromProgressMarker: [{ dataTypeId: 1 }]
    }
  };

  //let req = requestMsg.decode(request);
  let req = requestMsg.create(request);
  console.log(req);
  const contents = req.messageContents;

  let walletMarker;
  if (req && req.getUpdates && req.getUpdates.fromProgressMarker) {
    for (let index in req.getUpdates.fromProgressMarker) {
      if (req.getUpdates.fromProgressMarker[index].dataTypeId == sync.SyncTypeName.AUTOFILL_WALLET_DATA.id) {
        walletMarker = req.getUpdates.fromProgressMarker[index];
        req.getUpdates.fromProgressMarker.pop(index);
        break;
      }
    }
    console.log('remove wallet marker:', walletMarker);
  }
  //let progressMarkerMsg = requestMsg.lookupType('')

  const httpStatusCode = sync.internalServer.handleCommand(req, query);
  console.log('HttpStatusCode:', httpStatusCode);

  if (walletMarker) {
    req.getUpdates.fromProgressMarker.push(walletMarker);
    console.log('add wallet marker');
    if (httpStatusCode == 200) {
      handleWalletRequest(req, walletMarker, response);
    }
  }
  if (httpStatusCode == 200) {
    injectClientCommand(response);
  }

  console.log(requestMsg.encode(requestJSON).finish());
  //response.writeHead(200, {"Content-Type":"text/plain; charset=utf-8"});  // 얘네는 실제론 process 내에서 처리되야 함
  // response.writeHead(200, {"Content-Type":"application/protobuf; charset=utf-8"});  // 얘네는 실제론 process 내에서 처리되야 함
  return 200;
};

function handleWalletRequest(req, walletMarker, response) {  // CToSMessage, DataTypeProgressMarker, raw_response
  if (req.messageContents != Contents.GET_UPDATES) {
    return;
  }
  let responseMsg = protojs.root.lookupType('sync_pb.ClientToServerResponse');
  res = responseMsg.create(response);
  populateWalletResults(walletEntities = [], walletMarker, res.getUpdates);
  //response = res.toJSON();  // 이 코드 없어도 response에 값 적용됨.
};

function populateWalletResults(walletEntities, walletMarker, getUpdates) {  //vector<sync_pb.SyncEntity>, DataTypeProgressMarker, sync_pb.GetUpdatesResponse
  verifyNoWalletDataProgressMarkerExists(getUpdates);
  let marker = protojs.root.lookupType('DataTypeProgressMarker').create();
  marker.dataTypeId = sync.SyncTypeName.AUTOFILL_WALLET_DATA.id;
  getUpdates.newProgressMarker.push(marker);
  console.log('getUpdates:', getUpdates);
  // TODO: 실제 Entities에 대한 Wallet처리 로직 구현
};

function verifyNoWalletDataProgressMarkerExists(getUpdates) {
  for (const marker of getUpdates.newProgressMarker) {
    assert(marker.dataTypeId != sync.SyncTypeName.AUTOFILL_WALLET_DATA.id);
  }
};

function injectClientCommand(response) {
  let responseMsg = protojs.root.lookupType('sync_pb.ClientToServerResponse');
  const SyncEnums = responseMsg.lookup('SyncEnums');
  const ErrorType = SyncEnums.getEnum('ErrorType');
  res = responseMsg.create(response);
  if (res.errorCode == ErrorType.SUCCESS) {  // 따로 처리 안해도 response 파라미터에 clientCommand값 들어감. 확인할 것...
    res.clientCommand = clientCommand;
  }
}

/* protobuf */
function getSyncedProtobufMessage() {
  let pb = new sync_pb();
  let protojs = pb.getSyncProto();  //sync.proto파일 Load
  if (true) {
    sync_msg = protojs.root.lookupType('sync_pb.GetUpdatesMessage');  //sync.proto에 정의된 메시지 중에 요청에 맞는 메시지 Read
  } else {
    console.error('not reached');
  }
  //console.log('sync_msg: ', sync_msg);
  return sync_msg;
};

/* DB */
function old_processDB() {
  console.log('db1: start');
  asynced.waterfall([
    function (callback) {
      console.log('db2: processing');
      mydb.db_init();
      callback(null, 'db3: end');
    }
  ], function (callback, msg) {
    console.log(msg);
  });
  //mydb.db_init(); // 특정 계정 요청이 있을 때 db를 오픈
  console.log('db4: initialized complete');
  setTimeout(closeDB, 3000, [1, 2, 3]);
}

function closeDB(args) {
  mydb.db_release();
  console.log('db5: released');
  //console.log(args);
}

processDB = function (callback) {
  mydb.db_init(callback);
};