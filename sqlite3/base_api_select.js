const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('./sqlite3/chinook.db', (err) => {
  if (err) console.error(err.message);
  console.log('Connected to the chinook database.');
});

// 1. Query All
// playlists 테이블에서 'Name'열을 name으로 얻어옴
// name을 기준으로 정렬
let sql_ordered_rows = `SELECT DISTINCT Name name FROM playlists
          ORDER BY name`;
// 테이블 내의 모든 행을 query (rows는 Array)
db.all(sql_ordered_rows, [], (err, rows) => {
  if (err) throw err;
  rows.forEach((row) => {
    console.log(row.name);
  });
});

// 2. Query Specific Row 
let sql_get_a_row = `SELECT PlaylistId id,
                            Name name
                    FROM playlists
                    WHERE PlaylistId = ?`;
let PlaylistId = 1;
db.get(sql_get_a_row, [PlaylistId], (err, row) => {
  if (err) console.error(err);
  return row
    ? console.log("Get PlaylistId:", row.id, row.name)
    : console.log(`PlaylistId ${PlaylistId} is not found.`);
});

// 3. Query Each row (== All)
let sql_each_row = `SELECT PlaylistId id,
                           Name name
                    FROM playlists`;
db.each(sql_each_row, (err, row) => {
  if (err) console.error(err);
  console.log(`Id: ${row.id}, Name: ${row.name}`);
});
// 3-2. Query Each row with Specific row
let sql_each_row_with_specific = `SELECT FirstName firstName,
                                         LastName lastName,
                                         Email email
                                  FROM customers
                                  WHERE Country = ?
                                  ORDER BY FirstName`;
let Country = 'USA';
db.each(sql_each_row_with_specific, [Country], (err, row) => {
  if (err) throw err;
  console.log(`${row.firstName} ${row.lastName} - ${row.email}`);
});

db.close((err) => {
  if (err) return console.error(err.message);
  console.log('closed');
})