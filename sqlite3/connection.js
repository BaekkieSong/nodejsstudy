const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database(':memory:', (err) => { // DB 연결
  if (err) return console.log(err.message);
  console.log('Connected to the In-memory SQLite Database');
});
db.close((err) => {
  if (err) return console.log(err.message);
  console.log('Close DB connection successfully');
}); // DB 연결 종료. 보류중인 모든 query가 완료될 때까지 기다림

