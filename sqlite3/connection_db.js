const sqlite3 = require('sqlite3').verbose();
// 3가지 시작 모드
// 1. sqlite3.OPEN_READONLY
// 2. sqlite3.OPEN_READWRITE
// 3. sqlite3.OPEN_CREATE
// let db_create = new sqlite3.Database('./localdb.db', sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE, (err) => {
//   if (err) return console.error(err.message);
// });
// db_create.close((err) => {
//   if (err) return console.error(err.message);
// })

// sample chinook.db
// 13개의 테이블 포함
// playlists는 'PlaylistId', 'Name' 열을 갖고 있음
let db = new sqlite3.Database('./sqlite3/chinook.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the chinook database.');
});

// playlists 테이블로부터 원하는 데이터 query
db.serialize(() => {
  db.each(`SELECT PlaylistId as id,
                  Name as name
           FROM playlists`, (err, row) => {
    if (err) console.error(err.message);
    console.log(row.id + "\t" + row.name);
  });
});

db.close((err) => {
  if (err) return console.error(err.message);
  console.log('closed');
})