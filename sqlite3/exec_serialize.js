// 명령문의 실행 흐름 제어 방법은 2가지
// 1. serialize()
// 2. parallelize()
// serialize는 실행모드를 직렬화 모드로 설정
// 한 번에 하나의 명령문만 실행, 나머지 명령문은 큐에서 대기
// 일종의 비동기 작업이므로, 다음에 일어날 작업은 메서드 중첩(narrow)사용
const sqlite3 = require('sqlite3').verbose();

// open the database connection
let db = new sqlite3.Database(':memory:', (err) => {
  if (err) throw err;
});

db.serialize(() => {
  // run메서드가 'Database'객체를 반환함(db반환)
  // 따라서 아래와 같이 체인형식으로 사용 가능
  // 단, 병렬처리 일 때는 사용불가
  // => 앞에 run이 끝나기 전에 뒤에 run이 호출될 수 있음!
  // Queries scheduled here will be serialized.
  db.run('CREATE TABLE greetings(message text)')
    .run(`INSERT INTO greetings(message)
          VALUES('Hi'),
                ('Hello'),
                ('Welcome')`)
    .each(`SELECT message FROM greetings`, (err, row) => {
      if (err) throw err;
      console.log(row.message);
    });
});