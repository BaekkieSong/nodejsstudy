// 업데이트 포맷
// UPDATE table_name
// SET column_name = value_1
// WHERE id = id_value;
const sqlite3 = require('sqlite3').verbose();
// open a database connection
let db = new sqlite3.Database('./sqlite3/mydb.db');

// 'Name'을 업데이트할 건데, 'Name'이 'A'인 값들만 찾아서 'Ansi C'로 변경
// 주의 - Name을 업데이트 하더라도, 업데이트할 대상을 찾는 WHERE는 다른 ID를 가질 수 있음
let data = ['Ansi C', 'A'];
let sql = `UPDATE langs
          SET name = ?
          WHERE name = ?`;

db.run(sql, data, function(err) {
  if (err) {
    return console.error(err.message);
  }
  console.log(`Row(s) updated: ${this.changes}`);

});

// close the database connection
db.close();