const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('./sqlite3/mydb.db',
  (err) => {
    if (err) throw err;
    console.log('Connected to the mydb database.');
  });

db.serialize(() => {
  db.run(`CREATE TABLE IF NOT EXISTS langs(Name Text)`);
  db.run(`INSERT INTO langs(Name) VALUES(?), (?)`, ['A', 'B'], (err) => {
    if (err) throw err;
    console.log(`Get Last Row ID: ${this.lastID}`)
  });

  let languages = ['C++', 'Python', 'Java', 'C#', 'Go'];
  console.log(languages.map((language) => `${language}`).join(','));
  // 그냥 단순히 language 개수만큼 (?) 붙이는 로직
  let placeholders = languages.map((language) => '(?)').join(',');
  let sql = 'INSERT INTO langs(Name) VALUES ' + placeholders;
  console.log(sql);
  db.run(sql, languages, function (err) {
    console.log(`Rows inserted ${this.changes}`);
  });
});


db.close((err) => {
  if (err) return console.error(err.message);
  console.log('closed');
})