import random
import string
import sys

print ('%0.30f' % random.random())
print (''.join(random.choice(string.ascii_uppercase + string.digits)
      for x in xrange(16)))
print (string.ascii_uppercase)
print (string.digits)
print ('%d^%d^%s' % (1, 2, 'aaa'))

class EntitySpec():
  def __init__(self, c):
    self.a = c
    print('init')
    #print('abc', self.a)
    
spec = EntitySpec(['bbb'])
desc = {
  'aaa': {'id': 1, 'name':'a'}, 
  'bbb': {'id': 2, 'name':'b'},
  'ccc': {'id': 3, 'name':'c'},
}
if 'aaa' in desc:
  selected_desc = desc['aaa']
  print (selected_desc['name'])
  print (getattr(spec, selected_desc['name']))
  #getattr(spec, selected_desc['name']).SetInParent()  #

def makeCurrentId(datatype, inner_id):
  return '%d^%d^%s' % (datatype, 1,inner_id)

print(makeCurrentId(2, 'ToGate_APP'))

ALL_TYPES = (
  TOP_LEVEL,
  APPS,
  APP_LIST,
  APP_SPEC,
) = range(4)
Sync_Type_To_Descriptor = {
  APPS: 'aa'
}
print (ALL_TYPES)
print (APPS)
print ('APPS:' + Sync_Type_To_Descriptor[APPS])
print (ALL_TYPES[APPS])
if APPS in Sync_Type_To_Descriptor:
  print('good')

# for c in xrange(ord('A'), ord('Z')): # 65~90
#   print(c)
lists = []
lists += (c for c in xrange(ord('A'), ord('Z')))
print (lists)
print (sys.maxint)
client_name = []
client_name = ('+' * times + chr(c) for times in xrange(0, sys.maxint) for c in xrange(ord('A'), ord('Z')))
# for i in range(100):
#   print('client_name:', client_name.next())

# class Error(Exception):
#   """ error asdasdasd"""
# if (True):
#   raise Error
data_types = [TOP_LEVEL, APPS, APP_LIST]
def shortSummary(data_types):
  print(set(data_types))
  included = set(data_types) - set([TOP_LEVEL])
  print('included:',included)
shortSummary(data_types)