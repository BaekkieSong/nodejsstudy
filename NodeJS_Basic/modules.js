// 주의! 이미 존재하는 코어 모듈의 경우('http' 등), 재정의해도 코어 모듈이 호출됨!
const circle = require('./poly1.js');
const {PI} = require('./poly1.js');
console.log(`The area of a circle of radius 4 is ${circle.area(4)}`);
console.log(`The area of a circle of radius 4 is ${circle.circumference(5)}`);
const sq = require('./poly2.js');  // == Square
const mySquare = new sq(3);
console.log(`The area of mySquare is ${mySquare.area()}`)

console.log(require.main);

const report = require('process');
console.log(report);
report.writeReport('./foo.json');