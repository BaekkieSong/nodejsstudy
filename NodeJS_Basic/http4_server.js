//<net.Server>의 확장
//request == <http.IncomingMessage>, response == <http.ServerResponse> res는 사용자가 아닌 서버 내부적으로 생성됨

const http = require('http');

//새로 고침할 때 불리네...
const server = http.createServer((req, res) => {
  // 큐에 있는 전송되지 않은 응답값 읽고 쓰기. 키 값은 대소구분하지 않음을 유의.
  // res.writeHead(200, { 'Content-Type': 'text/plain',
  //                         'Trailer': 'Content-MD5' });
  // res.write("dataata");
  // res.addTrailers({ 'Content-MD5': '7895bf4b8828b55ceaf47747b4bca667' });
  //console.log(req.headers);
  res.setHeader('Content-Type', 'text/html');
  res.setHeader('X-Foo', 'bar');
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('X-Foo', 'rabrab');
  res.writeHead(200, { 'Content-Type': 'text/plain' }); //Content-Type의 경우 이전 중복 타입은 제거됨
  res.end('okaaaaaa');  //와... 이거 바디값이구나....
  //console.log(res.getHeaderNames());
  console.log(res.getHeaders())
  //res.end();  //얘 없으니까 버퍼링이 안끝남...
  
});
server.on('clientError', (err, socket) => {
  //req, res가 없는 상태 -> socket에 직접 작성 필요
  // socket이 사용가능한지 확인
  if (err.code === 'ECONNRESET' || !socket.writable) {
    return;
  }
  // socket에 직접 작성
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});
server.listen(8000, () => {
  console.log("data");
});
server.on('request', () => {
  console.log("이런거 있엄? 근데 이거 왜 두번씩 불리냐");
}
);