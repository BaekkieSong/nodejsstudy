const util = require('util');
const { hasUncaughtExceptionCaptureCallback } = require('process');

const setImmediatePromise = util.promisify(setImmediate);
setImmediatePromise('foobar').then((value) => {
  // value === 'foobar' (passing values is optional)
  // This is executed after all I/O callbacks.
  console.log("콜백호출됨");
})
// Or with async function
async function timerExample() {
  console.log('Before I/O callbacks');
  await setImmediatePromise();
  console.log('After I/O callbacks');
}
//timerExample();

const setTimeoutPromise = util.promisify(setTimeout);
setTimeoutPromise(2000, 'foobar').then((value) => {
  // value === 'foobar' (passing values is optional)
  // This is executed after about 40 milliseconds.
  console.log("timeout 콜백");
});

setTimeoutPromise();