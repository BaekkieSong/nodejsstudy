http = require('http');
const querystring = require('querystring');
const { URL } = require('url');
const { validateHeaderValue } = require('http');

const postData = querystring.stringify({
  'msg': 'Hello World!'
});

const options2 = {
  hostname: 'www.google.com',
  port: 80,
  path: '/upload',
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': Buffer.byteLength(postData)
  }
};
const options3 = new URL('http://www.google.com');

const options = new URL('http://127.0.0.1:8000');

const req = http.request(options, (res) => {  //res는 콜백인데... IncomingMessage네....
  try {
    validateHeaderValue('content-type', undefined); //안되네 슈밤;;;
  } catch (err) {
    console.error(err.message);
  }
  console.log(`STATUS: ${res.statusCode}`);
  console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
  res.setEncoding('utf8');
  res.on('data', (chunk) => {
    console.log(`BODY: ${chunk}`);
  });
  res.on('end', () => {
    console.log('No more data in response.');
  });
});

req.on('error', (e) => {
  console.error(`problem with request: ${e.message}`);
});

// Write data to request body
req.write(postData);  //얘는 서버에서 어케 보는거지...?
req.end();