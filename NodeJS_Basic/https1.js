// curl -k https://localhost:8000/
const https = require('https');
const fs = require('fs');

//서버 만들려면 cert랑 key 필요한 듯...
// SSL 키 만들어 봄 
const options = {
  key: fs.readFileSync('NodeJS_Basic/key.pem'),
  cert: fs.readFileSync('NodeJS_Basic/cert.pem')
};

https.createServer(options, (req, res) => {
  res.writeHead(200);
  res.end('hello world\n');
}).listen(7000);

// 다른 방법으로 만든 키
https.createServer({
  key: fs.readFileSync('NodeJS_Basic/server.key'),
  cert: fs.readFileSync('NodeJS_Basic/server.cert'),
  
  requestCert: false,
  rejectUnauthorized: false
}, (req, res) => {
  res.end("Good Day Comando");
}).listen(4331, () => {
  console.log('Listening...');
});

// https.get('https://encrypted.google.com/', (res) => {
//   console.log('statusCode:', res.statusCode);
//   console.log('headers:', res.headers);

//   res.on('data', (d) => {
//     process.stdout.write(d);
//   });

// }).on('error', (e) => {
//   console.error(e);
// });