var fs = require('fs');
var {Console} = require('console');
var output = fs.createWriteStream('./stdout.log');
var errorOutput = fs.createWriteStream('./stderr.log');

// Custom simple logger
//const logger = new Console({ stdout: output, stderr: errorOutput });
// use it like console
const count = 5;
//logger.log('count: %d', count);
// In stdout.log: count 5
debugger;   //breakpoint 역할

console.trace('Show Me');

console.log('hello world');
// Prints: hello world, to stdout
console.log('hello %s', 'world');
// Prints: hello world, to stdout
console.error(new Error('Whoops, something bad happened')); //에러 bt 출력
// Prints: [Error: Whoops, something bad happened], to stderr

const name = 'Will Robinson';
console.warn(`Danger ${name}! Danger!`); //빨간색으로 출력해 줌
// Prints: Danger Will Robinson! Danger!, to stderr

