const fs = require('fs');

// 비동기 작업
//fs.unlink('./file.txt', (err) => {    // <- 비동기 형식의 마지막은 항상 완료 콜백. 성공시 null or undefined
//    if (err) throw err;     //파일이 없으면 에러 발생
//    console.log('delete file');
//});

// 동기 작업  //전체 프로세스 차단(모든 연결 중지)되므로 되도록 사용 X
try {
  fs.unlinkSync('./file.txt');
  console.log('delete file sync');
} catch (err) {
  //console.log(err);
  console.log(err['path'], err['code']);
}


// 비동기 작업할 때는 요러케하면 에러날 수 있음...(rename전 변경된 이름 참조) 
// 계속 콘솔 결과 순서가 바뀌는 것을 확인할 수 있음
// fs.rename('./NodeJS_Basic/hello.txt', './NodeJS_Basic/world.txt', (err) => {
//   if (err) throw err;
//   console.log('renamed complete');
// });
// fs.stat('./NodeJS_Basic/world.txt', (err, stats) => {
//   if (err) throw err;
//   console.log(`stats: ${JSON.stringify(stats)}`);
//   console.log('stat complete');
// });
// fs.rename('./NodeJS_Basic/world.txt', './NodeJS_Basic/hello.txt', (err) => {
//   if (err) throw err;
//   console.log('renamed complete2');
// });

// 따라서 PostTask 할 때처럼 다음 비동기 작업은 내부로
fs.rename('./NodeJS_Basic/hello.txt', './NodeJS_Basic/world.txt', (err) => {
  console.log('start');
  if (err) throw err;
  fs.stat('./NodeJS_Basic/world.txt', (err, stats) => {
    if (err) throw err;
    //console.log(stats);
    console.log(`stats: ${JSON.stringify(stats)}`);
    fs.rename('./NodeJS_Basic/world.txt', './NodeJS_Basic/hello.txt', (err) => {
      if (err) throw err;
      console.log("complete");
    });
  });
});


// 파일 경로
// buffer 또는 URL 객체 형식의 파일 경로도 허용됨
console.log('현재 경로: ', process.cwd()); 
fs.open(process.cwd() + '/NodeJS_Basic/file.txt', 'r', (err, fd) => {
  console.log('open file');
  if (err) throw err;
  fs.close(fd, (err) => {
    if (err) throw err;
    console.log('close file');
  });
});
const fileUrl = new URL('c://Users/Tmax/Documents/VSCODE_WORKSPACE/NodeJS_Basic/file.txt');  //항상 절대경로!
fs.readFileSync(fileUrl);