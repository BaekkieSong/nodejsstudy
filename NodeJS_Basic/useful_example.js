/* ...의 용도 확인 */
/*   배열 확장 */
var mid = [3, 4];
var arr = [1, 2, mid, 5, 6];
var extendedArr = [1, 2, ...mid, 5, 6];
console.log(arr);
console.log(extendedArr);
/*   수학 */
console.log(Math.max())  //무한
console.log(Math.max(100, 2, 3)) //100
console.log(Math.max.apply(null, extendedArr)) //6
console.log(Math.max(...extendedArr));  //==6
/*   배열 복사 */
var originArr = ['a', 'b', 'c'];
var referencedArr = originArr;
referencedArr[0] = 'd';
console.log(originArr); // ['d', 'b', 'c'] Not Deep Copied.
referencedArr[0] = 'a'; // 복구
var deepCopiedArr = [...originArr]; // originArr이 deepCopiedArr 배열 정의를 채우기 위해 확장되었음
deepCopiedArr[0] = 'e';
console.log(originArr);
console.log(deepCopiedArr);
/*   문자열 배열화 */
var str = "hello";
var chars = [...str];
console.log(chars);


/* map, filter, reduce 활용 */
//Array.reduce((acc, cur, index) => {}, acc init_value) //init_value를 []로 하면 acc = []로 시작하는거랑 같음. 0으로 하면 acc=0으로 시작하는거랑 같음.
//Array.map((cur, index, Array) => {return (new)cur}) //배열에 1:1 대응하는 새로운 값을 추출하여 새로운 배열 반환
/*   공통 데이터 정의 */
let values = [1, 3, 2, 4, 6, 8, 1, 1, 1, 1,];
let groups = [
  { id: 6, name: 'aaa'},
  { id: 2, name: 'bbb'},
  { id: 1, name: 'ccc'},
  { id: 5, name: 'ddd'},
  { id: 1, name: 'eee'},
];
let nestedArr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
const AllTypesObject = {
  TOP_LEVEL: 0,  // Top level folder name. 'Google Chrome' => 'ToGate'
  APP: 1,
  APP_LIST: 2,
  APP_NOTIFICATION: 3,
  APP_SETTING: 4,
  ARC_PACKAGE: 5,
};
/*   기본 사용 */
// () => () 형태로 하면 ({}안쓰고) ()에 값이 자동으로 리턴됨
let reduceExample = groups.reduce((acc, cur, index) => { return acc + cur.id;}, 0);
console.log('Reduce',reduceExample);
let mapExample = groups.map((cur, index) => cur.name + '_' + index );
console.log('Map',mapExample);
/*   1. 중복 제거 */
let unique_values = [...new Set(values)]; //...: 확산 구문. iterable을 확장하는 용도(배열 확장)
let set_values = new Set(values);
console.log(unique_values);
console.log(set_values);
/*   2. 검색(case-sensitive) */
let filteredArr = groups.filter((it) => { return it.id.toString().includes('1')});
let filteredArr2 = groups.filter((it) => it.id.toString().includes('1'));  // 위에꺼랑 똑같..
console.log(filteredArr);
/*   3. 검색(case-insensitive) */
let filteredArrInsensitive = groups.filter(it => new RegExp('AA', "i").test(it.name))
console.log(filteredArrInsensitive);
/*   4. 특정 값 존재여부 */
let hasName = groups.some(it => it.name === 'aaa');
console.log('name has:', hasName);
/*   5. arr펼치기 */
let flatArr = [].concat.apply([], nestedArr);
console.log(flatArr);
/*   6. 특정키 빈도 */
let groupsWithCount = groups.reduce((acc, it) => ({ ...acc, [it.id]: (acc[it.id] || 0) + 1 }), {});
console.log(groupsWithCount);
/*   7. 객체들의 배열화 */
let gropusTable = groups.reduce((acc, it) => ({...acc, [it.id]: it }), {});
console.log(gropusTable);
/*   9. 키-값 역전 */
let typesValues = Object.keys(AllTypesObject).reduce(
  (acc, k) => (acc[AllTypesObject[k]] = [...(acc[AllTypesObject[k]] || []), k], acc) , {});
console.log('9: ', typesValues);
/*   10. 맵을 이용한 값변환 */
let celsius = [-15, -5, 0, 10, 16, 20, 24, 32]
let fahrenheit = celsius.map(t => t * 1.8 + 32);
/*   11. 객체 To QueryString */
let params = {lat: 45, lng: 6, alt: 1000};
let queryString = Object.entries(params).map(p => encodeURIComponent(p[0]) + '=' + encodeURIComponent(p[1])).join('&')
console.log(queryString);
/*   12. 명시된 키 값 추출해 사용 */
let groupStr = groups.map(({id, name}) => `\n${id} ${name}`)//.join(' ');
console.log(groupStr);
/*   13. 객체 배열에서 특정 값 업데이트 */    //react에서 사용한 방법으로, 좀 빠르대..
let updatedUsers = groups.map(
  p => p.id !== 5 ? p : {...p, name: p.name + 'X'}
);
console.log(updatedUsers);
/*   14. 배열 합집합/ 교집합 */
let arrA = [1, 4, 3, 2];
let arrB = [4, 2, 6, 7];
let sumArr = [...new Set([...arrA, ...arrB])];
let filterArr = arrA.filter(it => arrB.includes(it));
console.log(sumArr)
console.log(filterArr)

// const all_types_arr = [...new Set(AllTypesObject)];
// console.log(all_types_arr);


const AAA = {
  A: 'A',
  B: 'B',
  C: 'C',
};
const BBB = {
  'A': 1,
  B: 2,
}
function aaa(x) {
  console.log('A:', AAA[x]);
  console.log('B:', BBB[AAA[x]])
}
aaa('A');
let a = Object.entries(AAA).find((x) => x[0] == 'A' || 'B');
console.log(a)

var assert = require('assert');
function axx() {
  let a=0;
  return a+=1,assert(a == 1), a = 2;
}
let xxx = axx();
console.log(xxx);
console.log(axx());