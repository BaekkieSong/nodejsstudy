# 디버깅 방법
* node inspect [js이름]
* (debug모드 진입)
* 파일 내 원하는 곳에 breakpoint 삽입 (== debugger;)
* 또는 setBreakPoint(sb)를 이용해 break걸기 (입력x: 현재라인, line: 지정라인, 'fn()': 함수, '.js', line:특정 파일 특정 라인)
* clearBreakpoint(cb)를 이용해 break 제거 가능
* run
* cont (continue, c)
* next (n)
* repl - 값 출력 모드 진입 (코드 원격 평가)
* help 기타 명령 확인

* 주의!  '--'여부 확인
  * node --inspect/--inspect-brk [js이름] : v8 Inspector를 이용한 Chrome DevTools 디버깅 및 프로파일링에 사용됨