const http = require('http');
const options = {
  host: 'www.google.com',
};
const req = http.get(options);  //http.request()와 동일한 역할 수행. 차이는 GET요청에만 쓰이며, req.end()를 자동호출함.
req.end();
req.once('response', (res) => {
  console.log(res.httpVersion);
  console.log(res.headers);
  const ip = req.socket.localAddress;
  const port = req.socket.localPort;
  console.log(`Your IP address is ${ip} and your source port is ${port}.`);
  // Consume response object
});

// 안되서 일단 보류;;;
// const req2 = http.request({  //options를 바로 삽입한 상태.
//   host: '127.0.0.1',
//   port: 8000,
//   method: 'GET'
// }, (res) => { //res == IncomingMessage
//   res.resume();
//   res.on('end', () => {
//     if (!res.complete)
//       console.error(
//         'The connection was terminated while the message was still being sent');
//     console.log("complete");
//   });
// });
// req2.once('response', (res) => {
//   console.log("asdfasdf");
// });
// req2.on('connect', (res, socket, head) => {  //res:httpIncomingMessage, socket: <stream.Duplex>, head: buffer
//   console.log('got connected!');
// });