// crypto 암호화 사용
//let crypto;
function cryptoUsage() {
    try {
        const crypto2 = require('crypto');
        return crypto2;
    } catch (err) {
        console.log("크립토 사용 불가");
    }
};
var crypto2 = cryptoUsage();
const secret = 'abcedfg';   //암호화 키
const hash = crypto2.createHash('sha256', secret).update('Maybe anything?').digest('hex'); // update: ??
console.log(hash);

//SPKAC는 원래 Netscape에 의해 구현 된 인증서 서명 요청 메커니즘
//Node.js는 OpenSSL의 SPKAC구현을 내부적으로 사용하며, crypto모듈에서 Certificate SPKAC데이터 작업을 위한 클래스 제공
//const { Certificate } = require('crypto');
const cert = require('crypto').Certificate();
const cert1 = new crypto2.Certificate();
//const spkac = getSpkacSomehow();
const publicKey = crypto2.Certificate
console.log(publicKey);

// NodeJS 10버전 이상에서만 사용가능한 cipher로직.
// const crypto = require('crypto');
// const algorithm = 'aes-192-cbc';
// const password = 'Password used to generate key';
// // Key length is dependent on the algorithm. In this case for aes192, it is
// // 24 bytes (192 bits).
// // Use async `crypto.scrypt()` instead.
// const key = crypto.scryptSync(password, 'salt', 24);
// // Use `crypto.randomBytes()` to generate a random iv instead of the static iv
// // shown here.
// const iv = Buffer.alloc(16, 0); // Initialization vector.
// const cipher = crypto.createCipheriv(algorithm, key, iv);
// let encrypted = '';
// cipher.on('readable', () => {
//   let chunk;
//   while (null !== (chunk = cipher.read())) {
//     encrypted += chunk.toString('hex');
//   }
// });
// cipher.on('end', () => {
//   console.log(encrypted);
//   // Prints: e5f79c5915c02171eec6b212d5520d44480993d7d622a7c4c2da32f6efda0ffa
// });
// cipher.write('some clear text data');
// cipher.end();

const crypto = require('crypto');
const assert = require('assert');

// Generate Alice's keys...
const alice = crypto.createDiffieHellman(2048);
const aliceKey = alice.generateKeys();

console.log("alicekey: ", aliceKey);    //공개키 1

// Generate Bob's keys...
const bob = crypto.createDiffieHellman(alice.getPrime(), alice.getGenerator());
const bobKey = bob.generateKeys();

console.log("bobkey: ", bobKey);        //공개키 2

// Exchange and generate the secret...
const aliceSecret = alice.computeSecret(bobKey);    //개인키 
const bobSecret = bob.computeSecret(aliceKey);      //==개인키

// OK
assert.strictEqual(aliceSecret.toString('hex'), bobSecret.toString('hex'));
console.log(bobSecret, '\n', aliceSecret);