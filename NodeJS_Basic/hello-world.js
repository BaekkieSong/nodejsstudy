const assert = require('assert').strict; //엄격한 assert모드

const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    console.log("req Code: " + req)
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('....text body?\n');
});

server.listen(port, hostname, ()=> {
    //`를 사용해서 $값을 출력되게 할 수 있음
    console.log(`server addr is http://${hostname}:${port}/`);
});
