export const { PI } = Math;

// exports 키워드에 의해 함수 및 객체가 모듈의 Root로 추가됨
exports.area = (r) => PI * r ** 2;  //r을 인자로 받아 계산한 값을 저장한 변수

exports.circumference = (r) => 2 * PI * r; //r을 인자로 받아 계산한 값을 저장한 변수

