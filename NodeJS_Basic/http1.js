const http = require('http');
const net = require('net');
const { URL } = require('url');

//console.log(http);
//console.log(http.Agent);

// Create an HTTP tunneling proxy
const proxy = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('okay');
});
proxy.on('connect', (req, clientSocket, head) => {  //
  // Connect to an origin server
  const { port, hostname } = new URL(`http://${req.url}`);
  debugger;
  const serverSocket = net.connect(port || 80, hostname, () => {
    clientSocket.write('HTTP/1.1 200 Connection Established\r\n' +
      'Proxy-agent: Node.js-Proxy\r\n' +
      '\r\n');
    serverSocket.write(head);
    serverSocket.pipe(clientSocket);
    clientSocket.pipe(serverSocket);
  });
});

// Now that proxy is running
proxy.listen(1337, '127.0.0.1', () => {

  // Make a request to a tunneling proxy
  const options = {
    port: 1337,
    host: '127.0.0.1',
    method: 'CONNECT',
    path: 'www.google.com:80' //==req.url
  };
  // http.ClientRequest를 생성하여 반환됨. 헤더가 이미 큐잉된, 진행중인 요청을 나타냄. 스트림의 연장.
  // setHeader/getHeader/removeHeader API를 통해 헤더 변경 가능
  // 실제 헤더는 첫 번째 데이터 chunk 또는 request.end()가 호출될 때 보내지게 됨
  // 응답을 위해 'response' 에 대한 리스너 추가 필요. 
  // 'response' 응답 헤더가 수신되면 request객체에서 생성됨(http.IncomingMessage)
  // 'response' 이벤트 동안 응답객체에 리스너를 추가할 수 있고, 'data' 이벤트로 수신할 수 있음
  // 'response' 핸들러가 추가되지 않으면 응답이 완전히 삭제됨
  // 반대로 핸들러를 추가했다면, 응답객체의 data는 반드시 '소비'되야 함
  // response.read(), 'data', .resume() 등...
  // 'data'가 모두 '소비'될 때까지 'end' 이벤트가 호출되지 않음 -> 이로 인해 메모리가 계속 상주하여 leak 발생할 수 있다!
  // 응답객체는 너무 일찍 close될 경우 'error'이벤트 대신 'abort' 이벤트를 발생(emit)시킴 (원래 abort는 클라이언트 요청 중단에의해 발생)
  // 주의 - NodeJS에서는 Content-length와 전송 본문의 길이를 같은지 따로 확인하진 않음!
  const req = http.request(options);
  req.end();

  // 서버에서 1xx 응답값(101제외)을 보낼 때만 발생한다고 함
  req.on('information', (info) => {
    debugger; //왜 안걸리냐...
    console.log(`INFO: ${info}`);
  });
  
  // 서버가 요청에 응답할 때 발생하는 이벤트. 
  // 따로 'connect' 이벤트 수신하지 않을 경우 응답시 연결 닫음
  // 사용자가 소켓 타입을 지정하지 않으면 <net.Socket>(<stream.Duplex>의 서브클래스) 사용
  req.on('connect', (res, socket, head) => {  //res:httpIncomingMessage, socket: <stream.Duplex>, head: buffer
    console.log('got connected!');
    debugger;
    // Make a request over an HTTP tunnel
    socket.write('GET / HTTP/1.1\r\n' +
      'Host: www.google.com:80\r\n' +
      'Connection: close\r\n' +
      '\r\n');
    socket.on('data', (chunk) => {
      console.log(chunk.toString());
    });
    socket.on('end', () => {
      proxy.close();
    });
  });
});