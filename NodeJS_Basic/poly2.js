// Square클래스와 width를 인자로 받는 생성자, 메서드 정의
module.exports = class Square {
  constructor(width) {
    this.width = width;
  }

  area() {
    return this.width ** 2;
  }
};