const http2 = require('http2');
const fs = require('fs');

// http2.createServer(), http2.createSecureServer()의 리턴값은 각각 net.Server or tls.Server
//const server = http2.createServer(); //이건 대체 언제 쓸 수 있는거지...? 기본이 보안 연결인데...

// Create an unencrypted HTTP/2 server
const server = http2.createSecureServer({ //
  key: fs.readFileSync(__dirname + '/localhost-privkey.pem'),
  cert: fs.readFileSync(__dirname + '/localhost-cert.pem')
});

// 서버측에 세션 이벤트 발생 -> 세션에서 Stream 이벤트 발생시
// server.on('session', (session)=> {
//   session.on('stream', (stream, headers, flags) => {
//     const method = headers[':method'];
//     const path = headers[':path'];
//     // ...
//     stream.respond({
//       ':status': 200,
//       'content-type': 'text/plain'
//     });
//     stream.write('hello ');
//     stream.end('world');
//   });
  // session.altsvc('h2=":8000"', 'https://example.org:80'); //ServerHttp2Session. //code error임. url변경 필요
//   session.setTimeout(2000); //그냥 2초 후에 timeout 불리게 해주네...
//   session.on('timeout', () => { console.log("와... 타임아웃됫다...");});
// });

// 서버측에 Stream 이벤트 발생시
// 서버측에선 Http2Stream 이벤트를 직접 수신하는 대신 'stream'이벤트에 대한 핸들러를 등록하여 처리
// Http2Stream과 네트워크 소켓이 1:1이 아니더라도 네트워크 오류는 개별 스트림을 파괴함 => 최대한 스트림레벨에서 처리하도록 한다.
server.on('stream', (stream, headers) => {
  stream.respond({
    'content-type': 'text/html',
    ':status': 200
  });
  stream.on('error', (error) => console.error(error));
  stream.session.altsvc('h2=":8000"', stream.id); //특정 스트림ID를 가진 프레임 전송시, 대체 서비스가 Stream원점과 연관됨을 알림
  stream.write('aaaaa');
  stream.end('<h1>Hello World</h1>');
});

server.listen(1123);

