const http2 = require("http2");	//experimental....

// 구글 서버에 연결된 클라이언트 생성
const client = http2.connect("https://www.google.com");
// 클라이언트의 Request 생성
const req = client.request({
	":path": "/"
});
// Response 데이터를 저장할 변수
let data = "";
// Request에 대한 Response을 수신. 한 번만 발생함
req.on("response", (headers, flags) => {
	for (const name in headers) {
		console.log(`${name}: ${headers[name]}`);
	}
});
// Request에 대한 응답 data가 있으면 catch.
req.on("data", chunk => {
	data += chunk;
});
// 요청 전송 완료.
req.on("end", () => {
	console.log(data);
	client.close();
});
// Request 종료
req.end();