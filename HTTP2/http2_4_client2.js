//ClientHttp2Session

// const http2 = require('http2');
// const fs = require('fs');
// const client = http2.connect('https://localhost:1123', { // const client = http2.connect('https://localhost:1123');
//   ca: fs.readFileSync('localhost-cert.pem')
// });
// client.on('altsvc', (alt, origin, streamId) => {
//   console.log(alt);
//   console.log(origin);
//   console.log(streamId);
// });
// const req = client.request({ ':path': '/' });
// let data=""
// req.on('data', chunk => {
//   data += chunk;
// })
// req.on('end', ()=> {
//   console.log(data);
//   client.close();
// })

const http2 = require('http2');
const fs = require('fs');
const clientSession = http2.connect('https://localhost:1123', { // const client = http2.connect('https://localhost:1123');
  ca: fs.readFileSync('localhost-cert.pem')
});
const {
  HTTP2_HEADER_PATH,
  HTTP2_HEADER_STATUS,
  NGHTTP2_CANCEL,
} = http2.constants;

const req = clientSession.request({ [HTTP2_HEADER_PATH]: '/' });  //== ':path': '/'
req.on('response', (headers) => {
  let data="";
  console.log(headers[HTTP2_HEADER_STATUS]);
  req.on('data', (chunk) => { data+=chunk; });
  req.on('end', () => { console.log(data);});
  
  clientSession.setTimeout(5000, () => clientSession.close(NGHTTP2_CANCEL));//clientSession.close();//얘 안하면 클라이언트 종료가 안됨...
  //req.setTimeout(5000, () => req.close(NGHTTP2_CANCEL));// Cancel the stream if there's no activity after 5 seconds//동작안함
});

