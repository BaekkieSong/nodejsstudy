var fs = require('fs');
fs.readFile(__dirname + '/checkbox.html', 'utf8', (err, data) => { //encoding 안적어주면 깨지네.... Buffer가 출력됨
  console.log('aaa');
  console.log(data);
});

readDir();
fs.readdir(__dirname + '/example', (err, files) => {
  console.log("DIr: ");
  console.log(files);
})

function readDir() {
  fs.readdir(__dirname + '/example', (err, files) => {
    console.log('read dir:');
    console.log(files);
  })
  return 200;
};

readDir();

// 싱크가 먼저 처리되는 건 당연함. (비동기는 콜백구조라 현재사이클이 다 돌아야 처리됨)
var result = fs.readFileSync(__dirname + '/checkbox.html', 'utf8');
console.log('싱크로: ' + result);

var func = function() {
  console.log('B');
}
// == function func() { console.log('B'); }

function slowFunc(callback) {
  callback();
}
console.log('A');
slowFunc(func); //얘는 비동기는 아니니까 이름만 콜백이지 바로 실행됨
console.log('C');
