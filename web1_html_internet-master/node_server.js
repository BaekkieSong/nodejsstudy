var http = require('http');
var fs = require('fs');
var app = http.createServer(function(request,response){
    var url = request.url;
    console.log(url);
    if(request.url == '/'){
      url = '/index.html';
    }
    if(request.url == '/favicon.ico'){
			response.writeHead(404);
			response.end();
			return;
    }
    response.writeHead(200);
    // 실제로 화면에 그려질(클라이언트에 제공할) 응답 정보를 적어주게 됨.
    // response.end("이게 화면에 출력된다고");
    response.end(fs.readFileSync(__dirname + url)); //접속할 페이지 경로 제공. 경로줘야 열리지...
    console.log(__dirname + url);
});
//default 웹서버 포트번호: 80
app.listen(3000);