var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');
var path =require('path');

function templateHTML(title, list, body) {
  return `
  <!doctype html>
  <html>
  <head>
    <title> WEB </title>
    <meta charset="utf-8">
  </head>
  <body>
    <h1><a href="/">WEB</a></h1>
    ${list}
    <a href="/create">create</a>
    ${body}
  </body>
  </html>
  `;
}

function templateList(files) {  //파일목록을 이용해 html 리스트 생성 
  var list = '<ul>';
  var i = 0;
  while(i < files.length) {
    list = list + `<li><a href="/?id=${files[i]}">${files[i]}</a></li>`;
    i = i + 1;
  }
  lsit = list + '</ul>';
  return list;
};

var server = http.createServer((req, res) => {
  console.log(req.url);
  var _url = req.url;
  var queryData =  url.parse(_url, true).query; // 얘를 그냥쓰면 키값을 이용해 상위경로 참조할 수도 있음...
  //ex) localhost:3000/?id=/../OtherFolder/aaa.js
  var filteredID = path.parse(queryData.id).base; //상위 경로 무시하고 현재 키값만 가져오도록 함
  console.log(filteredID);  
  var pathname = url.parse(_url, true).pathname;
  
  if (pathname == '/') {
    if (filteredID == undefined) {
      fs.readdir(__dirname, (err, files) => {
        var title = "Welcome";
        var desc = "This is the main texts";
        var list = templateList(files);
        var template = templateHTML(title, list, `<h2>${title}</h2>${desc}`);
        res.writeHead(200);
        res.end(template);
      });
    } else {
      fs.readdir(__dirname, (err, files) => {
        fs.readFile(__dirname + `${filteredID}`, 'utf8', (err, data) => {
          var title = queryData.id;
          var list = templateList(files);
          var template = templateHTML(title, list, `<h2>${title}</h2>${data}`);
          res.writeHead(200);
          res.end(template);
        })
      });
    }
  } else if (pathname == '/create') {
    fs.readdir(__dirname, (err, files) => {
      var title = "WEB - create";
      var list = templateList(files);
      var template = templateHTML(title, list, `
      <form action="http://localhost:3000/process_create" method="post">
        <p><input type="text" name="title",placeholder="title"></p>
        <p>
          <textarea name="desc" placeholder="description"></textarea>
        </p>
        <p>
        <input type="submit">
        </p>
      </form>
      `); //post를 사용하면 querystring을 감춘상태로 url만 전송됨!
      res.writeHead(200);
      res.end(template);
    })
  } else if (pathname == '/process_create') {
    var body = '';
    req.on('data', (chunk) => {
      body += chunk;
    })
    req.on('end', () => {
      console.log(body);
      var post = qs.parse(body);
      console.log(post);
      var title = post.title;
      var desc = post.desc;
      console.log(title);
      console.log(desc);
    })
    res.writeHead(200);
    res.end('success');
  } else {
    // console.log(pathname);
    // console.log(__dirname + req.url);
    // res.end(fs.readFileSync(__dirname + req.url));
    res.writeHead(404);
    res.end("Not Found");
  }
})
server.listen(3000);