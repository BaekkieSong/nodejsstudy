var http = require('http');
var fs = require('fs');
var qs = require('querystring');
var url = require('url');

var app = http.createServer(function (request, response) {
  var resUrl = request.url;  //여기에 querystring이 들어옴
  console.log(resUrl);
  var queryData = url.parse(resUrl, true).query;
  var title = queryData.id;
  console.log(title);
  if (resUrl == '/') {
    title = 'InitPage';
  }
  if (resUrl == '/favicon.ico') {
    response.writeHead(404);
    response.end();
    return;
  }
  //동일한 부분은 그대로 사용하고, 변경될 부분만 새로 로드해서 그려주는 방법 제공!!!!!
  fs.readFile(__dirname + `/${queryData.id}`, 'utf8', (err, data) => {
    var contents = `
    <!doctype html>
    <html>
    <head>
      <title>WEB - ${title}</title>
      <meta charset="utf-8">
    </head>
    <body>
      <h1><a href="/">WEB - ${title}</a></h1>
      <ol>
        <li><a href="/?id=HTML">HTML</a></li>
        <li><a href="/?id=CSS">CSS</a></li>
        <li><a href="/?id=JavaScript">JavaScript</a></li>
      </ol>
      <p>${data}</p>
    </body>
    </html>
    `;
    response.writeHead(200);
  // 실제로 화면에 그려질(클라이언트에 제공할) 응답 정보를 적어주게 됨.
  // response.end("이게 화면에 출력된다고");
    response.end(contents); //접속할 페이지 경로 제공. 경로줘야 열리지...
  });

  
  
});
//default 웹서버 포트번호: 80
app.listen(3000);