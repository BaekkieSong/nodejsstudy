var protobuf = require('protobufjs');

console.log(__dirname + "/test.proto");
protobuf.load(__dirname + "/test.proto", (error, root) => {
  if (error)
    throw error;

  // 일단 원하는 message타입을 얻어옴
  var message = root.lookupType('testPackage.Request');
  //console.log(message);
  //let jsonsss = message.create();
  let jsonsss = {};
  jsonsss.query = 'aaa';
  jsonsss.a = 2;
  console.log(jsonsss);  //JSON.stringify(jsonsss)
  var errMsged = message.verify(jsonsss); // 잘못된 필드값을 거르지는 못함
  if (errMsged) {
    console.log(errMsged);
  }
  let msg = message.fromObject(jsonsss);
  console.log(msg);
  //console.log('tojson:',jsonssss);


  // 삽입할 fileds값 정의
  var fields = {
    query: "string",
    number: 1,
    result: 200,
  }

  // fields 값이 적절한지 확인
  var errMsg = message.verify(fields);
  if (errMsg)
    throw Error(errMsg);

  var real_message = message.create(fields);
  console.log("Real: ", real_message);
  // protobuf to Buffer serialize
  var buffer = message.encode(real_message).finish();
  console.log("MessageToBuffer: ", buffer);
  // Buffer to protobuf message
  var decoded_message = message.decode(buffer);
  console.log("BufferToMessage: ", decoded_message);

  // Json
  var object = message.toObject(decoded_message, {
    longs: String,
    enums: String,
    bytes: String,
  })
  console.log("ToObject: ", object);
  // Json2
  console.log("Json?: ", real_message.toJSON());
  console.log("field1: ", real_message.toJSON()['query']);
  // Real Json
  var json = message.toJSON(decoded_message);
  console.log("ToJson: ", json);
  console.log(json.fields.query.rule);
})