/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var unique_position_pb = require('./unique_position_pb.js');
goog.object.extend(proto, unique_position_pb);
goog.exportSymbol('proto.sync_pb.EntityMetadata', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.EntityMetadata = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.EntityMetadata, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.EntityMetadata.displayName = 'proto.sync_pb.EntityMetadata';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.EntityMetadata.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.EntityMetadata.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.EntityMetadata} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EntityMetadata.toObject = function(includeInstance, msg) {
  var obj = {
    clientTagHash: jspb.Message.getField(msg, 1),
    serverId: jspb.Message.getField(msg, 2),
    isDeleted: jspb.Message.getField(msg, 3),
    sequenceNumber: jspb.Message.getField(msg, 4),
    ackedSequenceNumber: jspb.Message.getField(msg, 5),
    serverVersion: jspb.Message.getFieldWithDefault(msg, 6, -1),
    creationTime: jspb.Message.getField(msg, 7),
    modificationTime: jspb.Message.getField(msg, 8),
    specificsHash: jspb.Message.getField(msg, 9),
    baseSpecificsHash: jspb.Message.getField(msg, 10),
    uniquePosition: (f = msg.getUniquePosition()) && unique_position_pb.UniquePosition.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.EntityMetadata}
 */
proto.sync_pb.EntityMetadata.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.EntityMetadata;
  return proto.sync_pb.EntityMetadata.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.EntityMetadata} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.EntityMetadata}
 */
proto.sync_pb.EntityMetadata.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setClientTagHash(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setServerId(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsDeleted(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSequenceNumber(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setAckedSequenceNumber(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setServerVersion(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCreationTime(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setModificationTime(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setSpecificsHash(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setBaseSpecificsHash(value);
      break;
    case 11:
      var value = new unique_position_pb.UniquePosition;
      reader.readMessage(value,unique_position_pb.UniquePosition.deserializeBinaryFromReader);
      msg.setUniquePosition(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.EntityMetadata.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.EntityMetadata.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.EntityMetadata} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.EntityMetadata.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBool(
      3,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeInt64(
      8,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeString(
      9,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 10));
  if (f != null) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getUniquePosition();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      unique_position_pb.UniquePosition.serializeBinaryToWriter
    );
  }
};


/**
 * optional string client_tag_hash = 1;
 * @return {string}
 */
proto.sync_pb.EntityMetadata.prototype.getClientTagHash = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/** @param {string} value */
proto.sync_pb.EntityMetadata.prototype.setClientTagHash = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearClientTagHash = function() {
  jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasClientTagHash = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string server_id = 2;
 * @return {string}
 */
proto.sync_pb.EntityMetadata.prototype.getServerId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/** @param {string} value */
proto.sync_pb.EntityMetadata.prototype.setServerId = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearServerId = function() {
  jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasServerId = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool is_deleted = 3;
 * Note that Boolean fields may be set to 0/1 when serialized from a Java server.
 * You should avoid comparisons like {@code val === true/false} in those cases.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.getIsDeleted = function() {
  return /** @type {boolean} */ (jspb.Message.getFieldWithDefault(this, 3, false));
};


/** @param {boolean} value */
proto.sync_pb.EntityMetadata.prototype.setIsDeleted = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearIsDeleted = function() {
  jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasIsDeleted = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 sequence_number = 4;
 * @return {number}
 */
proto.sync_pb.EntityMetadata.prototype.getSequenceNumber = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/** @param {number} value */
proto.sync_pb.EntityMetadata.prototype.setSequenceNumber = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearSequenceNumber = function() {
  jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasSequenceNumber = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 acked_sequence_number = 5;
 * @return {number}
 */
proto.sync_pb.EntityMetadata.prototype.getAckedSequenceNumber = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/** @param {number} value */
proto.sync_pb.EntityMetadata.prototype.setAckedSequenceNumber = function(value) {
  jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearAckedSequenceNumber = function() {
  jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasAckedSequenceNumber = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int64 server_version = 6;
 * @return {number}
 */
proto.sync_pb.EntityMetadata.prototype.getServerVersion = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, -1));
};


/** @param {number} value */
proto.sync_pb.EntityMetadata.prototype.setServerVersion = function(value) {
  jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearServerVersion = function() {
  jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasServerVersion = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional int64 creation_time = 7;
 * @return {number}
 */
proto.sync_pb.EntityMetadata.prototype.getCreationTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/** @param {number} value */
proto.sync_pb.EntityMetadata.prototype.setCreationTime = function(value) {
  jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearCreationTime = function() {
  jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasCreationTime = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional int64 modification_time = 8;
 * @return {number}
 */
proto.sync_pb.EntityMetadata.prototype.getModificationTime = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/** @param {number} value */
proto.sync_pb.EntityMetadata.prototype.setModificationTime = function(value) {
  jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearModificationTime = function() {
  jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasModificationTime = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional string specifics_hash = 9;
 * @return {string}
 */
proto.sync_pb.EntityMetadata.prototype.getSpecificsHash = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/** @param {string} value */
proto.sync_pb.EntityMetadata.prototype.setSpecificsHash = function(value) {
  jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearSpecificsHash = function() {
  jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasSpecificsHash = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string base_specifics_hash = 10;
 * @return {string}
 */
proto.sync_pb.EntityMetadata.prototype.getBaseSpecificsHash = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/** @param {string} value */
proto.sync_pb.EntityMetadata.prototype.setBaseSpecificsHash = function(value) {
  jspb.Message.setField(this, 10, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearBaseSpecificsHash = function() {
  jspb.Message.setField(this, 10, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasBaseSpecificsHash = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional UniquePosition unique_position = 11;
 * @return {?proto.sync_pb.UniquePosition}
 */
proto.sync_pb.EntityMetadata.prototype.getUniquePosition = function() {
  return /** @type{?proto.sync_pb.UniquePosition} */ (
    jspb.Message.getWrapperField(this, unique_position_pb.UniquePosition, 11));
};


/** @param {?proto.sync_pb.UniquePosition|undefined} value */
proto.sync_pb.EntityMetadata.prototype.setUniquePosition = function(value) {
  jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.EntityMetadata.prototype.clearUniquePosition = function() {
  this.setUniquePosition(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.EntityMetadata.prototype.hasUniquePosition = function() {
  return jspb.Message.getField(this, 11) != null;
};


goog.object.extend(exports, proto.sync_pb);
