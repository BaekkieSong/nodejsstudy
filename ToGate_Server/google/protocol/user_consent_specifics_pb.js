/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var sync_enums_pb = require('./sync_enums_pb.js');
goog.object.extend(proto, sync_enums_pb);
var user_consent_types_pb = require('./user_consent_types_pb.js');
goog.object.extend(proto, user_consent_types_pb);
goog.exportSymbol('proto.sync_pb.UserConsentSpecifics', null, global);
goog.exportSymbol('proto.sync_pb.UserConsentSpecifics.Feature', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.UserConsentSpecifics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.UserConsentSpecifics.repeatedFields_, proto.sync_pb.UserConsentSpecifics.oneofGroups_);
};
goog.inherits(proto.sync_pb.UserConsentSpecifics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.UserConsentSpecifics.displayName = 'proto.sync_pb.UserConsentSpecifics';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.UserConsentSpecifics.repeatedFields_ = [2];

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.sync_pb.UserConsentSpecifics.oneofGroups_ = [[7,8,9,10,11,13,14]];

/**
 * @enum {number}
 */
proto.sync_pb.UserConsentSpecifics.ConsentCase = {
  CONSENT_NOT_SET: 0,
  SYNC_CONSENT: 7,
  ARC_BACKUP_AND_RESTORE_CONSENT: 8,
  ARC_LOCATION_SERVICE_CONSENT: 9,
  ARC_PLAY_TERMS_OF_SERVICE_CONSENT: 10,
  ARC_METRICS_AND_USAGE_CONSENT: 11,
  UNIFIED_CONSENT: 13,
  ASSISTANT_ACTIVITY_CONTROL_CONSENT: 14
};

/**
 * @return {proto.sync_pb.UserConsentSpecifics.ConsentCase}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getConsentCase = function() {
  return /** @type {proto.sync_pb.UserConsentSpecifics.ConsentCase} */(jspb.Message.computeOneofCase(this, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.UserConsentSpecifics.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.UserConsentSpecifics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.UserConsentSpecifics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.UserConsentSpecifics.toObject = function(includeInstance, msg) {
  var obj = {
    locale: jspb.Message.getField(msg, 4),
    clientConsentTimeUsec: jspb.Message.getField(msg, 12),
    syncConsent: (f = msg.getSyncConsent()) && user_consent_types_pb.UserConsentTypes.SyncConsent.toObject(includeInstance, f),
    arcBackupAndRestoreConsent: (f = msg.getArcBackupAndRestoreConsent()) && user_consent_types_pb.UserConsentTypes.ArcBackupAndRestoreConsent.toObject(includeInstance, f),
    arcLocationServiceConsent: (f = msg.getArcLocationServiceConsent()) && user_consent_types_pb.UserConsentTypes.ArcGoogleLocationServiceConsent.toObject(includeInstance, f),
    arcPlayTermsOfServiceConsent: (f = msg.getArcPlayTermsOfServiceConsent()) && user_consent_types_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent.toObject(includeInstance, f),
    arcMetricsAndUsageConsent: (f = msg.getArcMetricsAndUsageConsent()) && user_consent_types_pb.UserConsentTypes.ArcMetricsAndUsageConsent.toObject(includeInstance, f),
    unifiedConsent: (f = msg.getUnifiedConsent()) && user_consent_types_pb.UserConsentTypes.UnifiedConsent.toObject(includeInstance, f),
    assistantActivityControlConsent: (f = msg.getAssistantActivityControlConsent()) && user_consent_types_pb.UserConsentTypes.AssistantActivityControlConsent.toObject(includeInstance, f),
    accountId: jspb.Message.getField(msg, 6),
    feature: jspb.Message.getField(msg, 1),
    descriptionGrdIdsList: jspb.Message.getRepeatedField(msg, 2),
    confirmationGrdId: jspb.Message.getField(msg, 3),
    status: jspb.Message.getField(msg, 5)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.UserConsentSpecifics}
 */
proto.sync_pb.UserConsentSpecifics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.UserConsentSpecifics;
  return proto.sync_pb.UserConsentSpecifics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.UserConsentSpecifics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.UserConsentSpecifics}
 */
proto.sync_pb.UserConsentSpecifics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setLocale(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setClientConsentTimeUsec(value);
      break;
    case 7:
      var value = new user_consent_types_pb.UserConsentTypes.SyncConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.SyncConsent.deserializeBinaryFromReader);
      msg.setSyncConsent(value);
      break;
    case 8:
      var value = new user_consent_types_pb.UserConsentTypes.ArcBackupAndRestoreConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.ArcBackupAndRestoreConsent.deserializeBinaryFromReader);
      msg.setArcBackupAndRestoreConsent(value);
      break;
    case 9:
      var value = new user_consent_types_pb.UserConsentTypes.ArcGoogleLocationServiceConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.ArcGoogleLocationServiceConsent.deserializeBinaryFromReader);
      msg.setArcLocationServiceConsent(value);
      break;
    case 10:
      var value = new user_consent_types_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent.deserializeBinaryFromReader);
      msg.setArcPlayTermsOfServiceConsent(value);
      break;
    case 11:
      var value = new user_consent_types_pb.UserConsentTypes.ArcMetricsAndUsageConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.ArcMetricsAndUsageConsent.deserializeBinaryFromReader);
      msg.setArcMetricsAndUsageConsent(value);
      break;
    case 13:
      var value = new user_consent_types_pb.UserConsentTypes.UnifiedConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.UnifiedConsent.deserializeBinaryFromReader);
      msg.setUnifiedConsent(value);
      break;
    case 14:
      var value = new user_consent_types_pb.UserConsentTypes.AssistantActivityControlConsent;
      reader.readMessage(value,user_consent_types_pb.UserConsentTypes.AssistantActivityControlConsent.deserializeBinaryFromReader);
      msg.setAssistantActivityControlConsent(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setAccountId(value);
      break;
    case 1:
      var value = /** @type {!proto.sync_pb.UserConsentSpecifics.Feature} */ (reader.readEnum());
      msg.setFeature(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.addDescriptionGrdIds(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setConfirmationGrdId(value);
      break;
    case 5:
      var value = /** @type {!proto.sync_pb.UserConsentTypes.ConsentStatus} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.UserConsentSpecifics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.UserConsentSpecifics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.UserConsentSpecifics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.UserConsentSpecifics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 12));
  if (f != null) {
    writer.writeInt64(
      12,
      f
    );
  }
  f = message.getSyncConsent();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      user_consent_types_pb.UserConsentTypes.SyncConsent.serializeBinaryToWriter
    );
  }
  f = message.getArcBackupAndRestoreConsent();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      user_consent_types_pb.UserConsentTypes.ArcBackupAndRestoreConsent.serializeBinaryToWriter
    );
  }
  f = message.getArcLocationServiceConsent();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      user_consent_types_pb.UserConsentTypes.ArcGoogleLocationServiceConsent.serializeBinaryToWriter
    );
  }
  f = message.getArcPlayTermsOfServiceConsent();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      user_consent_types_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent.serializeBinaryToWriter
    );
  }
  f = message.getArcMetricsAndUsageConsent();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      user_consent_types_pb.UserConsentTypes.ArcMetricsAndUsageConsent.serializeBinaryToWriter
    );
  }
  f = message.getUnifiedConsent();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      user_consent_types_pb.UserConsentTypes.UnifiedConsent.serializeBinaryToWriter
    );
  }
  f = message.getAssistantActivityControlConsent();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      user_consent_types_pb.UserConsentTypes.AssistantActivityControlConsent.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 6));
  if (f != null) {
    writer.writeString(
      6,
      f
    );
  }
  f = /** @type {!proto.sync_pb.UserConsentSpecifics.Feature} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getDescriptionGrdIdsList();
  if (f.length > 0) {
    writer.writeRepeatedInt32(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = /** @type {!proto.sync_pb.UserConsentTypes.ConsentStatus} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeEnum(
      5,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.sync_pb.UserConsentSpecifics.Feature = {
  FEATURE_UNSPECIFIED: 0,
  CHROME_SYNC: 1,
  PLAY_STORE: 2,
  BACKUP_AND_RESTORE: 3,
  GOOGLE_LOCATION_SERVICE: 4,
  CHROME_UNIFIED_CONSENT: 5,
  ASSISTANT_ACTIVITY_CONTROL: 6
};

/**
 * optional string locale = 4;
 * @return {string}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getLocale = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/** @param {string} value */
proto.sync_pb.UserConsentSpecifics.prototype.setLocale = function(value) {
  jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearLocale = function() {
  jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasLocale = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int64 client_consent_time_usec = 12;
 * @return {number}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getClientConsentTimeUsec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/** @param {number} value */
proto.sync_pb.UserConsentSpecifics.prototype.setClientConsentTimeUsec = function(value) {
  jspb.Message.setField(this, 12, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearClientConsentTimeUsec = function() {
  jspb.Message.setField(this, 12, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasClientConsentTimeUsec = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional UserConsentTypes.SyncConsent sync_consent = 7;
 * @return {?proto.sync_pb.UserConsentTypes.SyncConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getSyncConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.SyncConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.SyncConsent, 7));
};


/** @param {?proto.sync_pb.UserConsentTypes.SyncConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setSyncConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 7, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearSyncConsent = function() {
  this.setSyncConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasSyncConsent = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional UserConsentTypes.ArcBackupAndRestoreConsent arc_backup_and_restore_consent = 8;
 * @return {?proto.sync_pb.UserConsentTypes.ArcBackupAndRestoreConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getArcBackupAndRestoreConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.ArcBackupAndRestoreConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.ArcBackupAndRestoreConsent, 8));
};


/** @param {?proto.sync_pb.UserConsentTypes.ArcBackupAndRestoreConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setArcBackupAndRestoreConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 8, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearArcBackupAndRestoreConsent = function() {
  this.setArcBackupAndRestoreConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasArcBackupAndRestoreConsent = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional UserConsentTypes.ArcGoogleLocationServiceConsent arc_location_service_consent = 9;
 * @return {?proto.sync_pb.UserConsentTypes.ArcGoogleLocationServiceConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getArcLocationServiceConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.ArcGoogleLocationServiceConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.ArcGoogleLocationServiceConsent, 9));
};


/** @param {?proto.sync_pb.UserConsentTypes.ArcGoogleLocationServiceConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setArcLocationServiceConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 9, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearArcLocationServiceConsent = function() {
  this.setArcLocationServiceConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasArcLocationServiceConsent = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional UserConsentTypes.ArcPlayTermsOfServiceConsent arc_play_terms_of_service_consent = 10;
 * @return {?proto.sync_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getArcPlayTermsOfServiceConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent, 10));
};


/** @param {?proto.sync_pb.UserConsentTypes.ArcPlayTermsOfServiceConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setArcPlayTermsOfServiceConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 10, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearArcPlayTermsOfServiceConsent = function() {
  this.setArcPlayTermsOfServiceConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasArcPlayTermsOfServiceConsent = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional UserConsentTypes.ArcMetricsAndUsageConsent arc_metrics_and_usage_consent = 11;
 * @return {?proto.sync_pb.UserConsentTypes.ArcMetricsAndUsageConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getArcMetricsAndUsageConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.ArcMetricsAndUsageConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.ArcMetricsAndUsageConsent, 11));
};


/** @param {?proto.sync_pb.UserConsentTypes.ArcMetricsAndUsageConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setArcMetricsAndUsageConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 11, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearArcMetricsAndUsageConsent = function() {
  this.setArcMetricsAndUsageConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasArcMetricsAndUsageConsent = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional UserConsentTypes.UnifiedConsent unified_consent = 13;
 * @return {?proto.sync_pb.UserConsentTypes.UnifiedConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getUnifiedConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.UnifiedConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.UnifiedConsent, 13));
};


/** @param {?proto.sync_pb.UserConsentTypes.UnifiedConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setUnifiedConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 13, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearUnifiedConsent = function() {
  this.setUnifiedConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasUnifiedConsent = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional UserConsentTypes.AssistantActivityControlConsent assistant_activity_control_consent = 14;
 * @return {?proto.sync_pb.UserConsentTypes.AssistantActivityControlConsent}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getAssistantActivityControlConsent = function() {
  return /** @type{?proto.sync_pb.UserConsentTypes.AssistantActivityControlConsent} */ (
    jspb.Message.getWrapperField(this, user_consent_types_pb.UserConsentTypes.AssistantActivityControlConsent, 14));
};


/** @param {?proto.sync_pb.UserConsentTypes.AssistantActivityControlConsent|undefined} value */
proto.sync_pb.UserConsentSpecifics.prototype.setAssistantActivityControlConsent = function(value) {
  jspb.Message.setOneofWrapperField(this, 14, proto.sync_pb.UserConsentSpecifics.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearAssistantActivityControlConsent = function() {
  this.setAssistantActivityControlConsent(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasAssistantActivityControlConsent = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional string account_id = 6;
 * @return {string}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getAccountId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/** @param {string} value */
proto.sync_pb.UserConsentSpecifics.prototype.setAccountId = function(value) {
  jspb.Message.setField(this, 6, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearAccountId = function() {
  jspb.Message.setField(this, 6, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasAccountId = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional Feature feature = 1;
 * @return {!proto.sync_pb.UserConsentSpecifics.Feature}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getFeature = function() {
  return /** @type {!proto.sync_pb.UserConsentSpecifics.Feature} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {!proto.sync_pb.UserConsentSpecifics.Feature} value */
proto.sync_pb.UserConsentSpecifics.prototype.setFeature = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearFeature = function() {
  jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasFeature = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated int32 description_grd_ids = 2;
 * @return {!Array<number>}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getDescriptionGrdIdsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 2));
};


/** @param {!Array<number>} value */
proto.sync_pb.UserConsentSpecifics.prototype.setDescriptionGrdIdsList = function(value) {
  jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 */
proto.sync_pb.UserConsentSpecifics.prototype.addDescriptionGrdIds = function(value, opt_index) {
  jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearDescriptionGrdIdsList = function() {
  this.setDescriptionGrdIdsList([]);
};


/**
 * optional int32 confirmation_grd_id = 3;
 * @return {number}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getConfirmationGrdId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/** @param {number} value */
proto.sync_pb.UserConsentSpecifics.prototype.setConfirmationGrdId = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearConfirmationGrdId = function() {
  jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasConfirmationGrdId = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional UserConsentTypes.ConsentStatus status = 5;
 * @return {!proto.sync_pb.UserConsentTypes.ConsentStatus}
 */
proto.sync_pb.UserConsentSpecifics.prototype.getStatus = function() {
  return /** @type {!proto.sync_pb.UserConsentTypes.ConsentStatus} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/** @param {!proto.sync_pb.UserConsentTypes.ConsentStatus} value */
proto.sync_pb.UserConsentSpecifics.prototype.setStatus = function(value) {
  jspb.Message.setField(this, 5, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.UserConsentSpecifics.prototype.clearStatus = function() {
  jspb.Message.setField(this, 5, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.UserConsentSpecifics.prototype.hasStatus = function() {
  return jspb.Message.getField(this, 5) != null;
};


goog.object.extend(exports, proto.sync_pb);
