/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.sync_pb.GlobalIdDirective', null, global);
goog.exportSymbol('proto.sync_pb.HistoryDeleteDirectiveSpecifics', null, global);
goog.exportSymbol('proto.sync_pb.TimeRangeDirective', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.HistoryDeleteDirectiveSpecifics, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.HistoryDeleteDirectiveSpecifics.displayName = 'proto.sync_pb.HistoryDeleteDirectiveSpecifics';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.GlobalIdDirective = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.sync_pb.GlobalIdDirective.repeatedFields_, null);
};
goog.inherits(proto.sync_pb.GlobalIdDirective, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.GlobalIdDirective.displayName = 'proto.sync_pb.GlobalIdDirective';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.sync_pb.TimeRangeDirective = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.sync_pb.TimeRangeDirective, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.sync_pb.TimeRangeDirective.displayName = 'proto.sync_pb.TimeRangeDirective';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.HistoryDeleteDirectiveSpecifics.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.HistoryDeleteDirectiveSpecifics} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.toObject = function(includeInstance, msg) {
  var obj = {
    globalIdDirective: (f = msg.getGlobalIdDirective()) && proto.sync_pb.GlobalIdDirective.toObject(includeInstance, f),
    timeRangeDirective: (f = msg.getTimeRangeDirective()) && proto.sync_pb.TimeRangeDirective.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.HistoryDeleteDirectiveSpecifics}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.HistoryDeleteDirectiveSpecifics;
  return proto.sync_pb.HistoryDeleteDirectiveSpecifics.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.HistoryDeleteDirectiveSpecifics} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.HistoryDeleteDirectiveSpecifics}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.sync_pb.GlobalIdDirective;
      reader.readMessage(value,proto.sync_pb.GlobalIdDirective.deserializeBinaryFromReader);
      msg.setGlobalIdDirective(value);
      break;
    case 2:
      var value = new proto.sync_pb.TimeRangeDirective;
      reader.readMessage(value,proto.sync_pb.TimeRangeDirective.deserializeBinaryFromReader);
      msg.setTimeRangeDirective(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.HistoryDeleteDirectiveSpecifics.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.HistoryDeleteDirectiveSpecifics} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGlobalIdDirective();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.sync_pb.GlobalIdDirective.serializeBinaryToWriter
    );
  }
  f = message.getTimeRangeDirective();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.sync_pb.TimeRangeDirective.serializeBinaryToWriter
    );
  }
};


/**
 * optional GlobalIdDirective global_id_directive = 1;
 * @return {?proto.sync_pb.GlobalIdDirective}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.getGlobalIdDirective = function() {
  return /** @type{?proto.sync_pb.GlobalIdDirective} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.GlobalIdDirective, 1));
};


/** @param {?proto.sync_pb.GlobalIdDirective|undefined} value */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.setGlobalIdDirective = function(value) {
  jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.clearGlobalIdDirective = function() {
  this.setGlobalIdDirective(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.hasGlobalIdDirective = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional TimeRangeDirective time_range_directive = 2;
 * @return {?proto.sync_pb.TimeRangeDirective}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.getTimeRangeDirective = function() {
  return /** @type{?proto.sync_pb.TimeRangeDirective} */ (
    jspb.Message.getWrapperField(this, proto.sync_pb.TimeRangeDirective, 2));
};


/** @param {?proto.sync_pb.TimeRangeDirective|undefined} value */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.setTimeRangeDirective = function(value) {
  jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.clearTimeRangeDirective = function() {
  this.setTimeRangeDirective(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.HistoryDeleteDirectiveSpecifics.prototype.hasTimeRangeDirective = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.sync_pb.GlobalIdDirective.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.GlobalIdDirective.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.GlobalIdDirective.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.GlobalIdDirective} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GlobalIdDirective.toObject = function(includeInstance, msg) {
  var obj = {
    globalIdList: jspb.Message.getRepeatedField(msg, 1),
    startTimeUsec: jspb.Message.getField(msg, 2),
    endTimeUsec: jspb.Message.getField(msg, 3)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.GlobalIdDirective}
 */
proto.sync_pb.GlobalIdDirective.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.GlobalIdDirective;
  return proto.sync_pb.GlobalIdDirective.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.GlobalIdDirective} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.GlobalIdDirective}
 */
proto.sync_pb.GlobalIdDirective.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.addGlobalId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStartTimeUsec(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setEndTimeUsec(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.GlobalIdDirective.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.GlobalIdDirective.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.GlobalIdDirective} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.GlobalIdDirective.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGlobalIdList();
  if (f.length > 0) {
    writer.writeRepeatedInt64(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * repeated int64 global_id = 1;
 * @return {!Array<number>}
 */
proto.sync_pb.GlobalIdDirective.prototype.getGlobalIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/** @param {!Array<number>} value */
proto.sync_pb.GlobalIdDirective.prototype.setGlobalIdList = function(value) {
  jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 */
proto.sync_pb.GlobalIdDirective.prototype.addGlobalId = function(value, opt_index) {
  jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 */
proto.sync_pb.GlobalIdDirective.prototype.clearGlobalIdList = function() {
  this.setGlobalIdList([]);
};


/**
 * optional int64 start_time_usec = 2;
 * @return {number}
 */
proto.sync_pb.GlobalIdDirective.prototype.getStartTimeUsec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {number} value */
proto.sync_pb.GlobalIdDirective.prototype.setStartTimeUsec = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.GlobalIdDirective.prototype.clearStartTimeUsec = function() {
  jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GlobalIdDirective.prototype.hasStartTimeUsec = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional int64 end_time_usec = 3;
 * @return {number}
 */
proto.sync_pb.GlobalIdDirective.prototype.getEndTimeUsec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/** @param {number} value */
proto.sync_pb.GlobalIdDirective.prototype.setEndTimeUsec = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.GlobalIdDirective.prototype.clearEndTimeUsec = function() {
  jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.GlobalIdDirective.prototype.hasEndTimeUsec = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.sync_pb.TimeRangeDirective.prototype.toObject = function(opt_includeInstance) {
  return proto.sync_pb.TimeRangeDirective.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.sync_pb.TimeRangeDirective} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.TimeRangeDirective.toObject = function(includeInstance, msg) {
  var obj = {
    startTimeUsec: jspb.Message.getField(msg, 1),
    endTimeUsec: jspb.Message.getField(msg, 2)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.sync_pb.TimeRangeDirective}
 */
proto.sync_pb.TimeRangeDirective.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.sync_pb.TimeRangeDirective;
  return proto.sync_pb.TimeRangeDirective.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.sync_pb.TimeRangeDirective} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.sync_pb.TimeRangeDirective}
 */
proto.sync_pb.TimeRangeDirective.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStartTimeUsec(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setEndTimeUsec(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.sync_pb.TimeRangeDirective.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.sync_pb.TimeRangeDirective.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.sync_pb.TimeRangeDirective} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.sync_pb.TimeRangeDirective.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional int64 start_time_usec = 1;
 * @return {number}
 */
proto.sync_pb.TimeRangeDirective.prototype.getStartTimeUsec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.sync_pb.TimeRangeDirective.prototype.setStartTimeUsec = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.TimeRangeDirective.prototype.clearStartTimeUsec = function() {
  jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.TimeRangeDirective.prototype.hasStartTimeUsec = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional int64 end_time_usec = 2;
 * @return {number}
 */
proto.sync_pb.TimeRangeDirective.prototype.getEndTimeUsec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/** @param {number} value */
proto.sync_pb.TimeRangeDirective.prototype.setEndTimeUsec = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 */
proto.sync_pb.TimeRangeDirective.prototype.clearEndTimeUsec = function() {
  jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.sync_pb.TimeRangeDirective.prototype.hasEndTimeUsec = function() {
  return jspb.Message.getField(this, 2) != null;
};


goog.object.extend(exports, proto.sync_pb);
